﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Satpam_Desktop.Class
{
    public class RequestCompleteEventArgs : System.EventArgs
    {
        private string response, bagian;

        public RequestCompleteEventArgs(string response, string bagian)
        {
            this.response = response;
            this.bagian = bagian;
        }

        public string Bagian
        {
            get
            {
                return bagian;
            }
        }

        public string Response
        {
            get
            {
                return response;
            }
        }
    }

    public class DataGridSegarkanEventArgs : System.EventArgs
    {
        private string mBagian;

        public DataGridSegarkanEventArgs(string Bagian)
        {
            this.mBagian = Bagian;
        }

        public string Bagian
        {
            get
            {
                return mBagian;
            }
        }
    }

    public class ItemTerpilihEventArgs : System.EventArgs
    {
        public List<string> mDaftar = new List<string>();

        public ItemTerpilihEventArgs(List<string> Daftar)
        {
            this.mDaftar = Daftar;
        }

        public List<string> Daftar
        {
            get
            {
                return mDaftar;
            }
        }
    }
}
