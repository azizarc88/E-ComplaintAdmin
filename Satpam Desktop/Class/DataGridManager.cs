﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Satpam_Desktop.Class;
using DevComponents.DotNetBar;
using System.Drawing;
using System.Collections.Specialized;

namespace Satpam_Desktop.Class
{
    class DataGridManager
    {
        string requestUrl;
        DataGridView datagrid;
        RESTService rs;
        bool sembunyi;

        public void IsiDG(DataGridView datagrid, String bagian, string unique = null, bool sembunyi = false)
        {
            rs = new RESTService();
            rs.RequestComplete += new RESTService.RequestCompleteHandler(RequestComplete_Event);
            this.datagrid = datagrid;
            this.sembunyi = sembunyi;
            NameValueCollection formData = null;

            if (bagian == "Pengguna")
            {
                requestUrl = "ambil_pengguna.php";
            }
            else if (bagian == "Laporan Fasilitas")
            {
                formData = new NameValueCollection();
                formData["tabel"] = "laporanfasilitas";
                requestUrl = "ambil_laporan.php";
            }
            else if (bagian == "Komentar Fasilitas")
            {
                requestUrl = "ambil_komentar_fasilitas.php?lfid=" + unique + "&oleh=" + Program.Pgid;
            }
            else if (bagian == "Laporan Pelanggaran")
            {
                formData = new NameValueCollection();
                formData["tabel"] = "laporanpelanggaran";
                requestUrl = "ambil_laporan.php";
            }
            else if (bagian == "Komentar Pelanggaran")
            {
                requestUrl = "ambil_komentar_pelanggaran.php?lpid=" + unique + "&oleh=" + Program.Pgid;
            }
            else if (bagian == "LaporanFasDilaporkan")
            {
                requestUrl = "ambil_laporan_laporanfas.php";
            }
            else if (bagian == "LaporanPlgrDilaporkan")
            {
                requestUrl = "ambil_laporan_laporanplgr.php";
            }
            else if (bagian == "Laporan KomentarFas")
            {
                requestUrl = "ambil_laporan_komentarfasilitas.php";
            }
            else if (bagian == "Laporan KomentarPlgr")
            {
                requestUrl = "ambil_laporan_komentarpelanggaran.php";
            }
            else if (bagian == "Obrolan")
            {
                requestUrl = "ambil_obrolan_admin.php?untuk=" + unique;
            }
            else if (bagian == "ObrolanMasuk")
            {
                requestUrl = "ambil_obrolan.php?dari=" + unique + "&untuk=" + Program.Pgid;
            }
            else if (bagian == "Pengaturan Sebagai")
            {
                requestUrl = "ambil_sebagai.php";
            }

            rs.MakeRequestAsync(requestUrl, formData, bagian);
        }

        private void RequestComplete_Event(object sender, RequestCompleteEventArgs e)
        {
            string hasil = rs.GetString(e.Response, "hasil");
            if (hasil == "SUKSES")
            {
                if (!sembunyi)
                {
                    datagrid.Rows.Clear();
                }
                if (e.Bagian == "Pengguna")
                {
                    string[] data = rs.GetArray(e.Response, "pengguna");

                    foreach (var item in data)
                    {
                        datagrid.Rows.Add(rs.GetString(item, "pgid"), rs.GetString(item, "nama"), rs.GetString(item, "sebagai"), rs.GetString(item, "alamat"), rs.GetString(item, "desa"), rs.GetString(item, "kota"), rs.GetString(item, "tanggallahir"), rs.GetString(item, "totallaporan"), rs.GetString(item, "nohp"), rs.GetString(item, "email"), rs.GetString(item, "password"), rs.GetString(item, "blokir"));
                    }
                }
                else if (e.Bagian == "Laporan Fasilitas")
                {
                    string[] data = rs.GetArray(e.Response, "laporanfasilitas");

                    foreach (var item in data)
                    {
                        DateTime date = DateTime.Parse(rs.GetString(item, "dilaporkanpada"));
                        datagrid.Rows.Add(rs.GetString(item, "lfid"), rs.GetString(item, "foto"), rs.GetString(item, "keluhan"), rs.GetString(item, "lat"), rs.GetString(item, "ling"), rs.GetString(item, "alamat"), rs.GetString(item, "prioritas"), date.ToLongDateString() + " " + date.ToLongTimeString(), rs.GetString(item, "dilaporkanoleh"), rs.GetString(item, "laporanrahasia"), rs.GetString(item, "dukungan"), rs.GetString(item, "komentar"));
                    }
                }
                else if (e.Bagian == "Komentar Fasilitas")
                {
                    string[] data = rs.GetArray(e.Response, "komentar");
                    
                    if (data.Length > 0)
                    {
                        if (datagrid.Visible == false)
                        {
                            datagrid.Visible = true;
                        }
                    }

                    datagrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                    int a, b, c, d;
                    a = 0; b = 1; c = 2; d = 3 ;
                    foreach (var item in data)
                    {
                        datagrid.Rows.Add(rs.GetString(item, "kmid"), rs.GetString(item, "komentarku"), rs.GetString(item, "nama"), "X");
                        datagrid.Rows[a].Cells[2].Style.Font = new Font("Microsoft Sans Serif", 10, FontStyle.Regular);
                        DateTime date = DateTime.Parse(rs.GetString(item, "waktu"));
                        datagrid.Rows.Add("", "", date.ToLongDateString() + " " + date.ToLongTimeString(), "");
                        datagrid.Rows[b].Cells[2].Style.Font = new Font("Microsoft Sans Serif", 6.5f, FontStyle.Regular);
                        datagrid.Rows.Add("", "", rs.GetString(item, "komentar"), "");
                        datagrid.Rows[c].Cells[2].Style.Font = new Font("Microsoft Sans Serif", 10, FontStyle.Regular);
                        datagrid.Rows[c].Cells[2].Style.Alignment = DataGridViewContentAlignment.BottomLeft;
                        datagrid.Rows[c].Cells[2].Style.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
                        datagrid.Rows.Add(" ");
                        datagrid.Rows[d].Cells[2].Style.Font = new Font("Microsoft Sans Serif", 2f, FontStyle.Regular);
                        a += 4;
                        b += 4;
                        c += 4;
                        d += 4;
                    }
                }
                else if (e.Bagian == "Laporan Pelanggaran")
                {
                    string[] data = rs.GetArray(e.Response, "laporanpelanggaran");

                    foreach (var item in data)
                    {
                        DateTime date = DateTime.Parse(rs.GetString(item, "dilaporkanpada"));
                        datagrid.Rows.Add(rs.GetString(item, "lpid"), rs.GetString(item, "foto"), rs.GetString(item, "pelanggaran"), rs.GetString(item, "lat"), rs.GetString(item, "ling"), rs.GetString(item, "alamat"), rs.GetString(item, "prioritas"), date.ToLongDateString() + " " + date.ToLongTimeString(), rs.GetString(item, "dilaporkanoleh"), rs.GetString(item, "laporanrahasia"), rs.GetString(item, "dukungan"), rs.GetString(item, "komentar"));
                    }
                }
                else if (e.Bagian == "Komentar Pelanggaran")
                {
                    string[] data = rs.GetArray(e.Response, "komentar");

                    if (data.Length > 0)
                    {
                        if (datagrid.Visible == false)
                        {
                            datagrid.Visible = true;
                        }
                    }

                    datagrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                    int a, b, c, d;
                    a = 0; b = 1; c = 2; d = 3;
                    foreach (var item in data)
                    {
                        datagrid.Rows.Add(rs.GetString(item, "kmid"), rs.GetString(item, "komentarku"), rs.GetString(item, "nama"), "X");
                        datagrid.Rows[a].Cells[2].Style.Font = new Font("Microsoft Sans Serif", 10, FontStyle.Regular);
                        DateTime date = DateTime.Parse(rs.GetString(item, "waktu"));
                        datagrid.Rows.Add("", "", date.ToLongDateString() + " " + date.ToLongTimeString(), "");
                        datagrid.Rows[b].Cells[2].Style.Font = new Font("Microsoft Sans Serif", 6.5f, FontStyle.Regular);
                        datagrid.Rows.Add("", "", rs.GetString(item, "komentar"), "");
                        datagrid.Rows[c].Cells[2].Style.Font = new Font("Microsoft Sans Serif", 10, FontStyle.Regular);
                        datagrid.Rows[c].Cells[2].Style.Alignment = DataGridViewContentAlignment.BottomLeft;
                        datagrid.Rows[c].Cells[2].Style.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
                        datagrid.Rows.Add(" ");
                        datagrid.Rows[d].Cells[2].Style.Font = new Font("Microsoft Sans Serif", 2f, FontStyle.Regular);
                        a += 4;
                        b += 4;
                        c += 4;
                        d += 4;
                    }
                }
                else if (e.Bagian == "LaporanFasDilaporkan")
                {
                    string[] data = rs.GetArray(e.Response, "fasdilaporkan");

                    foreach (var item in data)
                    {
                        DateTime date = DateTime.Parse(rs.GetString(item, "waktu"));
                        datagrid.Rows.Add(rs.GetString(item, "ldid"), rs.GetString(item, "lfid"), rs.GetString(item, "pgid"), date.ToLongDateString() + " " + date.ToLongTimeString(), rs.GetString(item, "keluhan"), rs.GetString(item, "nama"));
                    }
                }
                else if (e.Bagian == "LaporanPlgrDilaporkan")
                {
                    string[] data = rs.GetArray(e.Response, "plgrdilaporkan");

                    foreach (var item in data)
                    {
                        DateTime date = DateTime.Parse(rs.GetString(item, "waktu"));
                        datagrid.Rows.Add(rs.GetString(item, "ldid"), rs.GetString(item, "lpid"), rs.GetString(item, "pgid"), date.ToLongDateString() + " " + date.ToLongTimeString(), rs.GetString(item, "pelanggaran"), rs.GetString(item, "nama"));
                    }
                }
                else if (e.Bagian == "Laporan KomentarFas" || e.Bagian == "Laporan KomentarPlgr")
                {
                    string[] data = rs.GetArray(e.Response, "laporankomentar");

                    foreach (var item in data)
                    {
                        DateTime date = DateTime.Parse(rs.GetString(item, "waktu"));
                        datagrid.Rows.Add(rs.GetString(item, "kdid"), rs.GetString(item, "untuk"), rs.GetString(item, "pgid"), date.ToLongDateString() + " " + date.ToLongTimeString(), rs.GetString(item, "komentar"), rs.GetString(item, "nama"));
                    }
                }
                else if (e.Bagian == "Obrolan")
                {
                    string[] data = rs.GetArray(e.Response, "feed");

                    foreach (var item in data)
                    {
                        datagrid.Rows.Add(rs.GetString(item, "pgid"), rs.GetString(item, "nama"));
                    }
                }
                else if (e.Bagian == "ObrolanMasuk")
                {
                    string[] data = rs.GetArray(e.Response, "feed");

                    if (data.Length > 0)
                    {
                        if (datagrid.Visible == false)
                        {
                            datagrid.Visible = true;
                        }
                    }

                    datagrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                    int a, b, c;
                    a = 0; b = 1; c = 2;
                    DataGridViewContentAlignment align = DataGridViewContentAlignment.MiddleCenter;
                    foreach (var item in data)
                    {
                        string dari = rs.GetString(item, "dari");
                        if (dari == Program.Pgid)
                        {
                            align = DataGridViewContentAlignment.MiddleRight;
                        }
                        else
                        {
                            align = DataGridViewContentAlignment.MiddleLeft;
                        }

                        try
                        {
                            if (datagrid.Rows[a] != null)
                            {
                                datagrid.Rows[a].Cells[0].Value = (rs.GetString(item, "isipesan").Replace("\\n", " ").Replace("\\r", ""));
                                datagrid.Rows[a].Cells[0].Style.Font = new Font("Microsoft Sans Serif", 10, FontStyle.Regular);
                                datagrid.Rows[a].Cells[0].Style.Alignment = align;
                                DateTime date = DateTime.Parse(rs.GetString(item, "waktu"));
                                datagrid.Rows[b].Cells[0].Value = date.ToLongDateString() + " " + date.ToLongTimeString();
                                datagrid.Rows[b].Cells[0].Style.Font = new Font("Microsoft Sans Serif", 6.5f, FontStyle.Regular);
                                datagrid.Rows[b].Cells[0].Style.Alignment = align;
                                datagrid.Rows[c].Cells[0].Style.Font = new Font("Microsoft Sans Serif", 2f, FontStyle.Regular);
                            }
                            else
                            {
                                datagrid.Rows.Add(rs.GetString(item, "isipesan").Replace("\\n", " ").Replace("\\r", ""));
                                datagrid.Rows[a].Cells[0].Style.Font = new Font("Microsoft Sans Serif", 10, FontStyle.Regular);
                                datagrid.Rows[a].Cells[0].Style.Alignment = align;
                                DateTime date = DateTime.Parse(rs.GetString(item, "waktu"));
                                datagrid.Rows.Add(date.ToLongDateString() + " " + date.ToLongTimeString());
                                datagrid.Rows[b].Cells[0].Style.Font = new Font("Microsoft Sans Serif", 6.5f, FontStyle.Regular);
                                datagrid.Rows[b].Cells[0].Style.Alignment = align;
                                datagrid.Rows.Add(" ");
                                datagrid.Rows[c].Cells[0].Style.Font = new Font("Microsoft Sans Serif", 2f, FontStyle.Regular);
                            }
                        }
                        catch (Exception)
                        {   
                            datagrid.Rows.Add(rs.GetString(item, "isipesan").Replace("\\n", " ").Replace("\\r", ""));
                            datagrid.Rows[a].Cells[0].Style.Font = new Font("Microsoft Sans Serif", 10, FontStyle.Regular);
                            datagrid.Rows[a].Cells[0].Style.Alignment = align;
                            DateTime date = DateTime.Parse(rs.GetString(item, "waktu"));
                            datagrid.Rows.Add(date.ToLongDateString() + " " + date.ToLongTimeString());
                            datagrid.Rows[b].Cells[0].Style.Font = new Font("Microsoft Sans Serif", 6.5f, FontStyle.Regular);
                            datagrid.Rows[b].Cells[0].Style.Alignment = align;
                            datagrid.Rows.Add(" ");
                            datagrid.Rows[c].Cells[0].Style.Font = new Font("Microsoft Sans Serif", 2f, FontStyle.Regular);
                        }
                        a += 3;
                        b += 3;
                        c += 3;
                    }
                }
                else if (e.Bagian == "Pengaturan Sebagai")
                {
                    string[] data = rs.GetArray(e.Response, "sebagai");

                    foreach (var item in data)
                    {
                        datagrid.Rows.Add(rs.GetString(item, "sbid"), rs.GetString(item, "sebagai"), rs.GetString(item, "cpanel"), rs.GetString(item, "cpengguna"), rs.GetString(item, "claporanfas"), rs.GetString(item, "claporanplgr"), rs.GetString(item, "claporankmt"), rs.GetString(item, "csebagai"));
                    }
                }
            }
            else if (hasil == "TIDAK ADA")
            {
                datagrid.Rows.Clear();
                if (!sembunyi)
                {
                    ToastNotification.Show(datagrid, "Tidak ada data", null, 3000, eToastGlowColor.Orange);
                }
            }
            else if (hasil == "GAGAL")
            {
                if (!sembunyi)
                {
                    ToastNotification.Show(datagrid, "Tidak dapat terhubung, periksa koneksi", null, 3000, eToastGlowColor.Red);
                }
            }
            else
            {
                if (!sembunyi)
                {
                    MessageBoxEx.Show(e.Response, "Terjadi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
