﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Satpam_Desktop.Form;

namespace Satpam_Desktop
{
    static class Program
    {
        public static string Pgid = "";
        public static string Id = "";
        public static string Nama = "";
        public static string Sebagai = "";
        public static string Server = "http://smpn3-pwt.sch.id/satpam/php/";

        public static bool cpanel;
        public static bool cpengguna;
        public static bool claporanfas;
        public static bool claporanplgr;
        public static bool claporankmt;
        public static bool csebagai;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form_Login());

            }
            catch (Exception)
            {

            }
        }
    }
}
