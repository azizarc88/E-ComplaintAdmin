﻿using System;
using System.Collections.Generic;
using System.Text;
using Satpam_Desktop.Class;
using System.Collections.Specialized;
using Satpam_Desktop.Form;
using System.Windows.Forms;
using System.Net;

namespace Satpam_Desktop.Class
{
    class RefreshManager
    {
        bool laporanfasilitas = true;
        public bool KoneksiBagus
        {
            get; set;
        }

        RESTService rs = new RESTService();
        Timer t = new Timer();
        Timer t2 = new Timer();

        public RefreshManager()
        {
            t.Interval = 1500;
            t.Enabled = true;
            t.Tick += T_Tick;
            rs.RequestComplete += Rs_RequestComplete;

            t2.Interval = 5000;
            t2.Tick += T2_Tick;
        }

        private void T2_Tick(object sender, EventArgs e)
        {
            if (t.Enabled == false)
            {
                t.Enabled = true;
                t2.Enabled = false;
            }
        }

        private void Rs_RequestComplete(object sender, RequestCompleteEventArgs e)
        {
            string hasil = rs.GetString(e.Response, "hasil");
            if (e.Bagian == "laporanfasbaru")
            {
                if (hasil == "SUKSES")
                {
                    string[] data = rs.GetArray(e.Response, "laporanfasilitas");
                    string alamat = "", prioritas = "", laporan = "", id = "";

                    foreach (var item in data)
                    {
                        alamat = rs.GetString(item, "alamat");
                        prioritas = rs.GetString(item, "prioritas");
                        laporan = rs.GetString(item, "keluhan");
                        id = rs.GetString(item, "lfid");
                    }

                    Form_Pemberitahuan fpemberitahuan = new Form_Pemberitahuan("Ada laporan fasilitas baru" + Environment.NewLine + Environment.NewLine + laporan, alamat, prioritas, Form_Pemberitahuan.Bagian.Fasilitas, id);
                    fpemberitahuan.Show();

                    t.Enabled = true;
                    KoneksiBagus = true;
                }
                else
                {
                    KoneksiBagus = false;
                }
            }
            else if (e.Bagian == "laporanplgrbaru")
            {
                if (hasil == "SUKSES")
                {
                    string[] data = rs.GetArray(e.Response, "laporanpelanggaran");
                    string alamat = "", prioritas = "", laporan = "", id = "";

                    foreach (var item in data)
                    {
                        alamat = rs.GetString(item, "alamat");
                        prioritas = rs.GetString(item, "prioritas");
                        laporan = rs.GetString(item, "pelanggaran");
                        id = rs.GetString(item, "lpid");
                    }

                    Form_Pemberitahuan f = new Form_Pemberitahuan("Ada laporan pelanggaran baru\n\n" + laporan, alamat, prioritas, Form_Pemberitahuan.Bagian.Pelanggaran, id);
                    f.Show();

                    t.Enabled = true;
                    KoneksiBagus = true;
                }
                else
                {
                    KoneksiBagus = false;
                }
            }
            else if (e.Bagian == "getdatafas")
            {
                if (hasil == "SUKSES")
                {
                    KoneksiBagus = true;
                    int total = int.Parse(rs.GetString(e.Response, "total"));
                    if (firstfas)
                    {
                        totalfas = total;
                        firstfas = false;
                        t.Enabled = true;
                    }
                    else
                    {
                        if (total > totalfas)
                        {
                            NameValueCollection formData = new NameValueCollection();
                            formData["tabel"] = "laporanfasilitas";
                            rs.MakeRequestAsync("ambil_laporan_baru.php", formData, "laporanfasbaru");
                            totalfas = total;
                        }
                        else if (total < totalfas)
                        {
                            totalfas = total;
                            t.Enabled = true;
                        }
                        else
                        {
                            KoneksiBagus = true;
                            t.Enabled = true;
                        }
                    }
                }
                else
                {
                    KoneksiBagus = false;
                }
            }
            else if (e.Bagian == "getdataplgr")
            {
                if (hasil == "SUKSES")
                {
                            KoneksiBagus = true;
                    int total = int.Parse(rs.GetString(e.Response, "total"));
                    if (firstplgr)
                    {
                        totalplgr = total;
                        firstplgr = false;
                        t.Enabled = true;
                    }
                    else
                    {
                        if (total > totalplgr)
                        {
                            NameValueCollection formData = new NameValueCollection();
                            formData["tabel"] = "laporanpelanggaran";
                            rs.MakeRequestAsync("ambil_laporan_baru.php", formData, "laporanplgrbaru");
                            totalplgr = total;
                        }
                        else if (total < totalplgr)
                        {
                            totalplgr = total;
                            t.Enabled = true;
                        }
                        else
                        {
                            t.Enabled = true;
                        }
                    }
                }
                else
                {
                    KoneksiBagus = false;
                }
            }

            if (hasil == "GAGAL")
            {
                t2.Enabled = true;
                KoneksiBagus = false;
            }
        }

        private void T_Tick(object sender, EventArgs e)
        {
            t.Enabled = false;
            SegarkanLaporan();
        }

        private void SegarkanLaporan()
        {
            if (laporanfasilitas)
            {
                NameValueCollection formData = new NameValueCollection();
                formData["tabel"] = "laporanfasilitas";
                rs.MakeRequestAsync("refresh_laporan.php", formData, "getdatafas");
                laporanfasilitas = false;
            }
            else
            {
                NameValueCollection formData = new NameValueCollection();
                formData["tabel"] = "laporanpelanggaran";
                rs.MakeRequestAsync("refresh_laporan.php", formData, "getdataplgr");
                laporanfasilitas = true;
            }
        }

        int totalfas = 0;
        int totalplgr = 0;

        bool firstfas = true, firstplgr = true;
    }
}
