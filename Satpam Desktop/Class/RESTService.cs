﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Satpam_Desktop.Class
{
    public class RESTService
    {
        private string response;

        public delegate void RequestCompleteHandler(object sender, RequestCompleteEventArgs e);
        public event RequestCompleteHandler RequestComplete;

        public void MakeRequest(string requestUrl, NameValueCollection formData, string bagian)
        {
            WebClient webClient = new WebClient();

            if (formData == null)
            {
                formData = new NameValueCollection();
                formData["1"] = "1";
            }

            byte[] responseBytes = null;

            try
            {
                //Program.fdb.Write(Program.Server + requestUrl);
                responseBytes = webClient.UploadValues(Program.Server + requestUrl, "POST", formData);
                response = Encoding.UTF8.GetString(responseBytes);
                //Program.fdb.Write("Got response request " + response);
            }
            catch (Exception e)
            {
                response = "{\"hasil\":\"GAGAL\"}";
                //Program.fdb.Write("On error request " + e.ToString());
            }
            webClient.Dispose();


            //Program.fdb.Write("Begin make event request");
            RequestCompleteEventArgs args = new RequestCompleteEventArgs(response, bagian);
            RequestComplete(this, args);
        }

        WebClient webClientAsync;
        public void MakeRequestAsync(string requestUrl, NameValueCollection formData, string bagian)
        {
            webClientAsync = new WebClient();
            Uri uri = new Uri(Program.Server + requestUrl);
            //Program.fdb.Write("Begin make event request async " + uri.ToString());
            webClientAsync.UploadValuesCompleted += WebClient_UploadValuesCompleted;

            if (formData == null)
            {
                formData = new NameValueCollection();
                formData["1"] = "1";
            }

            try
            {
                //Program.fdb.Write("Begin request async " + uri.ToString());
                webClientAsync.UploadValuesAsync(uri, "POST", formData, bagian);
            }
            catch (Exception e)
            {
                response = "{\"hasil\":\"GAGAL\"}";
                //Program.fdb.Write("On error request async " + e.ToString());
            }
        }

        private void WebClient_UploadValuesCompleted(object sender, UploadValuesCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                //Program.fdb.Write("Got response request async " + Encoding.UTF8.GetString(e.Result));
                RequestCompleteEventArgs args = new RequestCompleteEventArgs(Encoding.UTF8.GetString(e.Result), e.UserState.ToString());
                RequestComplete(this, args);
            }
            else
            {
                //Program.fdb.Write("Got eror response " + e.Error.ToString());
                RequestCompleteEventArgs args = new RequestCompleteEventArgs("{\"hasil\":\"GAGAL\"}", e.UserState.ToString());
                RequestComplete(this, args);
            }
            
            webClientAsync.Dispose();
        }

        public string GetString(string namavalue)
        {
            string hasil = "";
            string temp = response.Replace("{", "");
            temp = temp.Replace("}", "");

            string[] sem = temp.Split(',');

            foreach (var item in sem)
            {
                if (item.Contains("\""))
                {
                    if (item.Contains("\"" + namavalue + "\""))
                    {
                        string v = "\"" + namavalue + "\":";
                        hasil = item.Substring(item.LastIndexOf(v) + v.Length);
                        hasil = hasil.Replace("\\/", "/");
                        hasil = hasil.Replace("\"", "");
                    }
                }
            }

            return hasil;
        }

        public string GetString(string response, string namavalue)
        {
            string hasil = "";
            string temp = response.Replace("{", "");
            temp = temp.Replace("}", "");
            temp = temp.Replace("]", "");

            string[] sem = temp.Split(',');

            foreach (var item in sem)
            {
                if (item.Contains("\""))
                {
                    if (item.Contains("\"" + namavalue + "\""))
                    {
                        string v = "\"" + namavalue + "\":";
                        hasil = item.Substring(item.LastIndexOf(v) + v.Length);
                        hasil = hasil.Replace("\\/", "/");
                        hasil = hasil.Replace("\"", "");
                    }
                }
            }

            return hasil;
        }

        public string[] GetArray(string namavalue)
        {
            string v = "\"" + namavalue + "\":[{";
            string sem = response.Substring(response.LastIndexOf(v) + v.Length);
            sem = sem.Replace("\\/", "/");
            sem = sem.Replace("]}", "");
            sem = sem.Replace("},", "");
            string[] temp = sem.Split('{');
            return temp;
        }

        public string[] GetArray(string response, string namavalue)
        {
            string v = "\"" + namavalue + "\":[{";
            string sem = response.Substring(response.LastIndexOf(v) + v.Length);
            sem = sem.Replace("\\/", "/");
            sem = sem.Replace("]}", "");
            sem = sem.Replace("},", "");
            string[] temp = sem.Split('{');
            return temp;
        }
    }
}
