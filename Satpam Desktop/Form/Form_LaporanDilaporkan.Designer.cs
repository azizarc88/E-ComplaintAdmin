﻿namespace Satpam_Desktop.Form
{
    partial class Form_LaporanDilaporkan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DGLaporanDilaporkan = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ldid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lfid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pgid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.waktu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.keluhan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BHapus = new DevComponents.DotNetBar.ButtonX();
            this.BUbah = new DevComponents.DotNetBar.ButtonX();
            this.BDetail = new DevComponents.DotNetBar.ButtonX();
            this.BTambah = new DevComponents.DotNetBar.ButtonX();
            this.statusStrip3 = new System.Windows.Forms.StatusStrip();
            this.TSSLBanyakDataPiket = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel7 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLNimPiket = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSPiket = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.DGLaporanDilaporkan)).BeginInit();
            this.statusStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // DGLaporanDilaporkan
            // 
            this.DGLaporanDilaporkan.AllowUserToAddRows = false;
            this.DGLaporanDilaporkan.AllowUserToDeleteRows = false;
            this.DGLaporanDilaporkan.AllowUserToResizeRows = false;
            this.DGLaporanDilaporkan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGLaporanDilaporkan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGLaporanDilaporkan.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGLaporanDilaporkan.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGLaporanDilaporkan.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DGLaporanDilaporkan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGLaporanDilaporkan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ldid,
            this.lfid,
            this.pgid,
            this.waktu,
            this.keluhan,
            this.nama});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGLaporanDilaporkan.DefaultCellStyle = dataGridViewCellStyle5;
            this.DGLaporanDilaporkan.EnableHeadersVisualStyles = false;
            this.DGLaporanDilaporkan.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGLaporanDilaporkan.Location = new System.Drawing.Point(12, 48);
            this.DGLaporanDilaporkan.Name = "DGLaporanDilaporkan";
            this.DGLaporanDilaporkan.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGLaporanDilaporkan.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DGLaporanDilaporkan.RowHeadersVisible = false;
            this.DGLaporanDilaporkan.RowHeadersWidth = 100;
            this.DGLaporanDilaporkan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGLaporanDilaporkan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGLaporanDilaporkan.Size = new System.Drawing.Size(799, 446);
            this.DGLaporanDilaporkan.TabIndex = 3;
            this.DGLaporanDilaporkan.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGLaporanDilaporkan_CellClick);
            this.DGLaporanDilaporkan.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGLaporanDilaporkan_CellDoubleClick);
            this.DGLaporanDilaporkan.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DGLaporanDilaporkan_RowsAdded);
            // 
            // ldid
            // 
            this.ldid.HeaderText = "LDID";
            this.ldid.Name = "ldid";
            this.ldid.ReadOnly = true;
            this.ldid.Visible = false;
            // 
            // lfid
            // 
            this.lfid.HeaderText = "LFID";
            this.lfid.Name = "lfid";
            this.lfid.ReadOnly = true;
            this.lfid.Visible = false;
            // 
            // pgid
            // 
            this.pgid.HeaderText = "PGID";
            this.pgid.Name = "pgid";
            this.pgid.ReadOnly = true;
            this.pgid.Visible = false;
            // 
            // waktu
            // 
            this.waktu.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.waktu.FillWeight = 25F;
            this.waktu.HeaderText = "Waktu";
            this.waktu.Name = "waktu";
            this.waktu.ReadOnly = true;
            // 
            // keluhan
            // 
            this.keluhan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.keluhan.DataPropertyName = "CPesan";
            this.keluhan.HeaderText = "Keluhan";
            this.keluhan.Name = "keluhan";
            this.keluhan.ReadOnly = true;
            // 
            // nama
            // 
            this.nama.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nama.FillWeight = 30F;
            this.nama.HeaderText = "Dilaporkan Oleh";
            this.nama.Name = "nama";
            this.nama.ReadOnly = true;
            // 
            // BHapus
            // 
            this.BHapus.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BHapus.AntiAlias = true;
            this.BHapus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.BHapus.EnableMarkup = false;
            this.BHapus.Location = new System.Drawing.Point(50, 12);
            this.BHapus.Margin = new System.Windows.Forms.Padding(5);
            this.BHapus.Name = "BHapus";
            this.BHapus.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.BHapus.Size = new System.Drawing.Size(28, 28);
            this.BHapus.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.BHapus.Symbol = "";
            this.BHapus.SymbolSize = 14F;
            this.BHapus.TabIndex = 4;
            this.BHapus.TextColor = System.Drawing.Color.AliceBlue;
            this.BHapus.Tooltip = "Hapus data";
            this.BHapus.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // BUbah
            // 
            this.BUbah.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BUbah.AntiAlias = true;
            this.BUbah.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.BUbah.EnableMarkup = false;
            this.BUbah.Location = new System.Drawing.Point(88, 12);
            this.BUbah.Margin = new System.Windows.Forms.Padding(5);
            this.BUbah.Name = "BUbah";
            this.BUbah.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.BUbah.Size = new System.Drawing.Size(28, 28);
            this.BUbah.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.BUbah.Symbol = "";
            this.BUbah.SymbolSize = 14F;
            this.BUbah.TabIndex = 4;
            this.BUbah.TextColor = System.Drawing.Color.AliceBlue;
            this.BUbah.Tooltip = "Ubah data";
            this.BUbah.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // BDetail
            // 
            this.BDetail.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BDetail.AntiAlias = true;
            this.BDetail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.BDetail.EnableMarkup = false;
            this.BDetail.Location = new System.Drawing.Point(133, 12);
            this.BDetail.Margin = new System.Windows.Forms.Padding(5);
            this.BDetail.Name = "BDetail";
            this.BDetail.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.BDetail.Size = new System.Drawing.Size(28, 28);
            this.BDetail.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.BDetail.Symbol = "";
            this.BDetail.SymbolSize = 14F;
            this.BDetail.TabIndex = 4;
            this.BDetail.TextColor = System.Drawing.Color.AliceBlue;
            this.BDetail.Tooltip = "Lihat detail laporan";
            this.BDetail.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // BTambah
            // 
            this.BTambah.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BTambah.AntiAlias = true;
            this.BTambah.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.BTambah.EnableMarkup = false;
            this.BTambah.Location = new System.Drawing.Point(12, 12);
            this.BTambah.Margin = new System.Windows.Forms.Padding(5);
            this.BTambah.Name = "BTambah";
            this.BTambah.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.BTambah.Size = new System.Drawing.Size(28, 28);
            this.BTambah.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.BTambah.Symbol = "";
            this.BTambah.SymbolSize = 14F;
            this.BTambah.TabIndex = 4;
            this.BTambah.TextColor = System.Drawing.Color.AliceBlue;
            this.BTambah.Tooltip = "Tambah data";
            this.BTambah.Click += new System.EventHandler(this.BLPlgrDilaporkan_Click);
            // 
            // statusStrip3
            // 
            this.statusStrip3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusStrip3.AutoSize = false;
            this.statusStrip3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.statusStrip3.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip3.ForeColor = System.Drawing.Color.Black;
            this.statusStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSSLBanyakDataPiket,
            this.toolStripStatusLabel7,
            this.TSSLNimPiket,
            this.LDSPiket});
            this.statusStrip3.Location = new System.Drawing.Point(12, 494);
            this.statusStrip3.Name = "statusStrip3";
            this.statusStrip3.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip3.Size = new System.Drawing.Size(799, 22);
            this.statusStrip3.SizingGrip = false;
            this.statusStrip3.TabIndex = 5;
            this.statusStrip3.Text = "SSAnggota";
            // 
            // TSSLBanyakDataPiket
            // 
            this.TSSLBanyakDataPiket.Name = "TSSLBanyakDataPiket";
            this.TSSLBanyakDataPiket.Size = new System.Drawing.Size(14, 17);
            this.TSSLBanyakDataPiket.Text = "0";
            // 
            // toolStripStatusLabel7
            // 
            this.toolStripStatusLabel7.Name = "toolStripStatusLabel7";
            this.toolStripStatusLabel7.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel7.Text = " | ";
            // 
            // TSSLNimPiket
            // 
            this.TSSLNimPiket.Name = "TSSLNimPiket";
            this.TSSLNimPiket.Size = new System.Drawing.Size(10, 17);
            this.TSSLNimPiket.Text = " ";
            // 
            // LDSPiket
            // 
            this.LDSPiket.Name = "LDSPiket";
            this.LDSPiket.Size = new System.Drawing.Size(755, 17);
            this.LDSPiket.Spring = true;
            // 
            // Form_LaporanDilaporkan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(823, 527);
            this.Controls.Add(this.statusStrip3);
            this.Controls.Add(this.BDetail);
            this.Controls.Add(this.BUbah);
            this.Controls.Add(this.BHapus);
            this.Controls.Add(this.BTambah);
            this.Controls.Add(this.DGLaporanDilaporkan);
            this.DoubleBuffered = true;
            this.Name = "Form_LaporanDilaporkan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_LaporanDilaporkan";
            this.Load += new System.EventHandler(this.Form_LaporanDilaporkan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGLaporanDilaporkan)).EndInit();
            this.statusStrip3.ResumeLayout(false);
            this.statusStrip3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public DevComponents.DotNetBar.Controls.DataGridViewX DGLaporanDilaporkan;
        internal DevComponents.DotNetBar.ButtonX BHapus;
        internal DevComponents.DotNetBar.ButtonX BUbah;
        internal DevComponents.DotNetBar.ButtonX BDetail;
        internal DevComponents.DotNetBar.ButtonX BTambah;
        internal System.Windows.Forms.StatusStrip statusStrip3;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLBanyakDataPiket;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel7;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLNimPiket;
        internal System.Windows.Forms.ToolStripStatusLabel LDSPiket;
        private System.Windows.Forms.DataGridViewTextBoxColumn ldid;
        private System.Windows.Forms.DataGridViewTextBoxColumn lfid;
        private System.Windows.Forms.DataGridViewTextBoxColumn pgid;
        private System.Windows.Forms.DataGridViewTextBoxColumn waktu;
        private System.Windows.Forms.DataGridViewTextBoxColumn keluhan;
        private System.Windows.Forms.DataGridViewTextBoxColumn nama;
    }
}