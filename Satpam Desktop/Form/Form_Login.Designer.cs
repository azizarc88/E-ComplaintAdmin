﻿namespace Satpam_Desktop
{
    partial class Form_Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GBUserName = new System.Windows.Forms.GroupBox();
            this.TBEmail = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.GBPass = new System.Windows.Forms.GroupBox();
            this.TBPass = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.BLogin = new System.Windows.Forms.Button();
            this.BTutup = new System.Windows.Forms.Button();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.GBUserName.SuspendLayout();
            this.GBPass.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // GBUserName
            // 
            this.GBUserName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GBUserName.Controls.Add(this.TBEmail);
            this.GBUserName.ForeColor = System.Drawing.Color.Black;
            this.GBUserName.Location = new System.Drawing.Point(12, 12);
            this.GBUserName.Name = "GBUserName";
            this.GBUserName.Size = new System.Drawing.Size(182, 46);
            this.GBUserName.TabIndex = 15;
            this.GBUserName.TabStop = false;
            this.GBUserName.Text = "Email / No. Handphone";
            // 
            // TBEmail
            // 
            this.TBEmail.AutoSelectAll = true;
            this.TBEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBEmail.Border.Class = "TextBoxBorder";
            this.TBEmail.Border.CornerDiameter = 3;
            this.TBEmail.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBEmail.FocusHighlightColor = System.Drawing.Color.DeepSkyBlue;
            this.TBEmail.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBEmail, true);
            this.TBEmail.Location = new System.Drawing.Point(5, 17);
            this.TBEmail.MaxLength = 30;
            this.TBEmail.Name = "TBEmail";
            this.TBEmail.Size = new System.Drawing.Size(168, 22);
            this.TBEmail.TabIndex = 1;
            // 
            // GBPass
            // 
            this.GBPass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GBPass.Controls.Add(this.TBPass);
            this.GBPass.ForeColor = System.Drawing.Color.Black;
            this.GBPass.Location = new System.Drawing.Point(12, 63);
            this.GBPass.Name = "GBPass";
            this.GBPass.Size = new System.Drawing.Size(182, 48);
            this.GBPass.TabIndex = 16;
            this.GBPass.TabStop = false;
            this.GBPass.Text = "Kata Sandi";
            // 
            // TBPass
            // 
            this.TBPass.AutoSelectAll = true;
            this.TBPass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBPass.Border.Class = "TextBoxBorder";
            this.TBPass.Border.CornerDiameter = 3;
            this.TBPass.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBPass.FocusHighlightColor = System.Drawing.Color.DeepSkyBlue;
            this.TBPass.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBPass, true);
            this.TBPass.Location = new System.Drawing.Point(5, 19);
            this.TBPass.MaxLength = 30;
            this.TBPass.Name = "TBPass";
            this.TBPass.PasswordChar = '●';
            this.TBPass.Size = new System.Drawing.Size(168, 22);
            this.TBPass.TabIndex = 0;
            // 
            // BLogin
            // 
            this.BLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.BLogin.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BLogin.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.BLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkTurquoise;
            this.BLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue;
            this.BLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BLogin.ForeColor = System.Drawing.Color.Black;
            this.BLogin.Location = new System.Drawing.Point(117, 120);
            this.BLogin.Name = "BLogin";
            this.BLogin.Size = new System.Drawing.Size(75, 23);
            this.BLogin.TabIndex = 18;
            this.BLogin.Text = "Login";
            this.BLogin.UseVisualStyleBackColor = false;
            this.BLogin.Click += new System.EventHandler(this.BLogin_Click);
            // 
            // BTutup
            // 
            this.BTutup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTutup.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.BTutup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkTurquoise;
            this.BTutup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue;
            this.BTutup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTutup.ForeColor = System.Drawing.Color.Black;
            this.BTutup.Location = new System.Drawing.Point(12, 119);
            this.BTutup.Name = "BTutup";
            this.BTutup.Size = new System.Drawing.Size(75, 23);
            this.BTutup.TabIndex = 17;
            this.BTutup.Text = "Tutup";
            this.BTutup.UseVisualStyleBackColor = false;
            this.BTutup.Click += new System.EventHandler(this.BTutup_Click);
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242))))), System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183))))));
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.pictureBox2.ForeColor = System.Drawing.Color.Black;
            this.pictureBox2.Image = global::Satpam_Desktop.Properties.Resources.SATPAMIcon2__Custom_;
            this.pictureBox2.Location = new System.Drawing.Point(214, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(137, 131);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 20;
            this.pictureBox2.TabStop = false;
            // 
            // Form_Login
            // 
            this.AcceptButton = this.BLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BTutup;
            this.ClientSize = new System.Drawing.Size(358, 152);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.GBUserName);
            this.Controls.Add(this.GBPass);
            this.Controls.Add(this.BLogin);
            this.Controls.Add(this.BTutup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form_Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login Satpam";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Login_FormClosing);
            this.Load += new System.EventHandler(this.Form_Login_Load);
            this.GBUserName.ResumeLayout(false);
            this.GBPass.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox GBUserName;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
        internal System.Windows.Forms.GroupBox GBPass;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBPass;
        internal System.Windows.Forms.Button BLogin;
        internal System.Windows.Forms.Button BTutup;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBEmail;
        internal System.Windows.Forms.PictureBox pictureBox2;
    }
}

