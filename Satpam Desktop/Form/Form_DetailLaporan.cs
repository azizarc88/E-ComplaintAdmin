﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Satpam_Desktop.Class;
using System.Collections.Specialized;

namespace Satpam_Desktop.Form
{
    public partial class Form_DetailLaporan : DevComponents.DotNetBar.Metro.MetroForm
    {
        private Bagian bagian;
        private string id = "";
        RESTService rs = new RESTService();
        DataGridManager DGM = new DataGridManager();
        private string url;
        private bool bolehhapus = true;
        private string lat, ling;

        public enum Bagian
        {
            Fasilitas = 0,
            Pelanggaran = 1
        }

        public Form_DetailLaporan(Bagian bagian, string id)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
            this.bagian = bagian;
            this.id = id;
            rs.RequestComplete += Rs_RequestComplete;

            if (bagian == Bagian.Fasilitas)
            {
                this.Text = "Detail Laporan Fasilitas";
                if (Program.claporanfas == false)
                {
                    bolehhapus = false;
                }
            }
            else if (bagian == Bagian.Pelanggaran)
            {
                this.Text = "Detail Laporan Pelanggaran";
                if (Program.claporanplgr == false)
                {
                    bolehhapus = false;
                }
            }
        }

        private void Segarkan()
        {
            if (bagian == Bagian.Fasilitas)
            {
                rs.MakeRequestAsync("ambil_feed_laporanfas.php?lfid=" + id, null, "Fasilitas");
                DGM.IsiDG(DGKomentar, "Komentar Fasilitas", id);
            }
            else
            {
                rs.MakeRequestAsync("ambil_feed_laporanplgr.php?lpid=" + id, null, "Pelanggaran");
                DGM.IsiDG(DGKomentar, "Komentar Pelanggaran", id);
            }
        }

        private void Rs_RequestComplete(object sender, RequestCompleteEventArgs e)
        {
            string hasil = rs.GetString(e.Response, "hasil");
            if (e.Bagian == "Fasilitas" || e.Bagian == "Pelanggaran")
            {
                if (hasil != "GAGAL")
                {
                    LNama.Text = rs.GetString(e.Response, "n");
                    DateTime date = DateTime.Parse(rs.GetString(e.Response, "h"));
                    LWaktu.Text = date.ToLongDateString() + " " + date.ToLongTimeString();
                    TBStatus.Text = rs.GetString(e.Response, "c");
                    this.url = rs.GetString(e.Response, "b");
                    PBFoto.LoadAsync(url);
                    if (rs.GetString(e.Response, "f") != "")
                    {
                        LLokasi.Text = rs.GetString(e.Response, "f");
                    }
                    else
                    {
                        LLokasi.Text = rs.GetString(e.Response, "d") + ", " + rs.GetString(e.Response, "e");
                    }

                    lat = rs.GetString(e.Response, "d");
                    ling = rs.GetString(e.Response, "e");

                    LInformasi.Text = rs.GetString(e.Response, "l") + " Dukungan  -  " + rs.GetString(e.Response, "m") + " Komentar";
                }
            }
            else if (e.Bagian == "Komen Fasilitas")
            {
                if (hasil == "SUKSES")
                {
                    Segarkan();
                    TBKomentar.Text = "";
                }
                else
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "Batal Komen Fasilitas")
            {
                if (hasil == "SUKSES")
                {
                    Segarkan();
                }
                else if (hasil == "GAGAL")
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
                else
                {
                    ToastNotification.Show(this, e.Response, null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "Komen Pelanggaran")
            {
                if (hasil == "SUKSES")
                {
                    Segarkan();
                    TBKomentar.Text = "";
                }
                else
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "Batal Komen Pelanggaran")
            {
                if (hasil == "SUKSES")
                {
                    Segarkan();
                }
                else if (hasil == "GAGAL")
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
                else
                {
                    ToastNotification.Show(this, e.Response, null, 3000, eToastGlowColor.Red);
                }
            }
        }

        private void Form_DetailLaporan_Load(object sender, EventArgs e)
        {
            LNama.BackColor = Color.White;
            LWaktu.BackColor = Color.White;
            LInformasi.BackColor = Color.FromArgb(250, 251, 251);
            TBStatus.BackColor = Color.White;
            PBFoto.BackColor = Color.White;
            TBKomentar.BackColor = Color.White;
            LLokasi.BackColor = Color.FromArgb(250, 251, 251);

            Segarkan();
        }

        private void Form_DetailLaporan_Shown(object sender, EventArgs e)
        {
            TBKomentar.Focus();
        }

        bool sudahtampil = false;
        Form_TampilFoto form;

        private void PBFoto_Click(object sender, EventArgs e)
        {
            if (!sudahtampil)
            {
                string temp = url.Substring(0, url.LastIndexOf("/") + 1);
                temp += "Original/" + url.Substring(url.LastIndexOf("/") + 1);
                Form_TampilFoto f = new Form_TampilFoto(temp);
                f.FormClosed += F_FormClosed;
                f.Show();
                sudahtampil = true;
                form = f;
            }
            else
            {
                if (form.WindowState == FormWindowState.Normal)
                {
                    form.BringToFront();
                }
                else if (form.WindowState == FormWindowState.Minimized)
                {
                    form.WindowState = FormWindowState.Normal;
                }
            }
        }

        private void F_FormClosed(object sender, FormClosedEventArgs e)
        {
            sudahtampil = false;
        }

        private void BKirimKomen_Click(object sender, EventArgs e)
        {
            if (bagian == Bagian.Fasilitas)
            {
                if (TBKomentar.Text != "")
                {
                    NameValueCollection formData = new NameValueCollection();
                    formData["oleh"] = Program.Pgid;
                    formData["untuk"] = id;
                    formData["komentar"] = TBKomentar.Text;
                    formData["waktu"] = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
                    rs.MakeRequestAsync("komen_fasilitas.php", formData, "Komen Fasilitas");
                }
                else
                {
                    ToastNotification.Show(this, "Isi komentar terlebih dahulu", null, 3000, eToastGlowColor.Orange);
                    TBKomentar.Focus();
                }
            }
            else if (bagian == Bagian.Pelanggaran)
            {
                if (TBKomentar.Text != "")
                {
                    NameValueCollection formData = new NameValueCollection();
                    formData["oleh"] = Program.Pgid;
                    formData["untuk"] = id;
                    formData["komentar"] = TBKomentar.Text;
                    formData["waktu"] = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
                    rs.MakeRequestAsync("komen_pelanggaran.php", formData, "Komen Pelanggaran");
                }
                else
                {
                    ToastNotification.Show(this, "Isi komentar terlebih dahulu", null, 3000, eToastGlowColor.Orange);
                    TBKomentar.Focus();
                }
            }
        }

        private void Form_DetailLaporan_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (form != null)
            {
                form.Close();
            }

            if (form2 != null)
            {
                form2.Close();
            }
        }

        private void DGKomentar_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex % 4 == 0 && e.ColumnIndex == 3)
            {
                if (bolehhapus)
                {
                    if (MessageBoxEx.Show(this, "Apa Anda yakin ingin menghapus komentar ini ?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (bagian == Bagian.Fasilitas)
                        {
                            DGKomentar.Visible = false;
                            NameValueCollection formData = new NameValueCollection();
                            formData["kmid"] = DGKomentar.CurrentRow.Cells[0].Value.ToString();
                            formData["untuk"] = id;
                            rs.MakeRequestAsync("batal_komen_fasilitas.php", formData, "Batal Komen Fasilitas");
                        }
                        else if (bagian == Bagian.Pelanggaran)
                        {
                            DGKomentar.Visible = false;
                            NameValueCollection formData = new NameValueCollection();
                            formData["kmid"] = DGKomentar.CurrentRow.Cells[0].Value.ToString();
                            formData["untuk"] = id;
                            rs.MakeRequestAsync("batal_komen_pelanggaran.php", formData, "Batal Komen Pelanggaran");
                        }
                    }
                }
                else
                {
                    if (DGKomentar.CurrentRow.Cells[1].Value.ToString() == "1")
                    {
                        if (MessageBoxEx.Show(this, "Apa Anda yakin ingin menghapus komentar ini ?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            if (bagian == Bagian.Fasilitas)
                            {
                                DGKomentar.Visible = false;
                                NameValueCollection formData = new NameValueCollection();
                                formData["kmid"] = DGKomentar.CurrentRow.Cells[0].Value.ToString();
                                formData["untuk"] = id;
                                rs.MakeRequestAsync("batal_komen_fasilitas.php", formData, "Batal Komen Fasilitas");
                            }
                            else if (bagian == Bagian.Pelanggaran)
                            {
                                DGKomentar.Visible = false;
                                NameValueCollection formData = new NameValueCollection();
                                formData["kmid"] = DGKomentar.CurrentRow.Cells[0].Value.ToString();
                                formData["untuk"] = id;
                                rs.MakeRequestAsync("batal_komen_pelanggaran.php", formData, "Batal Komen Pelanggaran");
                            }
                        }
                    }
                    else
                    {
                        ToastNotification.Show(this, "Anda hanya berhak menghapus komentar Anda sendiri", null, 3000, eToastGlowColor.Orange);
                    }
                }
            }
        }

        Form_TampilPendukung form2;
        bool sudahtampil2 = false;

        private void LInformasi_Click(object sender, EventArgs e)
        {
            string tabel;

            if (bagian == Bagian.Fasilitas)
            {
                tabel = "dukunganfasilitas";
            }
            else
            {
                tabel = "dukunganpelanggaran";
            }

            if (!sudahtampil2)
            {
                Form_TampilPendukung f = new Form_TampilPendukung("ambil_pendukung.php?lfid=" + id + "&tabel=" + tabel);
                f.Show();
                f.FormClosed += F_FormClosed1;
                form2 = f;
                sudahtampil2 = true;
            }
            else
            {
                if (form2.WindowState == FormWindowState.Normal)
                {
                    form2.BringToFront();
                }
                else if (form.WindowState == FormWindowState.Minimized)
                {
                    form2.WindowState = FormWindowState.Normal;
                }
            }
        }

        private void F_FormClosed1(object sender, FormClosedEventArgs e)
        {
            sudahtampil2 = false;
        }

        private void LLokasi_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.google.com/maps/place/" + lat + "," + ling + "/");
        }
    }
}
