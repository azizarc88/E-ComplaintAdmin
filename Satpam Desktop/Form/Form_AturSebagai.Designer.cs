﻿namespace Satpam_Desktop.Form
{
    partial class Form_AturSebagai
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelX2 = new DevComponents.DotNetBar.LabelX();
            this.TBSebagai = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.CBPanel = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.CBPengguna = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.CBLaporanFas = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.CBSebagai = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.CBLaporanKmt = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.CBLaporanPlgr = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.BPrev = new DevComponents.DotNetBar.ButtonX();
            this.BNext = new DevComponents.DotNetBar.ButtonX();
            this.BSimpan = new DevComponents.DotNetBar.ButtonX();
            this.CBOtoClose = new DevComponents.DotNetBar.CheckBoxItem();
            this.BTutup = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LabelX2
            // 
            this.LabelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX2.ForeColor = System.Drawing.Color.Black;
            this.LabelX2.Location = new System.Drawing.Point(7, 12);
            this.LabelX2.Name = "LabelX2";
            this.LabelX2.Size = new System.Drawing.Size(100, 23);
            this.LabelX2.TabIndex = 79;
            this.LabelX2.Text = "Sebagai";
            // 
            // TBSebagai
            // 
            this.TBSebagai.AutoSelectAll = true;
            this.TBSebagai.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBSebagai.Border.Class = "TextBoxBorder";
            this.TBSebagai.Border.CornerDiameter = 2;
            this.TBSebagai.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBSebagai.ForeColor = System.Drawing.Color.Black;
            this.TBSebagai.Location = new System.Drawing.Point(107, 12);
            this.TBSebagai.MaxLength = 30;
            this.TBSebagai.Name = "TBSebagai";
            this.TBSebagai.Size = new System.Drawing.Size(206, 22);
            this.TBSebagai.TabIndex = 78;
            // 
            // CBPanel
            // 
            this.CBPanel.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.CBPanel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CBPanel.Location = new System.Drawing.Point(5, 5);
            this.CBPanel.Margin = new System.Windows.Forms.Padding(5);
            this.CBPanel.Name = "CBPanel";
            this.CBPanel.Size = new System.Drawing.Size(143, 23);
            this.CBPanel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBPanel.TabIndex = 80;
            this.CBPanel.Text = "C_Panel";
            // 
            // CBPengguna
            // 
            this.CBPengguna.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.CBPengguna.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CBPengguna.Location = new System.Drawing.Point(5, 38);
            this.CBPengguna.Margin = new System.Windows.Forms.Padding(5);
            this.CBPengguna.Name = "CBPengguna";
            this.CBPengguna.Size = new System.Drawing.Size(143, 23);
            this.CBPengguna.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBPengguna.TabIndex = 80;
            this.CBPengguna.Text = "C_Pengguna";
            // 
            // CBLaporanFas
            // 
            this.CBLaporanFas.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.CBLaporanFas.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CBLaporanFas.Location = new System.Drawing.Point(5, 71);
            this.CBLaporanFas.Margin = new System.Windows.Forms.Padding(5);
            this.CBLaporanFas.Name = "CBLaporanFas";
            this.CBLaporanFas.Size = new System.Drawing.Size(143, 23);
            this.CBLaporanFas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBLaporanFas.TabIndex = 80;
            this.CBLaporanFas.Text = "C_LaporanFas";
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.CBLaporanPlgr);
            this.groupPanel1.Controls.Add(this.CBLaporanKmt);
            this.groupPanel1.Controls.Add(this.CBPanel);
            this.groupPanel1.Controls.Add(this.CBSebagai);
            this.groupPanel1.Controls.Add(this.CBPengguna);
            this.groupPanel1.Controls.Add(this.CBLaporanFas);
            this.groupPanel1.Location = new System.Drawing.Point(7, 41);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(306, 124);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 81;
            this.groupPanel1.Text = "Pengaturan";
            // 
            // CBSebagai
            // 
            this.CBSebagai.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.CBSebagai.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CBSebagai.Location = new System.Drawing.Point(152, 71);
            this.CBSebagai.Margin = new System.Windows.Forms.Padding(5);
            this.CBSebagai.Name = "CBSebagai";
            this.CBSebagai.Size = new System.Drawing.Size(143, 23);
            this.CBSebagai.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBSebagai.TabIndex = 80;
            this.CBSebagai.Text = "C_Sebagai";
            // 
            // CBLaporanKmt
            // 
            this.CBLaporanKmt.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.CBLaporanKmt.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CBLaporanKmt.Location = new System.Drawing.Point(152, 38);
            this.CBLaporanKmt.Margin = new System.Windows.Forms.Padding(5);
            this.CBLaporanKmt.Name = "CBLaporanKmt";
            this.CBLaporanKmt.Size = new System.Drawing.Size(143, 23);
            this.CBLaporanKmt.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBLaporanKmt.TabIndex = 80;
            this.CBLaporanKmt.Text = "C_LaporanKmt";
            // 
            // CBLaporanPlgr
            // 
            this.CBLaporanPlgr.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.CBLaporanPlgr.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CBLaporanPlgr.Location = new System.Drawing.Point(152, 5);
            this.CBLaporanPlgr.Margin = new System.Windows.Forms.Padding(5);
            this.CBLaporanPlgr.Name = "CBLaporanPlgr";
            this.CBLaporanPlgr.Size = new System.Drawing.Size(143, 23);
            this.CBLaporanPlgr.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBLaporanPlgr.TabIndex = 80;
            this.CBLaporanPlgr.Text = "C_LaporanPlgr";
            // 
            // BPrev
            // 
            this.BPrev.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPrev.Enabled = false;
            this.BPrev.Location = new System.Drawing.Point(5, 174);
            this.BPrev.Name = "BPrev";
            this.BPrev.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BPrev.Size = new System.Drawing.Size(22, 34);
            this.BPrev.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPrev.Symbol = "";
            this.BPrev.SymbolSize = 15F;
            this.BPrev.TabIndex = 82;
            this.BPrev.Click += new System.EventHandler(this.BPrev_Click);
            // 
            // BNext
            // 
            this.BNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BNext.Enabled = false;
            this.BNext.Location = new System.Drawing.Point(291, 174);
            this.BNext.Name = "BNext";
            this.BNext.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BNext.Size = new System.Drawing.Size(22, 34);
            this.BNext.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BNext.Symbol = "";
            this.BNext.SymbolSize = 15F;
            this.BNext.TabIndex = 85;
            this.BNext.Click += new System.EventHandler(this.BNext_Click);
            // 
            // BSimpan
            // 
            this.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BSimpan.Location = new System.Drawing.Point(163, 174);
            this.BSimpan.Name = "BSimpan";
            this.BSimpan.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BSimpan.Size = new System.Drawing.Size(126, 34);
            this.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BSimpan.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.CBOtoClose});
            this.BSimpan.SubItemsExpandWidth = 18;
            this.BSimpan.TabIndex = 84;
            this.BSimpan.Text = "Tambah";
            this.BSimpan.Click += new System.EventHandler(this.BSimpan_Click);
            // 
            // CBOtoClose
            // 
            this.CBOtoClose.Checked = true;
            this.CBOtoClose.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBOtoClose.GlobalItem = false;
            this.CBOtoClose.Name = "CBOtoClose";
            this.CBOtoClose.Stretch = true;
            this.CBOtoClose.Text = "Tutup otomatis";
            // 
            // BTutup
            // 
            this.BTutup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTutup.Location = new System.Drawing.Point(29, 174);
            this.BTutup.Name = "BTutup";
            this.BTutup.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BTutup.Size = new System.Drawing.Size(126, 34);
            this.BTutup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BTutup.TabIndex = 83;
            this.BTutup.Text = "Batal";
            // 
            // Form_AturSebagai
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 217);
            this.Controls.Add(this.BPrev);
            this.Controls.Add(this.BNext);
            this.Controls.Add(this.BSimpan);
            this.Controls.Add(this.BTutup);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.LabelX2);
            this.Controls.Add(this.TBSebagai);
            this.DoubleBuffered = true;
            this.Name = "Form_AturSebagai";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_AturSebagai";
            this.groupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.LabelX LabelX2;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBSebagai;
        private DevComponents.DotNetBar.Controls.CheckBoxX CBPanel;
        private DevComponents.DotNetBar.Controls.CheckBoxX CBPengguna;
        private DevComponents.DotNetBar.Controls.CheckBoxX CBLaporanFas;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.CheckBoxX CBLaporanPlgr;
        private DevComponents.DotNetBar.Controls.CheckBoxX CBLaporanKmt;
        private DevComponents.DotNetBar.Controls.CheckBoxX CBSebagai;
        internal DevComponents.DotNetBar.ButtonX BPrev;
        internal DevComponents.DotNetBar.ButtonX BNext;
        internal DevComponents.DotNetBar.ButtonX BSimpan;
        internal DevComponents.DotNetBar.CheckBoxItem CBOtoClose;
        internal DevComponents.DotNetBar.ButtonX BTutup;
    }
}