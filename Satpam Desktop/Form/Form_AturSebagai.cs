﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Satpam_Desktop.Class;
using System.Collections.Specialized;
using DevComponents.DotNetBar.Controls;

namespace Satpam_Desktop.Form
{
    public partial class Form_AturSebagai : DevComponents.DotNetBar.Metro.MetroForm
    {
        public int sem { set; get; }
        public int max { set; get; }
        private RESTService rs = new RESTService();
        private string[] dataubah;
        private Aksi aksi;
        private DataGridView datagrid;
        private string sbid;

        public delegate void DataGridSegarkanHandler(object sender, DataGridSegarkanEventArgs e);
        public event DataGridSegarkanHandler DataGridSegarkan;

        public enum Aksi
        {
            Tambah = 0,
            Ubah = 1
        }

        public Form_AturSebagai(Aksi aksi, string[] dataubah = null, DataGridView datagrid = null)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
            this.aksi = aksi;
            this.dataubah = dataubah;
            this.datagrid = datagrid;

            if (aksi == Aksi.Tambah)
            {
                this.Text = "Tambah Pengaturan Pengguna";
            }
            else
            {
                this.Text = "Ubah Pengaturan Pengguna";
                BSimpan.Text = "Ubah";

                MasukkanData();
            }
            rs.RequestComplete += Rs_RequestComplete;
        }

        private void Rs_RequestComplete(object sender, RequestCompleteEventArgs e)
        {
            string hasil = rs.GetString(e.Response, "hasil");

            if (hasil == "SUKSES")
            {
                DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Pengaturan Sebagai");
                DataGridSegarkan(this, args);

                if (CBOtoClose.Checked == true)
                {
                    this.Close();
                    this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                    return;
                }

                if (aksi == Aksi.Tambah)
                {
                    ToastNotification.Show(this, "Data telah ditambahkan", null, 3000, eToastGlowColor.Blue);
                }
                else
                {
                    ToastNotification.Show(this, "Data telah diubah", null, 3000, eToastGlowColor.Blue);
                }
                Bersihkan();
                TBSebagai.Focus();
            }
            else if (hasil == "GAGAL")
            {
                ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
            }
        }

        private void MasukkanData()
        {
            sbid = dataubah[0];
            TBSebagai.Text = dataubah[1];
            CBPanel.Checked = ConvertToBoolean(dataubah[2]);
            CBPengguna.Checked = ConvertToBoolean(dataubah[3]);
            CBLaporanFas.Checked = ConvertToBoolean(dataubah[4]);
            CBLaporanPlgr.Checked = ConvertToBoolean(dataubah[5]);
            CBLaporanKmt.Checked = ConvertToBoolean(dataubah[6]);
            CBSebagai.Checked = ConvertToBoolean(dataubah[7]);
        }

        private void Bersihkan()
        {
            TBSebagai.Text = "";
            CBPanel.Checked = false;
            CBPengguna.Checked = false;
            CBLaporanFas.Checked = false;
            CBLaporanPlgr.Checked = false;
            CBLaporanKmt.Checked = false;
            CBSebagai.Checked = false;
        }

        private string ConvertToString(CheckBoxX checkbox)
        {
            string hasil = "0";
            if (checkbox.Checked == true)
            {
                hasil = "1";
            }
            return hasil;
        }

        private bool ConvertToBoolean(string nilai)
        {
            bool hasil = false;
            if (nilai == "1")
            {
                hasil = true;
            }
            return hasil;
        }

        private void proses()
        {
            if (aksi == Aksi.Tambah)
            {
                NameValueCollection formData = new NameValueCollection();
                formData["sebagai"] = TBSebagai.Text;
                formData["cpanel"] = ConvertToString(CBPanel);
                formData["cpengguna"] = ConvertToString(CBPengguna);
                formData["claporanfas"] = ConvertToString(CBLaporanFas);
                formData["claporanplgr"] = ConvertToString(CBLaporanPlgr);
                formData["claporankmt"] = ConvertToString(CBLaporanKmt);
                formData["csebagai"] = ConvertToString(CBSebagai);
                rs.MakeRequestAsync("tambah_sebagai.php", formData, "SebagaiTambah");
            }
            else if (aksi == Aksi.Ubah)
            {
                NameValueCollection formData = new NameValueCollection();
                formData["sbid"] = sbid;
                formData["sebagai"] = TBSebagai.Text;
                formData["cpanel"] = ConvertToString(CBPanel);
                formData["cpengguna"] = ConvertToString(CBPengguna);
                formData["claporanfas"] = ConvertToString(CBLaporanFas);
                formData["claporanplgr"] = ConvertToString(CBLaporanPlgr);
                formData["claporankmt"] = ConvertToString(CBLaporanKmt);
                formData["csebagai"] = ConvertToString(CBSebagai);
                rs.MakeRequestAsync("ubah_sebagai.php", formData, "UbahTambah");
            }
        }

        private void BPrev_Click(object sender, EventArgs e)
        {
            sem--;
            if (sem < 0)
            {
                sem = max;
            }
            TBSebagai.Focus();

            string[] dataubah = new string[datagrid.Rows[sem].Cells.Count];
            int i = 0;
            foreach (DataGridViewCell cell in datagrid.Rows[sem].Cells)
            {
                dataubah[i] = cell.Value.ToString();
                i++;
            }
            this.dataubah = dataubah;
            MasukkanData();
        }

        private void BNext_Click(object sender, EventArgs e)
        {
            sem++;
            if (sem > max)
            {
                sem = 0;
            }
            TBSebagai.Focus();

            string[] dataubah = new string[datagrid.Rows[sem].Cells.Count];
            int i = 0;
            foreach (DataGridViewCell cell in datagrid.Rows[sem].Cells)
            {
                dataubah[i] = cell.Value.ToString();
                i++;
            }
            this.dataubah = dataubah;
            MasukkanData();
        }

        private void BSimpan_Click(object sender, EventArgs e)
        {
            proses();
        }
    }
}
