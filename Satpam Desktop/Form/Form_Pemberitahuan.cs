﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using AnimasiUI;

namespace Satpam_Desktop.Form
{
    public partial class Form_Pemberitahuan : DevComponents.DotNetBar.Metro.MetroForm
    {
        private Bagian bagian;
        private string id;

        public enum Bagian
        {
            Fasilitas = 0,
            Pelanggaran = 1
        }

        protected override System.Windows.Forms.CreateParams CreateParams
        {
            get
            {
                const int WS_EX_NOACTIVATE = 0x8000000;
                const int WS_EX_TOOLWINDOW = 0x80;
                const int WS_EX_TOPMOST = 0x8;
                const int WS_EX_WINDOWEDGE = 0x100;
                CreateParams Result = default(CreateParams);
                Result = base.CreateParams;
                Result.ExStyle = Result.ExStyle | WS_EX_NOACTIVATE | WS_EX_TOOLWINDOW | WS_EX_TOPMOST | WS_EX_WINDOWEDGE;
                return Result;
            }
        }

        public void TentukanBaris()
        {
            int batas = TBInfo.GetLineFromCharIndex(TBInfo.Text.Length);
            int h = TBInfo.Font.Height;
            int tinggi = 60;
            int tinggiq = 21;
            int postinggi = 62;

            for (int i = 0; i < batas; i++)
            {
                tinggi += h;
                tinggiq += h;
                postinggi += h;
            }

            this.Height = tinggi;
            this.TBInfo.Height = tinggiq;

            this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Width, 50);
        }

        public Form_Pemberitahuan(string info, string alamat, string prioritas, Bagian bagian, string id)
        {
            InitializeComponent();
            TBInfo.Text = info;
            if (alamat != "")
            {
                LAlamat.Text = alamat + " - Satpam";
            }
            else
            {
                LAlamat.Text = "Satpam";
            }
            LPrioritas.Text = prioritas;
            this.bagian = bagian;
            this.id = id;
        }

        private void Form_Pemberitahuan_Load(object sender, EventArgs e)
        {
            TentukanBaris();
            Class_Animasi.Animasikan(this.Handle, 300, Class_Animasi.GayaAnimasi.Menyatu, Class_Animasi.OpsiGaya.Buka);
            this.Show();
            System.Media.SoundPlayer player = new System.Media.SoundPlayer(Application.StartupPath + "\\Resources\\LYNC_presence.wav");
            player.Play();
            timer1.Enabled = true;
        }

        private void BHide_Click(object sender, EventArgs e)
        {
            Class_Animasi.Animasikan(this.Handle, 400, Class_Animasi.GayaAnimasi.Menyatu, Class_Animasi.OpsiGaya.Tutup);
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Class_Animasi.Animasikan(this.Handle, 400, Class_Animasi.GayaAnimasi.Menyatu, Class_Animasi.OpsiGaya.Tutup);
            this.Close();
        }

        private void BDetail_Click(object sender, EventArgs e)
        {
            if (bagian == Bagian.Fasilitas)
            {
                Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Fasilitas, id);
                f.Show();
            }
            else
            {
                Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Pelanggaran, id);
                f.Show();
            }
            Class_Animasi.Animasikan(this.Handle, 400, Class_Animasi.GayaAnimasi.Menyatu, Class_Animasi.OpsiGaya.Tutup);
            this.Close();
        }

        private void TBInfo_Click(object sender, EventArgs e)
        {
            if (bagian == Bagian.Fasilitas)
            {
                Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Fasilitas, id);
                f.Show();
            }
            else
            {
                Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Pelanggaran, id);
                f.Show();
            }
            Class_Animasi.Animasikan(this.Handle, 400, Class_Animasi.GayaAnimasi.Menyatu, Class_Animasi.OpsiGaya.Tutup);
            this.Close();
        }
    }
}
