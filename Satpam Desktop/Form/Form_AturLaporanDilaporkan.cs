﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Satpam_Desktop.Class;
using System.Collections.Specialized;

namespace Satpam_Desktop.Form
{
    public partial class Form_AturLaporanDilaporkan : DevComponents.DotNetBar.Metro.MetroForm
    {
        public int sem { set; get; }
        public int max { set; get; }
        private Aksi aksi;
        private string ldid = "tidak ada";
        private string lid = "tidak ada";
        private string pgid = "tidak ada";
        private string[] dataubah;
        private DataGridView datagrid;
        RESTService rs = new RESTService();

        public delegate void DataGridSegarkanHandler(object sender, DataGridSegarkanEventArgs e);
        public event DataGridSegarkanHandler DataGridSegarkan;

        public enum Aksi
        {
            TambahFasilitas = 0,
            UbahFasilitas = 1,
            TambahPelanggaran = 2,
            UbahPelanggaran = 3
        }

        private void Bersihkan()
        {
            ldid = "tidak ada";
            lid = "tidak ada";
            pgid = "tidak ada";
            LNamaLaporan.Text = "Belum ditentukan";
            LNamaPengguna.Text = "Belum ditentukan";
        }

        private void MasukkanData()
        {
            ldid = dataubah[0];
            lid = dataubah[1];
            pgid = dataubah[2];
            LNamaLaporan.Text = dataubah[4];
            LNamaPengguna.Text = dataubah[5];
            DTDilaporkanPada.Text = dataubah[3];
        }

        private bool isAllValid()
        {
            bool valid = true;
            string pesan = "";

            if (pgid == "tidak ada")
            {
                pesan = "Silakan pilih pengguna dahulu";
                BPilihPengguna.Focus();
                valid = false;
            }
            else if (lid == "tidak ada")
            {
                pesan = "Silakan pilih laporan dahulu";
                BPilihLaporan.Focus();
                valid = false;
            }
            else if (ldid == "tidak ada" && (aksi == Aksi.UbahFasilitas || aksi == Aksi.UbahPelanggaran))
            {
                pesan = "ldid ?";
                BPilihLaporan.Focus();
                valid = false;
            }

            if (!valid)
            {
                ToastNotification.Show(this, pesan, null, 3000, eToastGlowColor.Orange);
            }

            return valid;
        }

        private void proses()
        {
            if (aksi == Aksi.TambahFasilitas)
            {
                if (isAllValid())
                {
                    NameValueCollection formData = new NameValueCollection();
                    formData["lfid"] = lid;
                    formData["waktu"] = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
                    formData["oleh"] = pgid;
                    rs.MakeRequestAsync("tambah_laporan_laporanfas.php", formData, "FTambah");
                }
            }
            else if (aksi == Aksi.UbahFasilitas)
            {
                if (isAllValid())
                {
                    NameValueCollection formData = new NameValueCollection();
                    formData["tabel"] = "laporanfasdilaporkan";
                    formData["id"] = ldid;
                    formData["namalid"] = "lfid";
                    formData["lid"] = lid;
                    formData["waktu"] = DTDilaporkanPada.Value.Year + "-" + DTDilaporkanPada.Value.Month + "-" + DTDilaporkanPada.Value.Day + " " + DTDilaporkanPada.Value.Hour + ":" + DTDilaporkanPada.Value.Minute + ":" + DTDilaporkanPada.Value.Second;
                    formData["oleh"] = pgid;
                    rs.MakeRequestAsync("ubah_laporan_laporan.php", formData, "FUbah");
                }
            }
            else if (aksi == Aksi.TambahPelanggaran)
            {
                if (isAllValid())
                {
                    NameValueCollection formData = new NameValueCollection();
                    formData["lpid"] = lid;
                    formData["waktu"] = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
                    formData["oleh"] = pgid;
                    rs.MakeRequestAsync("tambah_laporan_laporanplgr.php", formData, "PTambah");
                }
            }
            else if (aksi == Aksi.UbahPelanggaran)
            {
                if (isAllValid())
                {
                    NameValueCollection formData = new NameValueCollection();
                    formData["tabel"] = "laporanplgrdilaporkan";
                    formData["id"] = ldid;
                    formData["namalid"] = "lpid";
                    formData["lid"] = lid;
                    formData["waktu"] = DTDilaporkanPada.Value.Year + "-" + DTDilaporkanPada.Value.Month + "-" + DTDilaporkanPada.Value.Day + " " + DTDilaporkanPada.Value.Hour + ":" + DTDilaporkanPada.Value.Minute + ":" + DTDilaporkanPada.Value.Second;
                    formData["oleh"] = pgid;
                    rs.MakeRequestAsync("ubah_laporan_laporan.php", formData, "PUbah");
                }
            }
        }

        public Form_AturLaporanDilaporkan(Aksi aksi, string[] dataubah = null, DataGridView datagrid = null)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
            this.aksi = aksi;
            this.dataubah = dataubah;
            this.datagrid = datagrid;

            if (aksi == Aksi.TambahFasilitas)
            {
                this.Text = "Tambah Laporan Laporan Fasilitas";
                DTDilaporkanPada.Enabled = false;
            }
            else if (aksi == Aksi.UbahFasilitas)
            {
                this.Text = "Ubah Laporan Laporan Fasilitas";
                BSimpan.Text = "Ubah";

                MasukkanData();
            }
            else if (aksi == Aksi.TambahPelanggaran)
            {
                this.Text = "Tambah Laporan Laporan Pelanggaran";
                DTDilaporkanPada.Enabled = false;
            }
            else if (aksi == Aksi.UbahPelanggaran)
            {
                this.Text = "Ubah Laporan Laporan Pelanggaran";
                BSimpan.Text = "Ubah";

                MasukkanData();
            }
        }

        private void BPrev_Click(object sender, EventArgs e)
        {
            sem--;
            if (sem < 0)
            {
                sem = max;
            }
            BPilihLaporan.Focus();

            string[] dataubah = new string[datagrid.Rows[sem].Cells.Count];
            int i = 0;
            foreach (DataGridViewCell cell in datagrid.Rows[sem].Cells)
            {
                dataubah[i] = cell.Value.ToString();
                i++;
            }
            this.dataubah = dataubah;
            MasukkanData();
        }

        private void BNext_Click(object sender, EventArgs e)
        {
            sem++;
            if (sem > max)
            {
                sem = 0;
            }
            BPilihLaporan.Focus();

            string[] dataubah = new string[datagrid.Rows[sem].Cells.Count];
            int i = 0;
            foreach (DataGridViewCell cell in datagrid.Rows[sem].Cells)
            {
                dataubah[i] = cell.Value.ToString();
                i++;
            }
            this.dataubah = dataubah;
            MasukkanData();
        }

        private void BSimpan_Click(object sender, EventArgs e)
        {
            proses();
        }
        
        private void Form_AturLaporanDilaporkan_Load(object sender, EventArgs e)
        {
            rs.RequestComplete += Rs_RequestComplete;
        }

        private void Rs_RequestComplete(object sender, RequestCompleteEventArgs e)
        {
            string hasil = rs.GetString(e.Response, "hasil");
            
            if (e.Bagian == "FTambah")
            {
                if (hasil == "SUDAH ADA")
                {
                    ToastNotification.Show(this, "Data yang Anda masukkan sudah ada", null, 3000, eToastGlowColor.Orange);
                }
                else if (hasil == "SUKSES")
                {
                    DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Laporan Fasilitas");
                    DataGridSegarkan(this, args);

                    if (CBOtoClose.Checked == true)
                    {
                        this.Close();
                        this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                        return;
                    }

                    ToastNotification.Show(this, "Data telah ditambahkan", null, 3000, eToastGlowColor.Blue);
                    Bersihkan();
                    BPilihLaporan.Focus();
                }
                else if (hasil == "GAGAL")
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "FUbah")
            {
                if (hasil == "SUKSES")
                {
                    DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Laporan Fasilitas");
                    DataGridSegarkan(this, args);

                    if (CBOtoClose.Checked == true)
                    {
                        this.Close();
                        this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                        return;
                    }

                    ToastNotification.Show(this, "Data telah diubah", null, 3000, eToastGlowColor.Blue);
                    Bersihkan();
                    BPilihLaporan.Focus();
                }
                else if (hasil == "GAGAL")
                {

                }
            }
            else if (e.Bagian == "PTambah")
            {
                if (hasil == "SUDAH ADA")
                {
                    ToastNotification.Show(this, "Data yang Anda masukkan sudah ada", null, 3000, eToastGlowColor.Orange);
                }
                else if (hasil == "SUKSES")
                {
                    DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Laporan Fasilitas");
                    DataGridSegarkan(this, args);

                    if (CBOtoClose.Checked == true)
                    {
                        this.Close();
                        this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                        return;
                    }

                    ToastNotification.Show(this, "Data telah ditambahkan", null, 3000, eToastGlowColor.Blue);
                    Bersihkan();
                    BPilihLaporan.Focus();
                }
                else if (hasil == "GAGAL")
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "PUbah")
            {
                DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Laporan Fasilitas");
                DataGridSegarkan(this, args);

                if (CBOtoClose.Checked == true)
                {
                    this.Close();
                    this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                    return;
                }

                ToastNotification.Show(this, "Data telah diubah", null, 3000, eToastGlowColor.Blue);
                Bersihkan();
                BPilihLaporan.Focus();
            }
        }

        private void BPilihLaporan_Click(object sender, EventArgs e)
        {
            if (aksi == Aksi.TambahFasilitas || aksi == Aksi.UbahFasilitas)
            {
                Form_PilihLaporan f = new Form_PilihLaporan(Form_PilihLaporan.Bagian.Fasilitas);
                f.ItemTerpilih += F_ItemTerpilih;
                f.ShowDialog();
            }
            else if (aksi == Aksi.TambahPelanggaran || aksi == Aksi.UbahPelanggaran)
            {
                Form_PilihLaporan f = new Form_PilihLaporan(Form_PilihLaporan.Bagian.Pelanggaran);
                f.ItemTerpilih += F_ItemTerpilih;
                f.ShowDialog();
            }
        }

        private void F_ItemTerpilih(object sender, ItemTerpilihEventArgs e)
        {
            lid = e.Daftar[0];
            LNamaLaporan.Text = e.Daftar[2];
        }

        private void BPilihPengguna_Click(object sender, EventArgs e)
        {
            Form_PilihAnggota FrmPilihItem = new Form_PilihAnggota();
            FrmPilihItem.ItemTerpilih += FrmPilihItem_ItemTerpilih; ;
            FrmPilihItem.ShowDialog();
        }

        private void FrmPilihItem_ItemTerpilih(object sender, ItemTerpilihEventArgs e)
        {
            pgid = e.Daftar[0];
            LNamaPengguna.Text = e.Daftar[1];
        }
    }
}
