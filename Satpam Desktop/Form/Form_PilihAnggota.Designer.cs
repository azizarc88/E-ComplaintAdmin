﻿namespace Satpam_Desktop.Form
{
    partial class Form_PilihAnggota
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DGPengguna = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.NimDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NamaanggotaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KelasDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlamatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JkelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NohpDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tanggallahir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totallaporan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nohp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.password = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.blokir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TBCari = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.BBatal = new DevComponents.DotNetBar.ButtonX();
            this.BPilih = new DevComponents.DotNetBar.ButtonX();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.LContentDA = new System.Windows.Forms.Label();
            this.LCNimDA = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DGPengguna)).BeginInit();
            this.SuspendLayout();
            // 
            // DGPengguna
            // 
            this.DGPengguna.AllowUserToAddRows = false;
            this.DGPengguna.AllowUserToDeleteRows = false;
            this.DGPengguna.AllowUserToResizeRows = false;
            this.DGPengguna.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGPengguna.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGPengguna.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGPengguna.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGPengguna.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGPengguna.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGPengguna.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NimDataGridViewTextBoxColumn,
            this.NamaanggotaDataGridViewTextBoxColumn,
            this.KelasDataGridViewTextBoxColumn,
            this.AlamatDataGridViewTextBoxColumn,
            this.JkelDataGridViewTextBoxColumn,
            this.NohpDataGridViewTextBoxColumn,
            this.tanggallahir,
            this.totallaporan,
            this.nohp,
            this.email,
            this.password,
            this.blokir});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGPengguna.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGPengguna.EnableHeadersVisualStyles = false;
            this.DGPengguna.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGPengguna.Location = new System.Drawing.Point(12, 40);
            this.DGPengguna.Name = "DGPengguna";
            this.DGPengguna.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGPengguna.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGPengguna.RowHeadersVisible = false;
            this.DGPengguna.RowHeadersWidth = 80;
            this.DGPengguna.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGPengguna.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGPengguna.Size = new System.Drawing.Size(1040, 464);
            this.DGPengguna.TabIndex = 1;
            this.DGPengguna.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGPengguna_CellClick);
            this.DGPengguna.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGPengguna_CellContentClick);
            // 
            // NimDataGridViewTextBoxColumn
            // 
            this.NimDataGridViewTextBoxColumn.DataPropertyName = "pgid";
            this.NimDataGridViewTextBoxColumn.FillWeight = 40F;
            this.NimDataGridViewTextBoxColumn.HeaderText = "Pgid";
            this.NimDataGridViewTextBoxColumn.Name = "NimDataGridViewTextBoxColumn";
            this.NimDataGridViewTextBoxColumn.ReadOnly = true;
            this.NimDataGridViewTextBoxColumn.Visible = false;
            // 
            // NamaanggotaDataGridViewTextBoxColumn
            // 
            this.NamaanggotaDataGridViewTextBoxColumn.DataPropertyName = "nama_pengguna";
            this.NamaanggotaDataGridViewTextBoxColumn.HeaderText = "Nama Pengguna";
            this.NamaanggotaDataGridViewTextBoxColumn.Name = "NamaanggotaDataGridViewTextBoxColumn";
            this.NamaanggotaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // KelasDataGridViewTextBoxColumn
            // 
            this.KelasDataGridViewTextBoxColumn.DataPropertyName = "sebagai";
            this.KelasDataGridViewTextBoxColumn.FillWeight = 60F;
            this.KelasDataGridViewTextBoxColumn.HeaderText = "Sebagai";
            this.KelasDataGridViewTextBoxColumn.Name = "KelasDataGridViewTextBoxColumn";
            this.KelasDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // AlamatDataGridViewTextBoxColumn
            // 
            this.AlamatDataGridViewTextBoxColumn.DataPropertyName = "alamat";
            this.AlamatDataGridViewTextBoxColumn.HeaderText = "Alamat";
            this.AlamatDataGridViewTextBoxColumn.Name = "AlamatDataGridViewTextBoxColumn";
            this.AlamatDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // JkelDataGridViewTextBoxColumn
            // 
            this.JkelDataGridViewTextBoxColumn.DataPropertyName = "desa";
            this.JkelDataGridViewTextBoxColumn.FillWeight = 40F;
            this.JkelDataGridViewTextBoxColumn.HeaderText = "Desa";
            this.JkelDataGridViewTextBoxColumn.Name = "JkelDataGridViewTextBoxColumn";
            this.JkelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // NohpDataGridViewTextBoxColumn
            // 
            this.NohpDataGridViewTextBoxColumn.DataPropertyName = "kota";
            this.NohpDataGridViewTextBoxColumn.FillWeight = 60F;
            this.NohpDataGridViewTextBoxColumn.HeaderText = "Kota";
            this.NohpDataGridViewTextBoxColumn.Name = "NohpDataGridViewTextBoxColumn";
            this.NohpDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tanggallahir
            // 
            this.tanggallahir.FillWeight = 60F;
            this.tanggallahir.HeaderText = "Tanggal Lahir";
            this.tanggallahir.Name = "tanggallahir";
            this.tanggallahir.ReadOnly = true;
            // 
            // totallaporan
            // 
            this.totallaporan.FillWeight = 42F;
            this.totallaporan.HeaderText = "Total Laporan";
            this.totallaporan.Name = "totallaporan";
            this.totallaporan.ReadOnly = true;
            // 
            // nohp
            // 
            this.nohp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.nohp.FillWeight = 60F;
            this.nohp.HeaderText = "Handphone";
            this.nohp.Name = "nohp";
            this.nohp.ReadOnly = true;
            this.nohp.Width = 88;
            // 
            // email
            // 
            this.email.HeaderText = "Email";
            this.email.Name = "email";
            this.email.ReadOnly = true;
            // 
            // password
            // 
            this.password.HeaderText = "Password";
            this.password.Name = "password";
            this.password.ReadOnly = true;
            // 
            // blokir
            // 
            this.blokir.FillWeight = 30F;
            this.blokir.HeaderText = "Blokir";
            this.blokir.Name = "blokir";
            this.blokir.ReadOnly = true;
            // 
            // TBCari
            // 
            this.TBCari.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBCari.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBCari.Border.Class = "TextBoxBorder";
            this.TBCari.Border.CornerDiameter = 3;
            this.TBCari.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBCari.ForeColor = System.Drawing.Color.Black;
            this.TBCari.Location = new System.Drawing.Point(12, 12);
            this.TBCari.Name = "TBCari";
            this.TBCari.Size = new System.Drawing.Size(1040, 22);
            this.TBCari.TabIndex = 18;
            this.TBCari.WatermarkColor = System.Drawing.SystemColors.Desktop;
            this.TBCari.WatermarkText = "Cari.....";
            this.TBCari.TextChanged += new System.EventHandler(this.TBCari_TextChanged);
            // 
            // BBatal
            // 
            this.BBatal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BBatal.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BBatal.Location = new System.Drawing.Point(841, 517);
            this.BBatal.Name = "BBatal";
            this.BBatal.Size = new System.Drawing.Size(101, 29);
            this.BBatal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BBatal.TabIndex = 21;
            this.BBatal.Text = "Batal";
            // 
            // BPilih
            // 
            this.BPilih.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPilih.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BPilih.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BPilih.Location = new System.Drawing.Point(951, 517);
            this.BPilih.Name = "BPilih";
            this.BPilih.Size = new System.Drawing.Size(101, 29);
            this.BPilih.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPilih.TabIndex = 22;
            this.BPilih.Text = "Pilih";
            this.BPilih.Click += new System.EventHandler(this.BPilih_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // LContentDA
            // 
            this.LContentDA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LContentDA.AutoSize = true;
            this.LContentDA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LContentDA.ForeColor = System.Drawing.Color.Black;
            this.LContentDA.Location = new System.Drawing.Point(82, 533);
            this.LContentDA.Name = "LContentDA";
            this.LContentDA.Size = new System.Drawing.Size(0, 13);
            this.LContentDA.TabIndex = 24;
            // 
            // LCNimDA
            // 
            this.LCNimDA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LCNimDA.AutoSize = true;
            this.LCNimDA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LCNimDA.ForeColor = System.Drawing.Color.Black;
            this.LCNimDA.Location = new System.Drawing.Point(12, 533);
            this.LCNimDA.Name = "LCNimDA";
            this.LCNimDA.Size = new System.Drawing.Size(0, 13);
            this.LCNimDA.TabIndex = 23;
            // 
            // Form_PilihAnggota
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 557);
            this.Controls.Add(this.LContentDA);
            this.Controls.Add(this.LCNimDA);
            this.Controls.Add(this.BBatal);
            this.Controls.Add(this.BPilih);
            this.Controls.Add(this.TBCari);
            this.Controls.Add(this.DGPengguna);
            this.DoubleBuffered = true;
            this.Name = "Form_PilihAnggota";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pilih Anggota";
            this.Load += new System.EventHandler(this.Form_PilihAnggota_Load);
            this.Shown += new System.EventHandler(this.Form_PilihAnggota_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.DGPengguna)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevComponents.DotNetBar.Controls.DataGridViewX DGPengguna;
        private System.Windows.Forms.DataGridViewTextBoxColumn NimDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NamaanggotaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn KelasDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlamatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn JkelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NohpDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tanggallahir;
        private System.Windows.Forms.DataGridViewTextBoxColumn totallaporan;
        private System.Windows.Forms.DataGridViewTextBoxColumn nohp;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.DataGridViewTextBoxColumn password;
        private System.Windows.Forms.DataGridViewTextBoxColumn blokir;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBCari;
        internal DevComponents.DotNetBar.ButtonX BBatal;
        internal DevComponents.DotNetBar.ButtonX BPilih;
        private System.Windows.Forms.Timer timer1;
        internal System.Windows.Forms.Label LContentDA;
        internal System.Windows.Forms.Label LCNimDA;
    }
}