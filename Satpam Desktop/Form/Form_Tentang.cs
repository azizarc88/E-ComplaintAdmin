﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace Satpam_Desktop.Form
{
    public partial class Form_Tentang : DevComponents.DotNetBar.Metro.MetroForm
    {
        public Form_Tentang()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
        }

        private void Form_Tentang_Load(object sender, EventArgs e)
        {
            Label1.Text = this.ProductVersion.ToString();
        }

        private void ButtonX3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonX2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.facebook.com/dilittlesnoopy");
        }

        private void ButtonX1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://twitter.com/Agateophobea");
        }

        private void ButtonX4_Click(object sender, EventArgs e)
        {
            Form_Dukungan f = new Form_Dukungan();
            f.ShowDialog();
        }
    }
}
