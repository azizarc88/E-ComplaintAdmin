﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Satpam_Desktop.Class;
using System.Collections.Specialized;
using MessageHandle;

namespace Satpam_Desktop.Form
{
    public partial class Form_LaporanDilaporkan : DevComponents.DotNetBar.Metro.MetroForm
    {
        RESTService rs = new RESTService();
        DataGridManager DGM = new DataGridManager();

        private Bagian bagian;

        public enum Bagian
        {
            Fasilitas = 0,
            Pelanggaran = 1
        }

        private void HapusDataUtama(DataGridViewSelectedRowCollection DataGridSelectedRows, string Bagian, string namatabel, bool Sembunyi = false, string unique = "nim")
        {
            MessageBoxEx.AntiAlias = true;
            MessageBoxEx.UseSystemLocalizedString = true;

            if (DataGridSelectedRows.Count == 0)
            {
                ToastNotification.Show(this, "Tidak ada data yang dipilih", null, 3000, eToastGlowColor.Orange);
                return;
            }

            if (Sembunyi == false)
            {
                if (KotakPesan.Show(this, "Apa Anda yakin ingin menghapus data terpilih ?", Application.ProductName, KotakPesan.TombolPesan.Ya_Tidak, KotakPesan.Warna.Pertanyaan) == KotakPesan.HasilPesan.Ya)
                {
                    foreach (DataGridViewRow item in DataGridSelectedRows)
                    {
                        NameValueCollection formData = new NameValueCollection();
                        formData["tabel"] = namatabel;
                        formData["namaid"] = unique;
                        formData["id"] = item.Cells[0].Value.ToString();
                        rs.MakeRequestAsync("hapus_data.php", formData, "Hapus" + Bagian);
                    }
                }
            }
            else
            {
                foreach (DataGridViewRow item in DataGridSelectedRows)
                {
                    NameValueCollection formData = new NameValueCollection();
                    formData["tabel"] = namatabel;
                    formData["namaid"] = unique;
                    formData["id"] = item.Cells[0].Value.ToString();
                    rs.MakeRequestAsync("hapus_data.php", formData, "Hapus" + Bagian);
                }
            }
        }

        private void Segarkan()
        {
            if (bagian == Bagian.Fasilitas)
            {
                DGM.IsiDG(DGLaporanDilaporkan, "LaporanFasDilaporkan");
            }
            else if (bagian == Bagian.Pelanggaran)
            {
                DGM.IsiDG(DGLaporanDilaporkan, "LaporanPlgrDilaporkan");
            }
        }

        public Form_LaporanDilaporkan(Bagian bagian)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
            this.bagian = bagian;
            rs.RequestComplete += Rs_RequestComplete;

            if (bagian == Bagian.Fasilitas)
            {
                this.Text = "Laporan Fasilitas Dilaporkan";
            }
            else if (bagian == Bagian.Pelanggaran)
            {
                this.Text = "Laporan Pelanggaran Dilaporkan";
                keluhan.HeaderText = "Pelanggaran";
            }
        }

        private void Rs_RequestComplete(object sender, RequestCompleteEventArgs e)
        {
            if (rs.GetString(e.Response, "hasil") == "SUKSES")
            {
                Segarkan();
                ToastNotification.Show(this, "Data terpilih telah dihapus", null, 3000, eToastGlowColor.Blue);
            }
            else
            {
                ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - HapusPengguna", null, 3000, eToastGlowColor.Red);
            }
        }

        private void BLPlgrDilaporkan_Click(object sender, EventArgs e)
        {
            if (bagian == Bagian.Fasilitas)
            {
                Form_AturLaporanDilaporkan f = new Form_AturLaporanDilaporkan(Form_AturLaporanDilaporkan.Aksi.TambahFasilitas);
                f.DataGridSegarkan += F_DataGridSegarkan;
                f.ShowDialog();
                if (f.DialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    ToastNotification.Show(DGLaporanDilaporkan, "Data telah ditambahkan", eToastPosition.MiddleCenter);
                }
            }
            else
            {
                Form_AturLaporanDilaporkan f = new Form_AturLaporanDilaporkan(Form_AturLaporanDilaporkan.Aksi.TambahPelanggaran);
                f.DataGridSegarkan += F_DataGridSegarkan;
                f.ShowDialog();
                if (f.DialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    ToastNotification.Show(DGLaporanDilaporkan, "Data telah ditambahkan", eToastPosition.MiddleCenter);
                }
            }
        }

        private void F_DataGridSegarkan(object sender, DataGridSegarkanEventArgs e)
        {
            Segarkan();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (DGLaporanDilaporkan.SelectedRows.Count == 0)
            {
                ToastNotification.Show(this, "Tidak ada data yang dipilih", null, 3000, eToastGlowColor.Orange);
                return;
            }

            if (bagian == Bagian.Fasilitas)
            {
                HapusDataUtama(DGLaporanDilaporkan.SelectedRows, "Laporan Fasilitas", "laporanfasdilaporkan", false, "ldid");
            }
            else
            {
                HapusDataUtama(DGLaporanDilaporkan.SelectedRows, "Laporan Fasilitas", "laporanplgrdilaporkan", false, "ldid");
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            if (DGLaporanDilaporkan.SelectedRows.Count == 0)
            {
                ToastNotification.Show(this, "Tidak ada data yang dipilih", null, 3000, eToastGlowColor.Orange);
                return;
            }

            string[] dataubah = new string[DGLaporanDilaporkan.CurrentRow.Cells.Count];
            int i = 0;
            foreach (DataGridViewCell cell in DGLaporanDilaporkan.CurrentRow.Cells)
            {
                dataubah[i] = cell.Value.ToString();
                i++;
            }

            if (bagian == Bagian.Fasilitas)
            {
                Form_AturLaporanDilaporkan f = new Form_AturLaporanDilaporkan(Form_AturLaporanDilaporkan.Aksi.UbahFasilitas, dataubah);
                f.DataGridSegarkan += new Form_AturLaporanDilaporkan.DataGridSegarkanHandler(F_DataGridSegarkan);
                f.sem = DGLaporanDilaporkan.CurrentCellAddress.Y;
                f.max = DGLaporanDilaporkan.RowCount - 1;
                f.ShowDialog();
                if (f.DialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    ToastNotification.Show(DGLaporanDilaporkan, "Data telah diubah", eToastPosition.MiddleCenter);
                }
            }
            else
            {
                Form_AturLaporanDilaporkan f = new Form_AturLaporanDilaporkan(Form_AturLaporanDilaporkan.Aksi.UbahPelanggaran, dataubah);
                f.DataGridSegarkan += new Form_AturLaporanDilaporkan.DataGridSegarkanHandler(F_DataGridSegarkan);
                f.sem = DGLaporanDilaporkan.CurrentCellAddress.Y;
                f.max = DGLaporanDilaporkan.RowCount - 1;
                f.ShowDialog();
                if (f.DialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    ToastNotification.Show(DGLaporanDilaporkan, "Data telah diubah", eToastPosition.MiddleCenter);
                }
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            try
            {
                if (bagian == Bagian.Fasilitas)
                {
                    Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Fasilitas, DGLaporanDilaporkan.CurrentRow.Cells[1].Value.ToString());
                    f.ShowDialog();
                }
                else
                {
                    Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Pelanggaran, DGLaporanDilaporkan.CurrentRow.Cells[1].Value.ToString());
                    f.ShowDialog();
                }
            }
            catch (Exception)
            {
            }
        }

        private void Form_LaporanDilaporkan_Load(object sender, EventArgs e)
        {
            Segarkan();
        }

        private void DGLaporanDilaporkan_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (bagian == Bagian.Fasilitas)
                {
                    Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Fasilitas, DGLaporanDilaporkan.CurrentRow.Cells[1].Value.ToString());
                    f.ShowDialog();
                }
                else
                {
                    Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Pelanggaran, DGLaporanDilaporkan.CurrentRow.Cells[1].Value.ToString());
                    f.ShowDialog();
                }
            }
            catch (Exception)
            {
            }
        }

        private void DGLaporanDilaporkan_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            TSSLNimPiket.Text = DGLaporanDilaporkan.CurrentRow.Cells[0].Value.ToString();
            LDSPiket.Text = DGLaporanDilaporkan.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
        }

        private void DGLaporanDilaporkan_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            TSSLBanyakDataPiket.Text = DGLaporanDilaporkan.RowCount.ToString();
        }
    }
}
