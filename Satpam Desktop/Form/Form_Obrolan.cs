﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Satpam_Desktop.Class;
using System.Collections.Specialized;

namespace Satpam_Desktop.Form
{
    public partial class Form_Obrolan : DevComponents.DotNetBar.Metro.MetroForm
    {
        RESTService rs = new RESTService();
        DataGridManager DGM = new DataGridManager();
        private string dari;
        private string nama;

        public Form_Obrolan(string dari, string nama)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
            this.dari = dari;
            this.nama = nama;

            LNama.Text = nama;
            this.Text = nama;
        }

        private void Form_Obrolan_Load(object sender, EventArgs e)
        {
            rs.RequestComplete += Rs_RequestComplete;
            DGM.IsiDG(DGObrolan, "ObrolanMasuk", dari);
            totalnow = DGObrolan.RowCount;
            TBPesan.BackColor = Color.White;
            timer1.Enabled = true;
        }

        private void BKirimKomen_Click(object sender, EventArgs e)
        {
            if (TBPesan.Text != "")
            {
                NameValueCollection formData = new NameValueCollection();
                formData["dari"] = Program.Pgid;
                formData["untuk"] = dari;
                formData["isipesan"] = TBPesan.Text;
                formData["waktu"] = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
                rs.MakeRequestAsync("tambah_obrolan.php", formData, "TambahObrolan");
            }
            else
            {
                ToastNotification.Show(this, "Isi pesan terlebih dahulu", null, 3000, eToastGlowColor.Orange);
                TBPesan.Focus();
            }
        }

        private void Rs_RequestComplete(object sender, RequestCompleteEventArgs e)
        {
            string hasil = rs.GetString(e.Response, "hasil");
            if (hasil == "SUKSES")
            {
                DGM.IsiDG(DGObrolan, "ObrolanMasuk", dari, true);
                totalnow = DGObrolan.RowCount;
                TBPesan.Text = "";
            }
            else if (hasil == "GAGAL")
            {
                ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
            }
        }

        int totalnow = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            DGM.IsiDG(DGObrolan, "ObrolanMasuk", dari, true);
            if (DGObrolan.RowCount > totalnow)
            {
                LBaru.Visible = true;
                totalnow = DGObrolan.RowCount;
            }
        }

        private void DGObrolan_Scroll(object sender, ScrollEventArgs e)
        {
            if ((DGObrolan.RowCount - DGObrolan.DisplayedRowCount(true)) == e.NewValue)
            {
                LBaru.Visible = false;
            }
        }

        private void Form_Obrolan_MouseEnter(object sender, EventArgs e)
        {
            if (LBaru.Visible == true && (DGObrolan.RowCount == DGObrolan.DisplayedRowCount(true)) == true)
            {
                LBaru.Visible = false;
            }
        }

        private void DGObrolan_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            DGObrolan.CurrentCell = null;
        }
    }
}
