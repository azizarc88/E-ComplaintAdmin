﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace Satpam_Desktop.Form
{
    public partial class Form_TampilFoto : DevComponents.DotNetBar.Metro.MetroForm
    {
        private string url;

        public Form_TampilFoto(string url)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
            this.url = url;
        }

        private void Form_TampilFoto_Load(object sender, EventArgs e)
        {
            pictureBox1.LoadAsync(url);
        }
    }
}
