﻿namespace Satpam_Desktop.Form
{
    partial class Form_Debug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TBLog = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.SuspendLayout();
            // 
            // TBLog
            // 
            this.TBLog.AcceptsReturn = true;
            this.TBLog.AcceptsTab = true;
            this.TBLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBLog.Border.Class = "TextBoxBorder";
            this.TBLog.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TBLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TBLog.ForeColor = System.Drawing.Color.Black;
            this.TBLog.Location = new System.Drawing.Point(0, 0);
            this.TBLog.MaxLength = 99999999;
            this.TBLog.Multiline = true;
            this.TBLog.Name = "TBLog";
            this.TBLog.ReadOnly = true;
            this.TBLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TBLog.Size = new System.Drawing.Size(556, 495);
            this.TBLog.TabIndex = 0;
            this.TBLog.WordWrap = false;
            // 
            // Form_Debug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 495);
            this.Controls.Add(this.TBLog);
            this.DoubleBuffered = true;
            this.Name = "Form_Debug";
            this.Text = "Form_Debug";
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.TextBoxX TBLog;
    }
}