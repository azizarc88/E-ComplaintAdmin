﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace Satpam_Desktop.Form
{
    public partial class Form_Debug : DevComponents.DotNetBar.Metro.MetroForm
    {
        public Form_Debug()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
            this.Show();
        }

        public void Write(string info)
        {
            Application.DoEvents();
            TBLog.AppendText(DateTime.Now.ToLongTimeString() + " " + DateTime.Now.Millisecond + " # " + info);
            TBLog.AppendText(Environment.NewLine);
            Application.DoEvents();
        }
    }
}
