﻿namespace Satpam_Desktop.Form
{
    partial class Form_AturPengguna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            this.TBPass = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TBRePass = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TBNoTelp = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.TBAlamat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TBNama = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.CBOtoClose = new DevComponents.DotNetBar.CheckBoxItem();
            this.BPrev = new DevComponents.DotNetBar.ButtonX();
            this.BNext = new DevComponents.DotNetBar.ButtonX();
            this.LUlangiPass = new DevComponents.DotNetBar.LabelX();
            this.BSimpan = new DevComponents.DotNetBar.ButtonX();
            this.BTutup = new DevComponents.DotNetBar.ButtonX();
            this.LPassword = new DevComponents.DotNetBar.LabelX();
            this.RequiredFieldValidator1 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Nim harus diisi");
            this.LabelX5 = new DevComponents.DotNetBar.LabelX();
            this.LabelX4 = new DevComponents.DotNetBar.LabelX();
            this.LabelX3 = new DevComponents.DotNetBar.LabelX();
            this.LabelX2 = new DevComponents.DotNetBar.LabelX();
            this.LabelX1 = new DevComponents.DotNetBar.LabelX();
            this.DTTanggal = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.TBDesa = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TBKota = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.LTotalL = new DevComponents.DotNetBar.LabelX();
            this.TBEmail = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.LEmail = new DevComponents.DotNetBar.LabelX();
            this.IITotalLaporan = new DevComponents.Editors.IntegerInput();
            this.BRefreshTLaporan = new DevComponents.DotNetBar.ButtonX();
            this.CBSebagai = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            ((System.ComponentModel.ISupportInitialize)(this.DTTanggal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IITotalLaporan)).BeginInit();
            this.SuspendLayout();
            // 
            // TBPass
            // 
            this.TBPass.AcceptsReturn = true;
            this.TBPass.AutoSelectAll = true;
            this.TBPass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBPass.Border.Class = "TextBoxBorder";
            this.TBPass.Border.CornerDiameter = 2;
            this.TBPass.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBPass.Border.TextShadowColor = System.Drawing.SystemColors.Highlight;
            this.TBPass.Border.TextShadowOffset = new System.Drawing.Point(2, 2);
            this.TBPass.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBPass, true);
            this.TBPass.Location = new System.Drawing.Point(435, 120);
            this.TBPass.MaxLength = 30;
            this.TBPass.Name = "TBPass";
            this.TBPass.PasswordChar = '●';
            this.TBPass.Size = new System.Drawing.Size(206, 22);
            this.TBPass.TabIndex = 10;
            // 
            // TBRePass
            // 
            this.TBRePass.AutoSelectAll = true;
            this.TBRePass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBRePass.Border.Class = "TextBoxBorder";
            this.TBRePass.Border.CornerDiameter = 2;
            this.TBRePass.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBRePass.Border.TextShadowColor = System.Drawing.SystemColors.Highlight;
            this.TBRePass.Border.TextShadowOffset = new System.Drawing.Point(2, 2);
            this.TBRePass.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBRePass, true);
            this.TBRePass.Location = new System.Drawing.Point(435, 148);
            this.TBRePass.MaxLength = 30;
            this.TBRePass.Name = "TBRePass";
            this.TBRePass.PasswordChar = '●';
            this.TBRePass.Size = new System.Drawing.Size(206, 22);
            this.TBRePass.TabIndex = 11;
            this.TBRePass.TextChanged += new System.EventHandler(this.TBRePass_TextChanged);
            // 
            // TBNoTelp
            // 
            this.TBNoTelp.AllowPromptAsInput = false;
            this.TBNoTelp.AntiAlias = true;
            this.TBNoTelp.AsciiOnly = true;
            this.TBNoTelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBNoTelp.BackgroundStyle.Class = "TextBoxBorder";
            this.TBNoTelp.BackgroundStyle.CornerDiameter = 2;
            this.TBNoTelp.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBNoTelp.BeepOnError = true;
            this.TBNoTelp.ButtonClear.Visible = true;
            this.TBNoTelp.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TBNoTelp.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.TBNoTelp.ForeColor = System.Drawing.Color.Black;
            this.TBNoTelp.HidePromptOnLeave = true;
            this.highlighter1.SetHighlightOnFocus(this.TBNoTelp, true);
            this.TBNoTelp.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.TBNoTelp.Location = new System.Drawing.Point(107, 94);
            this.TBNoTelp.Mask = "000000000000";
            this.TBNoTelp.Name = "TBNoTelp";
            this.TBNoTelp.PromptChar = ' ';
            this.TBNoTelp.ResetOnPrompt = false;
            this.TBNoTelp.ResetOnSpace = false;
            this.TBNoTelp.Size = new System.Drawing.Size(206, 22);
            this.TBNoTelp.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TBNoTelp.TabIndex = 4;
            this.TBNoTelp.Text = "";
            this.TBNoTelp.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TBAlamat
            // 
            this.TBAlamat.AutoSelectAll = true;
            this.TBAlamat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBAlamat.Border.Class = "TextBoxBorder";
            this.TBAlamat.Border.CornerDiameter = 2;
            this.TBAlamat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBAlamat.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBAlamat, true);
            this.TBAlamat.Location = new System.Drawing.Point(107, 122);
            this.TBAlamat.MaxLength = 65;
            this.TBAlamat.Multiline = true;
            this.TBAlamat.Name = "TBAlamat";
            this.TBAlamat.Size = new System.Drawing.Size(206, 95);
            this.TBAlamat.TabIndex = 5;
            // 
            // TBNama
            // 
            this.TBNama.AutoSelectAll = true;
            this.TBNama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBNama.Border.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBNama.Border.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBNama.Border.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBNama.Border.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBNama.Border.Class = "TextBoxBorder";
            this.TBNama.Border.CornerDiameter = 2;
            this.TBNama.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBNama.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBNama, true);
            this.TBNama.Location = new System.Drawing.Point(107, 10);
            this.TBNama.MaxLength = 25;
            this.TBNama.Name = "TBNama";
            this.TBNama.Size = new System.Drawing.Size(206, 22);
            this.TBNama.TabIndex = 1;
            // 
            // CBOtoClose
            // 
            this.CBOtoClose.Checked = true;
            this.CBOtoClose.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBOtoClose.GlobalItem = false;
            this.CBOtoClose.Name = "CBOtoClose";
            this.CBOtoClose.Stretch = true;
            this.CBOtoClose.Text = "Tutup otomatis";
            // 
            // BPrev
            // 
            this.BPrev.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BPrev.Enabled = false;
            this.BPrev.Location = new System.Drawing.Point(333, 183);
            this.BPrev.Name = "BPrev";
            this.BPrev.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BPrev.Size = new System.Drawing.Size(22, 34);
            this.BPrev.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPrev.Symbol = "";
            this.BPrev.SymbolSize = 15F;
            this.BPrev.TabIndex = 14;
            this.BPrev.Click += new System.EventHandler(this.BPrev_Click);
            // 
            // BNext
            // 
            this.BNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BNext.Enabled = false;
            this.BNext.Location = new System.Drawing.Point(619, 183);
            this.BNext.Name = "BNext";
            this.BNext.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BNext.Size = new System.Drawing.Size(22, 34);
            this.BNext.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BNext.Symbol = "";
            this.BNext.SymbolSize = 15F;
            this.BNext.TabIndex = 15;
            this.BNext.Click += new System.EventHandler(this.BNext_Click);
            // 
            // LUlangiPass
            // 
            this.LUlangiPass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LUlangiPass.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LUlangiPass.ForeColor = System.Drawing.Color.Black;
            this.LUlangiPass.Location = new System.Drawing.Point(335, 147);
            this.LUlangiPass.Name = "LUlangiPass";
            this.LUlangiPass.Size = new System.Drawing.Size(100, 23);
            this.LUlangiPass.TabIndex = 67;
            this.LUlangiPass.Text = "Ulangi Password";
            // 
            // BSimpan
            // 
            this.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BSimpan.Location = new System.Drawing.Point(491, 183);
            this.BSimpan.Name = "BSimpan";
            this.BSimpan.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BSimpan.Size = new System.Drawing.Size(126, 34);
            this.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BSimpan.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.CBOtoClose});
            this.BSimpan.SubItemsExpandWidth = 18;
            this.BSimpan.TabIndex = 13;
            this.BSimpan.Text = "Tambah";
            this.BSimpan.Click += new System.EventHandler(this.BSimpan_Click);
            // 
            // BTutup
            // 
            this.BTutup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BTutup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTutup.Location = new System.Drawing.Point(357, 183);
            this.BTutup.Name = "BTutup";
            this.BTutup.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BTutup.Size = new System.Drawing.Size(126, 34);
            this.BTutup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BTutup.TabIndex = 12;
            this.BTutup.Text = "Batal";
            // 
            // LPassword
            // 
            this.LPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LPassword.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LPassword.ForeColor = System.Drawing.Color.Black;
            this.LPassword.Location = new System.Drawing.Point(335, 119);
            this.LPassword.Name = "LPassword";
            this.LPassword.Size = new System.Drawing.Size(100, 23);
            this.LPassword.TabIndex = 66;
            this.LPassword.Text = "Password";
            // 
            // RequiredFieldValidator1
            // 
            this.RequiredFieldValidator1.ErrorMessage = "Nim harus diisi";
            this.RequiredFieldValidator1.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Orange;
            // 
            // LabelX5
            // 
            this.LabelX5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX5.ForeColor = System.Drawing.Color.Black;
            this.LabelX5.Location = new System.Drawing.Point(7, 92);
            this.LabelX5.Name = "LabelX5";
            this.LabelX5.Size = new System.Drawing.Size(100, 23);
            this.LabelX5.TabIndex = 64;
            this.LabelX5.Text = "No. Handphone";
            // 
            // LabelX4
            // 
            this.LabelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX4.ForeColor = System.Drawing.Color.Black;
            this.LabelX4.Location = new System.Drawing.Point(334, 8);
            this.LabelX4.Name = "LabelX4";
            this.LabelX4.Size = new System.Drawing.Size(100, 23);
            this.LabelX4.TabIndex = 60;
            this.LabelX4.Text = "Desa";
            // 
            // LabelX3
            // 
            this.LabelX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX3.ForeColor = System.Drawing.Color.Black;
            this.LabelX3.Location = new System.Drawing.Point(7, 121);
            this.LabelX3.Name = "LabelX3";
            this.LabelX3.Size = new System.Drawing.Size(100, 23);
            this.LabelX3.TabIndex = 57;
            this.LabelX3.Text = "Alamat";
            // 
            // LabelX2
            // 
            this.LabelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX2.ForeColor = System.Drawing.Color.Black;
            this.LabelX2.Location = new System.Drawing.Point(7, 37);
            this.LabelX2.Name = "LabelX2";
            this.LabelX2.Size = new System.Drawing.Size(100, 23);
            this.LabelX2.TabIndex = 54;
            this.LabelX2.Text = "Sebagai";
            // 
            // LabelX1
            // 
            this.LabelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX1.ForeColor = System.Drawing.Color.Black;
            this.LabelX1.Location = new System.Drawing.Point(7, 9);
            this.LabelX1.Name = "LabelX1";
            this.LabelX1.Size = new System.Drawing.Size(100, 23);
            this.LabelX1.TabIndex = 51;
            this.LabelX1.Text = "Nama";
            // 
            // DTTanggal
            // 
            this.DTTanggal.AntiAlias = true;
            this.DTTanggal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.DTTanggal.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DTTanggal.BackgroundStyle.CornerDiameter = 2;
            this.DTTanggal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.DTTanggal.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DTTanggal.ButtonDropDown.Visible = true;
            this.DTTanggal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.DTTanggal.ForeColor = System.Drawing.Color.Black;
            this.DTTanggal.IsPopupCalendarOpen = false;
            this.DTTanggal.Location = new System.Drawing.Point(107, 66);
            // 
            // 
            // 
            this.DTTanggal.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTTanggal.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTTanggal.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.DTTanggal.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DTTanggal.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DTTanggal.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DTTanggal.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DTTanggal.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DTTanggal.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DTTanggal.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DTTanggal.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTTanggal.MonthCalendar.DisplayMonth = new System.DateTime(2016, 2, 1, 0, 0, 0, 0);
            this.DTTanggal.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.DTTanggal.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DTTanggal.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTTanggal.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DTTanggal.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DTTanggal.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DTTanggal.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTTanggal.MonthCalendar.TodayButtonVisible = true;
            this.DTTanggal.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DTTanggal.Name = "DTTanggal";
            this.DTTanggal.Size = new System.Drawing.Size(206, 22);
            this.DTTanggal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DTTanggal.TabIndex = 3;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(7, 65);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(100, 23);
            this.labelX9.TabIndex = 70;
            this.labelX9.Text = "Tanggal Lahir";
            // 
            // TBDesa
            // 
            this.TBDesa.AutoSelectAll = true;
            this.TBDesa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBDesa.Border.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBDesa.Border.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBDesa.Border.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBDesa.Border.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBDesa.Border.Class = "TextBoxBorder";
            this.TBDesa.Border.CornerDiameter = 2;
            this.TBDesa.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBDesa.ForeColor = System.Drawing.Color.Black;
            this.TBDesa.Location = new System.Drawing.Point(435, 8);
            this.TBDesa.MaxLength = 20;
            this.TBDesa.Name = "TBDesa";
            this.TBDesa.Size = new System.Drawing.Size(206, 22);
            this.TBDesa.TabIndex = 6;
            // 
            // TBKota
            // 
            this.TBKota.AutoSelectAll = true;
            this.TBKota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBKota.Border.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBKota.Border.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBKota.Border.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBKota.Border.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBKota.Border.Class = "TextBoxBorder";
            this.TBKota.Border.CornerDiameter = 2;
            this.TBKota.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBKota.ForeColor = System.Drawing.Color.Black;
            this.TBKota.Location = new System.Drawing.Point(435, 36);
            this.TBKota.MaxLength = 20;
            this.TBKota.Name = "TBKota";
            this.TBKota.Size = new System.Drawing.Size(206, 22);
            this.TBKota.TabIndex = 7;
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(334, 35);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(100, 23);
            this.labelX10.TabIndex = 60;
            this.labelX10.Text = "Kota";
            // 
            // LTotalL
            // 
            this.LTotalL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LTotalL.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LTotalL.ForeColor = System.Drawing.Color.Black;
            this.LTotalL.Location = new System.Drawing.Point(334, 63);
            this.LTotalL.Name = "LTotalL";
            this.LTotalL.Size = new System.Drawing.Size(100, 23);
            this.LTotalL.TabIndex = 60;
            this.LTotalL.Text = "Total Laporan";
            // 
            // TBEmail
            // 
            this.TBEmail.AutoSelectAll = true;
            this.TBEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBEmail.Border.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBEmail.Border.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBEmail.Border.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBEmail.Border.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBEmail.Border.Class = "TextBoxBorder";
            this.TBEmail.Border.CornerDiameter = 2;
            this.TBEmail.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBEmail.ForeColor = System.Drawing.Color.Black;
            this.TBEmail.Location = new System.Drawing.Point(435, 92);
            this.TBEmail.MaxLength = 30;
            this.TBEmail.Name = "TBEmail";
            this.TBEmail.Size = new System.Drawing.Size(206, 22);
            this.TBEmail.TabIndex = 9;
            // 
            // LEmail
            // 
            this.LEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LEmail.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LEmail.ForeColor = System.Drawing.Color.Black;
            this.LEmail.Location = new System.Drawing.Point(334, 91);
            this.LEmail.Name = "LEmail";
            this.LEmail.Size = new System.Drawing.Size(100, 23);
            this.LEmail.TabIndex = 60;
            this.LEmail.Text = "Email";
            // 
            // IITotalLaporan
            // 
            this.IITotalLaporan.AllowEmptyState = false;
            this.IITotalLaporan.AntiAlias = true;
            this.IITotalLaporan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.IITotalLaporan.BackgroundStyle.Class = "DateTimeInputBackground";
            this.IITotalLaporan.BackgroundStyle.CornerDiameter = 2;
            this.IITotalLaporan.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.IITotalLaporan.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.IITotalLaporan.DisplayFormat = "0 Laporan";
            this.IITotalLaporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.IITotalLaporan.ForeColor = System.Drawing.Color.Black;
            this.IITotalLaporan.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            this.IITotalLaporan.Location = new System.Drawing.Point(435, 64);
            this.IITotalLaporan.MinValue = 0;
            this.IITotalLaporan.Name = "IITotalLaporan";
            this.IITotalLaporan.ShowUpDown = true;
            this.IITotalLaporan.Size = new System.Drawing.Size(182, 22);
            this.IITotalLaporan.TabIndex = 8;
            // 
            // BRefreshTLaporan
            // 
            this.BRefreshTLaporan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BRefreshTLaporan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BRefreshTLaporan.AntiAlias = true;
            this.BRefreshTLaporan.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BRefreshTLaporan.Location = new System.Drawing.Point(623, 64);
            this.BRefreshTLaporan.Name = "BRefreshTLaporan";
            this.BRefreshTLaporan.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BRefreshTLaporan.Size = new System.Drawing.Size(18, 22);
            this.BRefreshTLaporan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BRefreshTLaporan.Symbol = "";
            this.BRefreshTLaporan.SymbolSize = 10F;
            this.BRefreshTLaporan.TabIndex = 73;
            this.BRefreshTLaporan.Click += new System.EventHandler(this.BRefreshTLaporan_Click);
            // 
            // CBSebagai
            // 
            this.CBSebagai.DisplayMember = "Text";
            this.CBSebagai.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CBSebagai.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBSebagai.ForeColor = System.Drawing.Color.Black;
            this.CBSebagai.FormattingEnabled = true;
            this.CBSebagai.ItemHeight = 14;
            this.CBSebagai.Location = new System.Drawing.Point(107, 39);
            this.CBSebagai.Name = "CBSebagai";
            this.CBSebagai.Size = new System.Drawing.Size(206, 20);
            this.CBSebagai.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBSebagai.TabIndex = 2;
            // 
            // Form_AturPengguna
            // 
            this.AcceptButton = this.BSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BTutup;
            this.ClientSize = new System.Drawing.Size(650, 228);
            this.ControlBox = false;
            this.Controls.Add(this.CBSebagai);
            this.Controls.Add(this.BRefreshTLaporan);
            this.Controls.Add(this.IITotalLaporan);
            this.Controls.Add(this.DTTanggal);
            this.Controls.Add(this.labelX9);
            this.Controls.Add(this.BPrev);
            this.Controls.Add(this.BNext);
            this.Controls.Add(this.LUlangiPass);
            this.Controls.Add(this.BSimpan);
            this.Controls.Add(this.BTutup);
            this.Controls.Add(this.LPassword);
            this.Controls.Add(this.TBPass);
            this.Controls.Add(this.TBRePass);
            this.Controls.Add(this.TBNoTelp);
            this.Controls.Add(this.LabelX5);
            this.Controls.Add(this.LEmail);
            this.Controls.Add(this.LTotalL);
            this.Controls.Add(this.labelX10);
            this.Controls.Add(this.LabelX4);
            this.Controls.Add(this.LabelX3);
            this.Controls.Add(this.TBAlamat);
            this.Controls.Add(this.LabelX2);
            this.Controls.Add(this.LabelX1);
            this.Controls.Add(this.TBEmail);
            this.Controls.Add(this.TBKota);
            this.Controls.Add(this.TBDesa);
            this.Controls.Add(this.TBNama);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.Name = "Form_AturPengguna";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_AturPengguna";
            this.Shown += new System.EventHandler(this.Form_AturPengguna_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DTTanggal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IITotalLaporan)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBPass;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBRePass;
        internal DevComponents.DotNetBar.Controls.MaskedTextBoxAdv TBNoTelp;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBAlamat;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBNama;
        internal DevComponents.DotNetBar.CheckBoxItem CBOtoClose;
        internal DevComponents.DotNetBar.ButtonX BPrev;
        internal DevComponents.DotNetBar.ButtonX BNext;
        internal DevComponents.DotNetBar.LabelX LUlangiPass;
        internal DevComponents.DotNetBar.ButtonX BSimpan;
        internal DevComponents.DotNetBar.ButtonX BTutup;
        internal DevComponents.DotNetBar.LabelX LPassword;
        internal DevComponents.DotNetBar.Validator.RequiredFieldValidator RequiredFieldValidator1;
        internal DevComponents.DotNetBar.LabelX LabelX5;
        internal DevComponents.DotNetBar.LabelX LabelX4;
        internal DevComponents.DotNetBar.LabelX LabelX3;
        internal DevComponents.DotNetBar.LabelX LabelX2;
        internal DevComponents.DotNetBar.LabelX LabelX1;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DTTanggal;
        internal DevComponents.DotNetBar.LabelX labelX9;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBDesa;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBKota;
        internal DevComponents.DotNetBar.LabelX labelX10;
        internal DevComponents.DotNetBar.LabelX LTotalL;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBEmail;
        internal DevComponents.DotNetBar.LabelX LEmail;
        private DevComponents.Editors.IntegerInput IITotalLaporan;
        internal DevComponents.DotNetBar.ButtonX BRefreshTLaporan;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CBSebagai;
    }
}