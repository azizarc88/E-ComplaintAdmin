﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Satpam_Desktop.Class;
using System.Collections.Specialized;

namespace Satpam_Desktop.Form
{
    public partial class Form_AturPengguna : DevComponents.DotNetBar.Metro.MetroForm
    {
        public int sem { set; get; }
        public int max { set; get; }
        private DataGridView DataGrid;
        private Aksi aksi;
        private RESTService rs = new RESTService();
        private string[] dataubah;
        private List<string[]> sebagai = new List<string[]>();

        public delegate void DataGridSegarkanHandler(object sender, DataGridSegarkanEventArgs e);
        public event DataGridSegarkanHandler DataGridSegarkan;

        public enum Aksi
        {
            Tambah = 0,
            Ubah = 1
        }

        private void Bersihkan()
        {
            TBNama.Text = "";
            CBSebagai.Text = "";
            DTTanggal.Text = "";
            TBNoTelp.Text = "";
            TBAlamat.Text = "";
            TBDesa.Text = "";
            TBKota.Text = "";
            IITotalLaporan.Text = "";
            TBEmail.Text = "";
            TBPass.Text = "";
            TBRePass.Text = "";
        }

        private bool isAllValid()
        {
            bool valid = true;
            string pesan = "";

            if (TBNama.Text == "")
            {
                pesan = "Nama harus diisi";
                TBNama.Focus();
                valid = false;
            }
            else if (CBSebagai.Text == "")
            {
                pesan = "Sebagai harus diisi";
                CBSebagai.Focus();
                valid = false;
            }
            else if (DTTanggal.Text == "")
            {
                pesan = "Tanggal Lahir harus diisi";
                DTTanggal.Focus();
                valid = false;
            }
            else if (TBNoTelp.Text == "" && TBEmail.Text == "")
            {
                pesan = "Salah satu antara No. Telp dan Email harus diisi";
                TBNoTelp.Focus();
                valid = false;
            }
            else if (TBAlamat.Text == "")
            {
                pesan = "Alamat harus diisi";
                TBAlamat.Focus();
                valid = false;
            }
            else if (TBDesa.Text == "")
            {
                pesan = "Desa harus diisi";
                TBDesa.Focus();
                valid = false;
            }
            else if (TBKota.Text == "")
            {
                pesan = "Kota harus diisi";
                TBKota.Focus();
                valid = false;
            }
            else if (TBPass.Text == "")
            {
                pesan = "Password harus diisi";
                TBPass.Focus();
                valid = false;
            }
            else if (TBPass.Text != TBRePass.Text)
            {
                pesan = "Password tidak cocok";
                TBRePass.Focus();
                valid = false;
            }

            if (!valid)
            {
                ToastNotification.Show(this, pesan, null, 3000, eToastGlowColor.Orange);
            }

            return valid;
        }

        public Form_AturPengguna(Aksi aksi, string[] dataubah = null, DataGridView DataGrid = null)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
            this.aksi = aksi;

            if (aksi == Aksi.Tambah)
            {
                this.Text = "Tambah Pengguna";
                BSimpan.Text = "Tambah";
                BNext.Enabled = false;
                BPrev.Enabled = false;
                LTotalL.Visible = false;
                IITotalLaporan.Visible = false;
                BRefreshTLaporan.Visible = false;

                int off = 6;
                LEmail.Location = new Point(LEmail.Location.X, LEmail.Location.Y - IITotalLaporan.Size.Height - off);
                TBEmail.Location = new Point(TBEmail.Location.X, TBEmail.Location.Y - TBEmail.Size.Height - off);
                LPassword.Location = new Point(LPassword.Location.X, LPassword.Location.Y - LPassword.Size.Height - off);
                TBPass.Location = new Point(TBPass.Location.X, TBPass.Location.Y - TBPass.Size.Height - off);
                LUlangiPass.Location = new Point(LUlangiPass.Location.X, LUlangiPass.Location.Y - LUlangiPass.Size.Height - off);
                TBRePass.Location = new Point(TBRePass.Location.X, TBRePass.Location.Y - TBRePass.Size.Height - off);
            }
            else
            {
                this.dataubah = dataubah;
                this.DataGrid = DataGrid;
                this.Text = "Ubah Pengguna";
                BSimpan.Text = "Ubah";
                BNext.Enabled = true;
                BPrev.Enabled = true;
            }
        }

        private void MasukkanData()
        {
            TBNama.Text = dataubah[1];

            foreach (string[] item in sebagai)
            {
                if (item[0] == dataubah[2])
                {
                    CBSebagai.Text = item[1];
                }
            }

            try
            {
                DTTanggal.Text = dataubah[6];
            }
            catch (Exception e)
            {
                DTTanggal.Text = DateTime.Now.ToShortDateString();
            }
            TBNoTelp.Text = dataubah[8];
            TBAlamat.Text = dataubah[3];
            TBDesa.Text = dataubah[4];
            TBKota.Text = dataubah[5];
            IITotalLaporan.Text = dataubah[7];
            TBEmail.Text = dataubah[9];
            TBPass.Text = dataubah[10];
        }

        private void Form_AturPengguna_Load(object sender, EventArgs e)
        {
            TBNama.Focus();
            DTTanggal.MaxDate = DateTime.Now;
            rs.RequestComplete += Rs_RequestComplete;
            rs.MakeRequestAsync("ambil_sebagaij.php", null, "AmbilSebagai");

            if (aksi == Aksi.Ubah)
            {
                MasukkanData();
            }
        }

        private void Rs_RequestComplete(object sender, RequestCompleteEventArgs e)
        {
            string hasil = rs.GetString(e.Response, "hasil");
            
            if (e.Bagian == "Pengguna")
            {
                if (hasil == "EMAIL TIDAK VALID")
                {
                    ToastNotification.Show(this, "Email sudah digunakan", null, 3000, eToastGlowColor.Red);
                }
                else if (hasil == "NO HP TIDAK VALID")
                {
                    ToastNotification.Show(this, "No. Handphone sudah digunakan", null, 3000, eToastGlowColor.Red);
                }
                else if (hasil == "GAGAL")
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - Tambah", null, 3000, eToastGlowColor.Red);
                }
                else if (hasil == "SUKSES")
                {
                    DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Pengguna");
                    DataGridSegarkan(this, args);

                    if (CBOtoClose.Checked == true)
                    {
                        this.Close();
                        this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                        return;
                    }

                    ToastNotification.Show(this, "Data " + TBNama.Text + " telah ditambahkan", null, 3000, eToastGlowColor.Blue);
                    Bersihkan();
                    TBNama.Focus();
                }
            }
            else if (e.Bagian == "PenggunaTLaporan")
            {
                if (hasil == "SUKSES")
                {
                    IITotalLaporan.Text = rs.GetString(e.Response, "total");
                    ToastNotification.Show(this, rs.GetString(e.Response, "total") + " Laporan", null, 1000, eToastGlowColor.Blue);
                }
                else
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - TotalLaporan", null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "PenggunaUbah")
            {
                if (hasil == "SUKSES")
                {
                    DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Pengguna");
                    DataGridSegarkan(this, args);

                    if (CBOtoClose.Checked == true)
                    {
                        this.Close();
                        this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                        return;
                    }

                    Bersihkan();
                    ToastNotification.Show(this, "Data " + TBNama.Text + " telah diubah", null, 3000, eToastGlowColor.Blue);
                    TBNama.Focus();
                }
                else
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - Ubah", null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "AmbilSebagai")
            {
                if (hasil == "SUKSES")
                {
                    string[] data = rs.GetArray(e.Response, "sebagai");

                    foreach (var item in data)
                    {
                        string[] sebagai = new string[2];
                        sebagai[0] = rs.GetString(item, "sbid");
                        sebagai[1] = rs.GetString(item, "sebagai");
                        CBSebagai.Items.Add(rs.GetString(item, "sebagai"));
                        this.sebagai.Add(sebagai);
                    }

                    if (aksi == Aksi.Ubah)
                    {
                        foreach (string[] item in sebagai)
                        {
                            if (item[0] == dataubah[2])
                            {
                                CBSebagai.Text = item[1];
                            }
                        }
                    }
                }
                else
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - Ubah", null, 3000, eToastGlowColor.Red);
                }
            }
        }

        private void TBRePass_TextChanged(object sender, EventArgs e)
        {
            if (TBRePass.Text != TBPass.Text)
            {
                TBRePass.BackColor = Color.RosyBrown;
            }
            else
            {
                TBRePass.BackColor = Color.FromArgb(239, 239, 242);
            }
        }

        private void BPrev_Click(object sender, EventArgs e)
        {
            sem--;
            if (sem < 0)
            {
                sem = max;
            }
            TBNama.Focus();

            string[] dataubah = new string[DataGrid.Rows[sem].Cells.Count];
            int i = 0;
            foreach (DataGridViewCell cell in DataGrid.Rows[sem].Cells)
            {
                dataubah[i] = cell.Value.ToString();
                i++;
            }
            this.dataubah = dataubah;
            MasukkanData();
        }

        private void BNext_Click(object sender, EventArgs e)
        {
            sem++;
            if (sem > max)
            {
                sem = 0;
            }
            TBNama.Focus();

            string[] dataubah = new string[DataGrid.Rows[sem].Cells.Count];
            int i = 0;
            foreach (DataGridViewCell cell in DataGrid.Rows[sem].Cells)
            {
                dataubah[i] = cell.Value.ToString();
                i++;
            }
            this.dataubah = dataubah;
            MasukkanData();
        }

        private void proses()
        {
            if (aksi == Aksi.Tambah)
            {
                if (isAllValid())
                {
                    NameValueCollection formData = new NameValueCollection();

                    foreach (string[] item in sebagai)
                    {
                        if (item[1] == CBSebagai.SelectedItem.ToString())
                        {
                            formData["sebagai"] = item[0];
                        }
                    }
                    
                    formData["nama"] = TBNama.Text;
                    formData["alamat"] = TBAlamat.Text;
                    formData["desa"] = TBDesa.Text;
                    formData["kota"] = TBKota.Text;
                    formData["tanggallahir"] = DTTanggal.Value.Year + "-" + DTTanggal.Value.Month + "-" + DTTanggal.Value.Day;
                    formData["nohp"] = TBNoTelp.Text;
                    formData["email"] = TBEmail.Text;
                    formData["password"] = TBPass.Text;
                    rs.MakeRequestAsync("tambah_pengguna.php", formData, "Pengguna");
                }
            }
            else if (aksi == Aksi.Ubah)
            {
                if (isAllValid())
                {
                    NameValueCollection formData = new NameValueCollection();
                    formData["pgid"] = dataubah[0];

                    foreach (string[] item in sebagai)
                    {
                        if (item[1] == CBSebagai.SelectedItem.ToString())
                        {
                            formData["sebagai"] = item[0];
                        }
                    }

                    formData["nama"] = TBNama.Text;
                    formData["alamat"] = TBAlamat.Text;
                    formData["desa"] = TBDesa.Text;
                    formData["kota"] = TBKota.Text;
                    formData["tanggallahir"] = DTTanggal.Value.Year + "-" + DTTanggal.Value.Month + "-" + DTTanggal.Value.Day;
                    formData["nohp"] = TBNoTelp.Text;
                    formData["email"] = TBEmail.Text;
                    formData["password"] = TBPass.Text;
                    formData["totallaporan"] = IITotalLaporan.Value.ToString();
                    rs.MakeRequestAsync("ubah_pengguna.php", formData, "PenggunaUbah");
                }
            }
        }

        private void BSimpan_Click(object sender, EventArgs e)
        {
            proses();
        }

        private void BRefreshTLaporan_Click(object sender, EventArgs e)
        {
            NameValueCollection formData = new NameValueCollection();
            formData["dilaporkanoleh"] = dataubah[0];
            rs.MakeRequestAsync("ambil_total_laporan.php", formData, "PenggunaTLaporan");
        }
    }
}
