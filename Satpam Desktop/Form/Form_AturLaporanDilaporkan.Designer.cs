﻿namespace Satpam_Desktop.Form
{
    partial class Form_AturLaporanDilaporkan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BPilihLaporan = new DevComponents.DotNetBar.ButtonX();
            this.LNamaLaporan = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.LNamaPengguna = new DevComponents.DotNetBar.LabelX();
            this.BPilihPengguna = new DevComponents.DotNetBar.ButtonX();
            this.DTDilaporkanPada = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.BPrev = new DevComponents.DotNetBar.ButtonX();
            this.BNext = new DevComponents.DotNetBar.ButtonX();
            this.BSimpan = new DevComponents.DotNetBar.ButtonX();
            this.CBOtoClose = new DevComponents.DotNetBar.CheckBoxItem();
            this.BTutup = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.DTDilaporkanPada)).BeginInit();
            this.SuspendLayout();
            // 
            // BPilihLaporan
            // 
            this.BPilihLaporan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPilihLaporan.AntiAlias = true;
            this.BPilihLaporan.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BPilihLaporan.Location = new System.Drawing.Point(249, 10);
            this.BPilihLaporan.Name = "BPilihLaporan";
            this.BPilihLaporan.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BPilihLaporan.Size = new System.Drawing.Size(64, 22);
            this.BPilihLaporan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPilihLaporan.SymbolSize = 10F;
            this.BPilihLaporan.TabIndex = 85;
            this.BPilihLaporan.Text = "Pilih";
            this.BPilihLaporan.Click += new System.EventHandler(this.BPilihLaporan_Click);
            // 
            // LNamaLaporan
            // 
            this.LNamaLaporan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNamaLaporan.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNamaLaporan.ForeColor = System.Drawing.Color.Black;
            this.LNamaLaporan.Location = new System.Drawing.Point(107, 9);
            this.LNamaLaporan.Name = "LNamaLaporan";
            this.LNamaLaporan.Size = new System.Drawing.Size(136, 23);
            this.LNamaLaporan.TabIndex = 86;
            this.LNamaLaporan.Text = "Belum ditentukan";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(7, 9);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(100, 23);
            this.labelX6.TabIndex = 87;
            this.labelX6.Text = "Melaporkan";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(7, 37);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(100, 23);
            this.labelX1.TabIndex = 87;
            this.labelX1.Text = "Dilaporkan Oleh";
            // 
            // LNamaPengguna
            // 
            this.LNamaPengguna.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNamaPengguna.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNamaPengguna.ForeColor = System.Drawing.Color.Black;
            this.LNamaPengguna.Location = new System.Drawing.Point(107, 37);
            this.LNamaPengguna.Name = "LNamaPengguna";
            this.LNamaPengguna.Size = new System.Drawing.Size(136, 23);
            this.LNamaPengguna.TabIndex = 86;
            this.LNamaPengguna.Text = "Belum ditentukan";
            // 
            // BPilihPengguna
            // 
            this.BPilihPengguna.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPilihPengguna.AntiAlias = true;
            this.BPilihPengguna.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BPilihPengguna.Location = new System.Drawing.Point(249, 38);
            this.BPilihPengguna.Name = "BPilihPengguna";
            this.BPilihPengguna.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BPilihPengguna.Size = new System.Drawing.Size(64, 22);
            this.BPilihPengguna.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPilihPengguna.SymbolSize = 10F;
            this.BPilihPengguna.TabIndex = 85;
            this.BPilihPengguna.Text = "Pilih";
            this.BPilihPengguna.Click += new System.EventHandler(this.BPilihPengguna_Click);
            // 
            // DTDilaporkanPada
            // 
            this.DTDilaporkanPada.AntiAlias = true;
            this.DTDilaporkanPada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.DTDilaporkanPada.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DTDilaporkanPada.BackgroundStyle.CornerDiameter = 2;
            this.DTDilaporkanPada.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.DTDilaporkanPada.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DTDilaporkanPada.ButtonDropDown.Visible = true;
            this.DTDilaporkanPada.CustomFormat = "dddd, dd MMM yyyy h:mm:ss tt";
            this.DTDilaporkanPada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.DTDilaporkanPada.ForeColor = System.Drawing.Color.Black;
            this.DTDilaporkanPada.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DTDilaporkanPada.IsPopupCalendarOpen = false;
            this.DTDilaporkanPada.Location = new System.Drawing.Point(107, 66);
            // 
            // 
            // 
            this.DTDilaporkanPada.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTDilaporkanPada.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTDilaporkanPada.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.DTDilaporkanPada.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTDilaporkanPada.MonthCalendar.DisplayMonth = new System.DateTime(2016, 2, 1, 0, 0, 0, 0);
            this.DTDilaporkanPada.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.DTDilaporkanPada.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DTDilaporkanPada.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTDilaporkanPada.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DTDilaporkanPada.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DTDilaporkanPada.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DTDilaporkanPada.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTDilaporkanPada.MonthCalendar.TodayButtonVisible = true;
            this.DTDilaporkanPada.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DTDilaporkanPada.Name = "DTDilaporkanPada";
            this.DTDilaporkanPada.Size = new System.Drawing.Size(206, 22);
            this.DTDilaporkanPada.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DTDilaporkanPada.TabIndex = 88;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(7, 66);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(100, 23);
            this.labelX5.TabIndex = 89;
            this.labelX5.Text = "Dilaporkan Pada";
            // 
            // BPrev
            // 
            this.BPrev.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPrev.Enabled = false;
            this.BPrev.Location = new System.Drawing.Point(5, 99);
            this.BPrev.Name = "BPrev";
            this.BPrev.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BPrev.Size = new System.Drawing.Size(22, 34);
            this.BPrev.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPrev.Symbol = "";
            this.BPrev.SymbolSize = 15F;
            this.BPrev.TabIndex = 92;
            // 
            // BNext
            // 
            this.BNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BNext.Enabled = false;
            this.BNext.Location = new System.Drawing.Point(291, 99);
            this.BNext.Name = "BNext";
            this.BNext.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BNext.Size = new System.Drawing.Size(22, 34);
            this.BNext.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BNext.Symbol = "";
            this.BNext.SymbolSize = 15F;
            this.BNext.TabIndex = 93;
            // 
            // BSimpan
            // 
            this.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BSimpan.Location = new System.Drawing.Point(163, 99);
            this.BSimpan.Name = "BSimpan";
            this.BSimpan.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BSimpan.Size = new System.Drawing.Size(126, 34);
            this.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BSimpan.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.CBOtoClose});
            this.BSimpan.SubItemsExpandWidth = 18;
            this.BSimpan.TabIndex = 91;
            this.BSimpan.Text = "Tambah";
            this.BSimpan.Click += new System.EventHandler(this.BSimpan_Click);
            // 
            // CBOtoClose
            // 
            this.CBOtoClose.Checked = true;
            this.CBOtoClose.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBOtoClose.GlobalItem = false;
            this.CBOtoClose.Name = "CBOtoClose";
            this.CBOtoClose.Stretch = true;
            this.CBOtoClose.Text = "Tutup otomatis";
            // 
            // BTutup
            // 
            this.BTutup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTutup.Location = new System.Drawing.Point(29, 99);
            this.BTutup.Name = "BTutup";
            this.BTutup.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BTutup.Size = new System.Drawing.Size(126, 34);
            this.BTutup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BTutup.TabIndex = 90;
            this.BTutup.Text = "Batal";
            // 
            // Form_AturLaporanDilaporkan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 138);
            this.ControlBox = false;
            this.Controls.Add(this.BPrev);
            this.Controls.Add(this.BNext);
            this.Controls.Add(this.BSimpan);
            this.Controls.Add(this.BTutup);
            this.Controls.Add(this.DTDilaporkanPada);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.BPilihPengguna);
            this.Controls.Add(this.LNamaPengguna);
            this.Controls.Add(this.BPilihLaporan);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.LNamaLaporan);
            this.Controls.Add(this.labelX6);
            this.DoubleBuffered = true;
            this.Name = "Form_AturLaporanDilaporkan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_AturLaporanDilaporkan";
            this.Load += new System.EventHandler(this.Form_AturLaporanDilaporkan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DTDilaporkanPada)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.ButtonX BPilihLaporan;
        internal DevComponents.DotNetBar.LabelX LNamaLaporan;
        internal DevComponents.DotNetBar.LabelX labelX6;
        internal DevComponents.DotNetBar.LabelX labelX1;
        internal DevComponents.DotNetBar.LabelX LNamaPengguna;
        internal DevComponents.DotNetBar.ButtonX BPilihPengguna;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DTDilaporkanPada;
        internal DevComponents.DotNetBar.LabelX labelX5;
        internal DevComponents.DotNetBar.ButtonX BPrev;
        internal DevComponents.DotNetBar.ButtonX BNext;
        internal DevComponents.DotNetBar.ButtonX BSimpan;
        internal DevComponents.DotNetBar.CheckBoxItem CBOtoClose;
        internal DevComponents.DotNetBar.ButtonX BTutup;
    }
}