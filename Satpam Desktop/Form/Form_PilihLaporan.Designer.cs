﻿namespace Satpam_Desktop.Form
{
    partial class Form_PilihLaporan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TBCari = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.BBatal = new DevComponents.DotNetBar.ButtonX();
            this.BPilih = new DevComponents.DotNetBar.ButtonX();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.LContentDA = new System.Windows.Forms.Label();
            this.LCNimDA = new System.Windows.Forms.Label();
            this.DGLaporan = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.lfid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.foto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.keluhan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ling = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alamat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prioritas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dilaporkanpada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dilaporkanoleh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laporanrahasia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dukungan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.omentar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DGLaporan)).BeginInit();
            this.SuspendLayout();
            // 
            // TBCari
            // 
            this.TBCari.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBCari.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBCari.Border.Class = "TextBoxBorder";
            this.TBCari.Border.CornerDiameter = 3;
            this.TBCari.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBCari.ForeColor = System.Drawing.Color.Black;
            this.TBCari.Location = new System.Drawing.Point(12, 12);
            this.TBCari.Name = "TBCari";
            this.TBCari.Size = new System.Drawing.Size(1040, 22);
            this.TBCari.TabIndex = 18;
            this.TBCari.WatermarkColor = System.Drawing.SystemColors.Desktop;
            this.TBCari.WatermarkText = "Cari.....";
            this.TBCari.TextChanged += new System.EventHandler(this.TBCari_TextChanged);
            // 
            // BBatal
            // 
            this.BBatal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BBatal.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BBatal.Location = new System.Drawing.Point(841, 517);
            this.BBatal.Name = "BBatal";
            this.BBatal.Size = new System.Drawing.Size(101, 29);
            this.BBatal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BBatal.TabIndex = 21;
            this.BBatal.Text = "Batal";
            // 
            // BPilih
            // 
            this.BPilih.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPilih.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BPilih.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BPilih.Location = new System.Drawing.Point(951, 517);
            this.BPilih.Name = "BPilih";
            this.BPilih.Size = new System.Drawing.Size(101, 29);
            this.BPilih.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPilih.TabIndex = 22;
            this.BPilih.Text = "Pilih";
            this.BPilih.Click += new System.EventHandler(this.BPilih_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // LContentDA
            // 
            this.LContentDA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LContentDA.AutoSize = true;
            this.LContentDA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LContentDA.ForeColor = System.Drawing.Color.Black;
            this.LContentDA.Location = new System.Drawing.Point(82, 533);
            this.LContentDA.Name = "LContentDA";
            this.LContentDA.Size = new System.Drawing.Size(0, 13);
            this.LContentDA.TabIndex = 24;
            // 
            // LCNimDA
            // 
            this.LCNimDA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LCNimDA.AutoSize = true;
            this.LCNimDA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LCNimDA.ForeColor = System.Drawing.Color.Black;
            this.LCNimDA.Location = new System.Drawing.Point(12, 533);
            this.LCNimDA.Name = "LCNimDA";
            this.LCNimDA.Size = new System.Drawing.Size(0, 13);
            this.LCNimDA.TabIndex = 23;
            // 
            // DGLaporan
            // 
            this.DGLaporan.AllowUserToAddRows = false;
            this.DGLaporan.AllowUserToDeleteRows = false;
            this.DGLaporan.AllowUserToResizeRows = false;
            this.DGLaporan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGLaporan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGLaporan.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGLaporan.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGLaporan.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGLaporan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGLaporan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.lfid,
            this.foto,
            this.keluhan,
            this.lat,
            this.ling,
            this.alamat,
            this.prioritas,
            this.dilaporkanpada,
            this.dilaporkanoleh,
            this.laporanrahasia,
            this.dukungan,
            this.omentar});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGLaporan.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGLaporan.EnableHeadersVisualStyles = false;
            this.DGLaporan.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGLaporan.Location = new System.Drawing.Point(12, 40);
            this.DGLaporan.Name = "DGLaporan";
            this.DGLaporan.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGLaporan.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGLaporan.RowHeadersVisible = false;
            this.DGLaporan.RowHeadersWidth = 100;
            this.DGLaporan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGLaporan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGLaporan.Size = new System.Drawing.Size(1040, 464);
            this.DGLaporan.TabIndex = 25;
            this.DGLaporan.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGPengguna_CellClick);
            this.DGLaporan.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGPengguna_CellContentClick);
            // 
            // lfid
            // 
            this.lfid.HeaderText = "LFID";
            this.lfid.Name = "lfid";
            this.lfid.ReadOnly = true;
            this.lfid.Visible = false;
            // 
            // foto
            // 
            this.foto.FillWeight = 70F;
            this.foto.HeaderText = "URL Foto";
            this.foto.Name = "foto";
            this.foto.ReadOnly = true;
            this.foto.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // keluhan
            // 
            this.keluhan.HeaderText = "Keluhan";
            this.keluhan.Name = "keluhan";
            this.keluhan.ReadOnly = true;
            // 
            // lat
            // 
            this.lat.FillWeight = 50F;
            this.lat.HeaderText = "Latitude";
            this.lat.Name = "lat";
            this.lat.ReadOnly = true;
            // 
            // ling
            // 
            this.ling.FillWeight = 50F;
            this.ling.HeaderText = "Longitude";
            this.ling.Name = "ling";
            this.ling.ReadOnly = true;
            // 
            // alamat
            // 
            this.alamat.FillWeight = 80F;
            this.alamat.HeaderText = "Alamat";
            this.alamat.Name = "alamat";
            this.alamat.ReadOnly = true;
            // 
            // prioritas
            // 
            this.prioritas.FillWeight = 40F;
            this.prioritas.HeaderText = "Prioritas";
            this.prioritas.Name = "prioritas";
            this.prioritas.ReadOnly = true;
            // 
            // dilaporkanpada
            // 
            this.dilaporkanpada.FillWeight = 70F;
            this.dilaporkanpada.HeaderText = "Waktu";
            this.dilaporkanpada.Name = "dilaporkanpada";
            this.dilaporkanpada.ReadOnly = true;
            this.dilaporkanpada.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dilaporkanoleh
            // 
            this.dilaporkanoleh.FillWeight = 25F;
            this.dilaporkanoleh.HeaderText = "Oleh";
            this.dilaporkanoleh.Name = "dilaporkanoleh";
            this.dilaporkanoleh.ReadOnly = true;
            // 
            // laporanrahasia
            // 
            this.laporanrahasia.FillWeight = 30F;
            this.laporanrahasia.HeaderText = "Rahasia";
            this.laporanrahasia.Name = "laporanrahasia";
            this.laporanrahasia.ReadOnly = true;
            // 
            // dukungan
            // 
            this.dukungan.FillWeight = 35F;
            this.dukungan.HeaderText = "Dukungan";
            this.dukungan.Name = "dukungan";
            this.dukungan.ReadOnly = true;
            // 
            // omentar
            // 
            this.omentar.FillWeight = 35F;
            this.omentar.HeaderText = "Komentar";
            this.omentar.Name = "omentar";
            this.omentar.ReadOnly = true;
            // 
            // Form_PilihLaporan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 557);
            this.Controls.Add(this.DGLaporan);
            this.Controls.Add(this.LContentDA);
            this.Controls.Add(this.LCNimDA);
            this.Controls.Add(this.BBatal);
            this.Controls.Add(this.BPilih);
            this.Controls.Add(this.TBCari);
            this.DoubleBuffered = true;
            this.Name = "Form_PilihLaporan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pilih Anggota";
            this.Load += new System.EventHandler(this.Form_PilihAnggota_Load);
            this.Shown += new System.EventHandler(this.Form_PilihAnggota_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.DGLaporan)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal DevComponents.DotNetBar.Controls.TextBoxX TBCari;
        internal DevComponents.DotNetBar.ButtonX BBatal;
        internal DevComponents.DotNetBar.ButtonX BPilih;
        private System.Windows.Forms.Timer timer1;
        internal System.Windows.Forms.Label LContentDA;
        internal System.Windows.Forms.Label LCNimDA;
        public DevComponents.DotNetBar.Controls.DataGridViewX DGLaporan;
        private System.Windows.Forms.DataGridViewTextBoxColumn lfid;
        private System.Windows.Forms.DataGridViewTextBoxColumn foto;
        private System.Windows.Forms.DataGridViewTextBoxColumn keluhan;
        private System.Windows.Forms.DataGridViewTextBoxColumn lat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ling;
        private System.Windows.Forms.DataGridViewTextBoxColumn alamat;
        private System.Windows.Forms.DataGridViewTextBoxColumn prioritas;
        private System.Windows.Forms.DataGridViewTextBoxColumn dilaporkanpada;
        private System.Windows.Forms.DataGridViewTextBoxColumn dilaporkanoleh;
        private System.Windows.Forms.DataGridViewTextBoxColumn laporanrahasia;
        private System.Windows.Forms.DataGridViewTextBoxColumn dukungan;
        private System.Windows.Forms.DataGridViewTextBoxColumn omentar;
    }
}