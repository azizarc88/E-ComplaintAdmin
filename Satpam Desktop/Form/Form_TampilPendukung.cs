﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Satpam_Desktop.Class;

namespace Satpam_Desktop.Form
{
    public partial class Form_TampilPendukung : DevComponents.DotNetBar.Metro.MetroForm
    {
        RESTService rs = new RESTService();
        private string url;

        public Form_TampilPendukung(string url)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
            this.url = url;
            rs.RequestComplete += Rs_RequestComplete;
        }

        private void Form_TampilPendukung_Load(object sender, EventArgs e)
        {
            LBPendukung.BackColor = Color.White;
            rs.MakeRequestAsync(url, null, "pendukung");
        }

        private void Rs_RequestComplete(object sender, RequestCompleteEventArgs e)
        {
            string hasil = rs.GetString(e.Response, "hasil");

            if (hasil == "SUKSES")
            {
                string[] data = rs.GetArray(e.Response, "pendukung");

                foreach (var item in data)
                {
                    LBPendukung.Items.Add(rs.GetString(item, "nama"));
                }
            }
            else if (hasil == "TIDAK ADA")
            {
                ToastNotification.Show(this, "Tidak ada pendukung", null, 3000, eToastGlowColor.Orange);
            }
            else
            {
                ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
            }
        }
    }
}
