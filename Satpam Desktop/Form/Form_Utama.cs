﻿using DevComponents.DotNetBar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Satpam_Desktop.Class;
using System.Collections.Specialized;
using MessageHandle;
using Transitions;
using System.IO;
using System.Net;

namespace Satpam_Desktop.Form
{
    public partial class Form_Utama : DevComponents.DotNetBar.Metro.MetroForm
    {
        RESTService rs = new RESTService();
        DataGridManager DGM = new DataGridManager();

        private List<Image> daftarimagefas = new List<Image>();
        private List<string> daftargambarfas = new List<string>();
        private List<string> ldaftargambarfas = new List<string>();
        private List<string> daftaridfas = new List<string>();
        private List<string> daftarfilefas = new List<string>();

        private List<Image> daftarimageplgr = new List<Image>();
        private List<string> daftargambarplgr = new List<string>();
        private List<string> ldaftargambarplgr = new List<string>();
        private List<string> daftaridplgr = new List<string>();
        private List<string> daftarfileplgr = new List<string>();

        public string curbagian;

        public Form_Utama()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
        }

        private void Slide()
        {
            Control ctrlOnScreen, ctrlOffScreen;
            if (PBFas1.Left == 3)
            {
                ctrlOnScreen = PBFas1;
                ctrlOffScreen = PBFas2;
                PBFas2.Image = AmbilDataRandomFas();
                System.Threading.Thread.Sleep(100);
            }
            else
            {
                ctrlOnScreen = PBFas2;
                ctrlOffScreen = PBFas1;
                PBFas1.Image = AmbilDataRandomFas();
                System.Threading.Thread.Sleep(100);
            }
            ctrlOnScreen.SendToBack();
            ctrlOffScreen.BringToFront();

            Transition t = new Transition(new TransitionType_EaseInEaseOut(1000));
            t.add(ctrlOnScreen, "Left", ctrlOnScreen.Width * 2);
            t.add(ctrlOffScreen, "Left", 3);
            t.run();
        }

        private void Slide2()
        {
            Control ctrlOnScreen, ctrlOffScreen;
            if (PBPlgr1.Left == 3)
            {
                ctrlOnScreen = PBPlgr1;
                ctrlOffScreen = PBPlgr2;
                PBPlgr2.Image = AmbilDataRandomPlgr();
                System.Threading.Thread.Sleep(100);
            }
            else
            {
                ctrlOnScreen = PBPlgr2;
                ctrlOffScreen = PBPlgr1;
                PBPlgr1.Image = AmbilDataRandomPlgr();
                System.Threading.Thread.Sleep(100);
            }
            ctrlOnScreen.SendToBack();
            ctrlOffScreen.BringToFront();

            Transition t = new Transition(new TransitionType_EaseInEaseOut(1000));
            t.add(ctrlOnScreen, "Left", -1 * ctrlOnScreen.Width);
            t.add(ctrlOffScreen, "Left", 3);
            t.run();
        }

        private string curidfas;
        private string curidplgr;

        private Image AmbilDataRandomFas()
        {
            Random a = new Random();
            int temp = a.Next(0, daftarimagefas.Count);
            curidfas = daftaridfas[temp].ToString();
            return daftarimagefas[temp];
        }

        private Image AmbilDataRandomPlgr()
        {
            Random a = new Random();
            int temp = a.Next(0, daftarimageplgr.Count);
            curidplgr = daftaridplgr[temp].ToString();
            return daftarimageplgr[temp];
        }

        public Image AmbilGambar(string alamat)
        {
            try
            {
                using (var bmpTemp = new Bitmap(alamat))
                {
                    return new Bitmap(bmpTemp);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        private List<Image> TentukanGambarFas()
        {
            List<Image> hasil = new List<Image>();
            foreach (string item in daftargambarfas)
            {
                hasil.Add(AmbilGambar(item));
            }
            return hasil;
        }

        private List<Image> TentukanGambarPlgr()
        {
            List<Image> hasil = new List<Image>();
            foreach (string item in daftargambarplgr)
            {
                hasil.Add(AmbilGambar(item));
            }
            return hasil;
        }

        private List<string> AmbilAlamatFotoFas()
        {
            List<string> hasil = new List<string>();

            DirectoryInfo alamatfoto = new DirectoryInfo(Application.StartupPath + "\\Foto\\LaporanFas");
            
            foreach (string item in daftarfilefas)
            {
                hasil.Add(alamatfoto.ToString() +  "\\" + item.ToString());
            }
            
            return hasil;
        }

        private List<string> AmbilAlamatFotoPlgr()
        {
            List<string> hasil = new List<string>();

            DirectoryInfo alamatfoto = new DirectoryInfo(Application.StartupPath + "\\Foto\\LaporanPlgr");

            foreach (string item in daftarfileplgr)
            {
                hasil.Add(alamatfoto.ToString() + "\\" + item.ToString());
            }

            return hasil;
        }

        private void AturLevel()
        {
            if (Program.cpanel == false)
            {
                KotakPesan.Show(this, "Anda tidak memiliki hak untuk menggunakan aplikasi ini.", "Satpam Desktop", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan);
                this.Hide();
                Form_Login frm = new Form_Login();
                frm.Show();
            }

            if (Program.cpengguna == false)
            {
                password.Visible = false;
                RMITambahPengguna.Enabled = false;
                RMIUbahPengguna.Enabled = false;
                RMIHapusPengguna.Enabled = false;
                RMIBlokirPengguna.Enabled = false;
                RMIUBlokirPengguna.Enabled = false;
            }

            if (Program.claporanfas == false)
            {
                RMITambahLFas.Enabled = false;
                RMIUbahLFas.Enabled = false;
                RMIHapusLFas.Enabled = false;
            }

            if (Program.claporanplgr == false)
            {
                RMITambahPlgr.Enabled = false;
                RMIUbahPlgr.Enabled = false;
                RMIHapusPlgr.Enabled = false;
            }

            if (Program.csebagai == false)
            {
                RMITambahSebagai.Enabled = false;
                RMIUbahSebagai.Enabled = false;
                RMIHapusSebagai.Enabled = false;
            }
        }

        public void Segarkan(string DataGrid)
        {
            curbagian = DataGrid;
            if (DataGrid == "Pengguna")
            {
                DGM.IsiDG(DGPengguna, DataGrid);
            }
            else if (DataGrid == "Laporan Fasilitas")
            {
                DGM.IsiDG(DGLaporanFasilitas, DataGrid);
            }
            else if (DataGrid == "Laporan Pelanggaran")
            {
                DGM.IsiDG(DGLaporanPelanggaran, DataGrid);
            }
            else if (DataGrid == "Laporan Komentar")
            {
                DGM.IsiDG(DGLKomentarFas, DataGrid + "Fas");
                DataGridManager dgm = new DataGridManager();
                dgm.IsiDG(DGLKomentarPlgr, DataGrid + "Plgr");
            }
            else if (DataGrid == "Obrolan")
            {
                DGM.IsiDG(DGObrolan, DataGrid, Program.Pgid);
            }
            else if (DataGrid == "Pengaturan Sebagai")
            {
                DGM.IsiDG(DGSebagai, DataGrid);
            }
        }

        bool sudahhapus = false;

        private void HapusDataUtama(DataGridViewSelectedRowCollection DataGridSelectedRows, string Bagian, string namatabel, bool Sembunyi = false, string unique = "nim")
        {
            if (DataGridSelectedRows.Count == 0)
            {
                ToastNotification.Show(this, "Tidak ada data yang dipilih", null, 3000, eToastGlowColor.Orange);
                return;
            }

            if (Sembunyi == false)
            {
                if (MessageHandle.KotakPesan.Show(this, "Apa Anda yakin ingin menghapus data terpilih ?", Bagian, KotakPesan.TombolPesan.Ya_Tidak, KotakPesan.Warna.Pertanyaan) == KotakPesan.HasilPesan.Ya)
                {
                    foreach (DataGridViewRow item in DataGridSelectedRows)
                    {
                        sudahhapus = false;
                        NameValueCollection formData = new NameValueCollection();
                        formData["tabel"] = namatabel;
                        formData["namaid"] = unique;
                        formData["id"] = item.Cells[0].Value.ToString();
                        rs.MakeRequestAsync("hapus_data.php", formData, "hapus" + Bagian);
                    }
                    sudahhapus = true;
                }
            }
            else
            {
                foreach (DataGridViewRow item in DataGridSelectedRows)
                {
                    sudahhapus = false;
                    NameValueCollection formData = new NameValueCollection();
                    formData["tabel"] = namatabel;
                    formData["namaid"] = unique;
                    formData["id"] = item.Cells[0].Value.ToString();
                    rs.MakeRequestAsync("hapus_data.php", formData, "hapus" + Bagian);
                }
                sudahhapus = true;
            }
        }

        private void BlokirPengguna(DataGridViewSelectedRowCollection DataGridSelectedRows, bool blokir)
        {
            if (MessageHandle.KotakPesan.Show(this, "Apa Anda yakin ingin memblokir pengguna terpilih ?", Application.ProductName, KotakPesan.TombolPesan.Ya_Tidak, KotakPesan.Warna.Pertanyaan) == KotakPesan.HasilPesan.Ya)
            {
                foreach (DataGridViewRow item in DataGridSelectedRows)
                {
                    NameValueCollection formData = new NameValueCollection();
                    formData["pgid"] = item.Cells[0].Value.ToString();
                    if (blokir)
                    {
                        formData["blokir"] = "1";
                    }
                    else
                    {
                        formData["blokir"] = "0";
                    }
                    rs.MakeRequestAsync("blokir_pengguna.php", formData, "BlokirPengguna" + blokir);
                }
            }
        }

        private void Form_Utama_Load(object sender, EventArgs e)
        {
            LNamaP.Text = Program.Nama;
            LId.Text = Program.Id;
            LSebagai.Text = Program.Sebagai;

            LWaktuSekarang.Text = DateTime.Now.ToLongDateString();
            
            rs.RequestComplete += new RESTService.RequestCompleteHandler(RequestComplete_Event);
        }


        private void RequestComplete_Event(object sender, RequestCompleteEventArgs e)
        {
            if (e.Bagian == "informasi")
            {
                LTotalLFas.Text = rs.GetString(e.Response, "lfas") + " Laporan";
                LTotalLPlgr.Text = rs.GetString(e.Response, "lplgr") + " Laporan";
                LTotalLFasDil.Text = rs.GetString(e.Response, "llfas") + " Laporan";
                LTotalLPlgrDil.Text = rs.GetString(e.Response, "llplgr") + " Laporan";
                LTotalAngg.Text = rs.GetString(e.Response, "angg") + " Pengguna";
            }
            else if (e.Bagian == "LaporanFotoFas")
            {
                string hasil = rs.GetString(e.Response, "hasil");
                if (hasil == "SUKSES")
                {
                    string[] data = rs.GetArray(e.Response, "laporanfasilitas");

                    int a = 1;
                    foreach (var item in data)
                    {
                        string[] sudahdonlodfas = new string[3];
                        if (a != data.Length)
                        {
                            sudahdonlodfas[0] = "belum";
                        }
                        else
                        {
                            sudahdonlodfas[0] = "sudah";
                        }
                        sudahdonlodfas[1] = rs.GetString(item, "foto").Substring(rs.GetString(item, "foto").LastIndexOf('/') + 1);
                        sudahdonlodfas[2] = rs.GetString(item, "lfid");
                        WebClient downloader = new WebClient();
                        downloader.DownloadFileCompleted += Downloader_DownloadFileCompleted1;
                        if (!Directory.Exists(Application.StartupPath + "\\Foto\\" + "LaporanFas"))
                        {
                            Directory.CreateDirectory(Application.StartupPath + "\\Foto\\" + "LaporanFas");
                        }
                        Uri uri = new Uri(rs.GetString(item, "foto"));
                        downloader.DownloadFileAsync(uri, Application.StartupPath + "\\Foto\\" + "LaporanFas\\" + rs.GetString(item, "foto").Substring(rs.GetString(item, "foto").LastIndexOf('/') + 1), sudahdonlodfas);
                        a++;
                    }

                    foreach (var item in ldaftargambarfas)
                    {
                        
                    }
                }
                else
                {
                    //ToastNotification.Show(this, "Galat saat mengambil daftar gambar laporan fasilitas", null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "LaporanFotoPlgr")
            {
                string hasil = rs.GetString(e.Response, "hasil");
                if (hasil == "SUKSES")
                {
                    string[] data = rs.GetArray(e.Response, "laporanpelanggaran");

                    int a = 1;
                    foreach (var item in data)
                    {
                        string[] sudahdonlodfas = new string[3];
                        if (a != data.Length)
                        {
                            sudahdonlodfas[0] = "belum";
                        }
                        else
                        {
                            sudahdonlodfas[0] = "sudah";
                        }
                        sudahdonlodfas[1] = rs.GetString(item, "foto").Substring(rs.GetString(item, "foto").LastIndexOf('/') + 1);
                        sudahdonlodfas[2] = rs.GetString(item, "lpid");
                        WebClient downloader = new WebClient();
                        downloader.DownloadFileCompleted += Downloader_DownloadFileCompleted;
                        if (!Directory.Exists(Application.StartupPath + "\\Foto\\" + "LaporanPlgr"))
                        {
                            Directory.CreateDirectory(Application.StartupPath + "\\Foto\\" + "LaporanPlgr");
                        }
                        Uri uri = new Uri(rs.GetString(item, "foto"));
                        downloader.DownloadFileAsync(uri, Application.StartupPath + "\\Foto\\" + "LaporanPlgr\\" + rs.GetString(item, "foto").Substring(rs.GetString(item, "foto").LastIndexOf('/') + 1), sudahdonlodfas);
                        a++;
                    }
                }
                else
                {
                    //ToastNotification.Show(this, "Galat saat mengambil daftar gambar laporan pelanggaran", null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian.Contains("hapus"))
            {
                string bagian = e.Bagian.Replace("hapus", "");
                if (rs.GetString(e.Response, "hasil") == "SUKSES")
                {
                    Segarkan(bagian);
                    ToastNotification.Show(this, "Data terpilih telah dihapus", null, 3000, eToastGlowColor.Blue);
                }
                else
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - HapusPengguna", null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian.Contains("BlokirPengguna"))
            {
                string blokir = e.Bagian.Replace("BlokirPengguna", "");
                if (blokir == "True")
                {
                    if (rs.GetString(e.Response, "hasil") == "SUKSES")
                    {
                        Segarkan("Pengguna");
                        ToastNotification.Show(this, "Pengguna terpilih telah diblokir", null, 3000, eToastGlowColor.Blue);
                    }
                    else
                    {
                        ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - BlokirPengguna", null, 3000, eToastGlowColor.Red);
                    }
                }
                else
                {
                    if (rs.GetString(e.Response, "hasil") == "SUKSES")
                    {
                        Segarkan("Pengguna");
                        ToastNotification.Show(this, "Pengguna terpilih sudah tidak diblokir", null, 3000, eToastGlowColor.Blue);
                    }
                    else
                    {
                        ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - BlokirPengguna", null, 3000, eToastGlowColor.Red);
                    }
                }
            }
        }

        private void Downloader_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            string[] hasil = (string[]) e.UserState;
            daftarfileplgr.Add(hasil[1]);
            daftaridplgr.Add(hasil[2]);
            if (hasil[0] == "sudah")
            {
                circularProgress2.Visible = false;
                daftargambarplgr = AmbilAlamatFotoPlgr();
                TSlideShowPlgr.Enabled = true;
                daftarimageplgr = TentukanGambarPlgr();
            }
        }

        private void Downloader_DownloadFileCompleted1(object sender, AsyncCompletedEventArgs e)
        {
            string[] hasil = (string[])e.UserState;
            daftarfilefas.Add(hasil[1]);
            daftaridfas.Add(hasil[2]);
            if (hasil[0] == "sudah")
            {
                circularProgress1.Visible = false;
                daftargambarfas = AmbilAlamatFotoFas();
                TSlideShowFas.Enabled = true;
                daftarimagefas = TentukanGambarFas();
            }
        }

        private void Form_Utama_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!darilogout)
            {
                Application.Exit();
            }
        }

        bool darilogout = false;

        private void Logout_Klik(object sender, EventArgs e)
        {
            if (MessageHandle.KotakPesan.Show(this, "Apa Anda yakin ingin logout program ?", Application.ProductName, KotakPesan.TombolPesan.Ya_Tidak, KotakPesan.Warna.Pertanyaan) == KotakPesan.HasilPesan.Ya)
            {
                darilogout = true;
                this.Close();
                Form_Login frm = new Form_Login();
                frm.Show();
            }
        }

        private void TBIPengguna_Click(object sender, EventArgs e)
        {
            string tab = sender.ToString();
            Segarkan(tab);
        }

        private void Form_Utama_Deactivate(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.FromArgb(87, 171, 183);
        }

        private void Form_Utama_Activated(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.DarkSlateGray;
        }

        private void Form_Utama_Shown(object sender, EventArgs e)
        {
            rs.MakeRequestAsync("ambil_informasi.php", null, "informasi");
            NameValueCollection formData = new NameValueCollection();

            formData["tabel"] = "laporanfasilitas";
            rs.MakeRequestAsync("ambil_laporanfoto.php", formData, "LaporanFotoFas");

            formData["tabel"] = "laporanpelanggaran";
            rs.MakeRequestAsync("ambil_laporanfoto.php", formData, "LaporanFotoPlgr");

            Segarkan("Pengguna");
            tableLayoutPanel1.BackColor = Color.FromArgb(239, 239, 242);
            tableLayoutPanel2.BackColor = Color.FromArgb(239, 239, 242);
            metroStatusBar1.BackgroundStyle.BackColor = Color.FromArgb(87, 171, 183);

            AturLevel();

            circularProgress1.IsRunning = true;
            circularProgress2.IsRunning = true;

            refresh = new RefreshManager();
            timer1.Enabled = true;
        }

        RefreshManager refresh;

        private void RM_MouseEnter(object sender, EventArgs e)
        {
            RadialMenu m = (RadialMenu) sender;
            m.Symbol = "";
        }

        private void RM_MouseLeave(object sender, EventArgs e)
        {
            RadialMenu m = (RadialMenu) sender;
            m.Symbol = "";
        }

        private void RMITambahPengguna_Klik(object sender, EventArgs e)
        {
            Form_AturPengguna f = new Form_AturPengguna(Form_AturPengguna.Aksi.Tambah);
            f.DataGridSegarkan += F_DataGridSegarkan;
            f.ShowDialog();
            if (f.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGPengguna, "Data telah ditambahkan", eToastPosition.MiddleCenter);
            }
        }

        private void RMIUbahPengguna_Klik(object sender, EventArgs e)
        {
            if (DGPengguna.SelectedRows.Count == 0)
            {
                ToastNotification.Show(this, "Tidak ada data yang dipilih", null, 3000, eToastGlowColor.Orange);
                return;
            }

            string[] dataubah = new string[DGPengguna.CurrentRow.Cells.Count];
            int i = 0;
            foreach (DataGridViewCell cell in DGPengguna.CurrentRow.Cells)
            {
                dataubah[i] = cell.Value.ToString();
                i++;
            }
            Form_AturPengguna f = new Form_AturPengguna(Form_AturPengguna.Aksi.Ubah, dataubah, DGPengguna);
            f.DataGridSegarkan += new Form_AturPengguna.DataGridSegarkanHandler(F_DataGridSegarkan);
            f.sem = DGPengguna.CurrentCellAddress.Y;
            f.max = DGPengguna.RowCount - 1;
            f.ShowDialog();
            if (f.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGPengguna, "Data telah diubah", eToastPosition.MiddleCenter);
            }
        }

        private void F_DataGridSegarkan(object sender, DataGridSegarkanEventArgs e)
        {
            Segarkan(e.Bagian);
        }

        private void RMIHapusPengguna_Klik(object sender, EventArgs e)
        {
            HapusDataUtama(DGPengguna.SelectedRows, "Pengguna", "pengguna", false, "pgid");
        }

        private void RMIBlokirPengguna_Klik(object sender, EventArgs e)
        {
            BlokirPengguna(DGPengguna.SelectedRows, true);
        }

        private void RMIUBlokirPengguna_Klik(object sender, EventArgs e)
        {
            BlokirPengguna(DGPengguna.SelectedRows, false);
        }


        private void RMITambahLFas_Klik(object sender, EventArgs e)
        {
            Form_AturLaporanFasilitas f = new Form_AturLaporanFasilitas(Form_AturLaporanFasilitas.Aksi.TambahLFasilitas);
            f.DataGridSegarkan += F_DataGridSegarkan;
            f.ShowDialog();
            if (f.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGLaporanFasilitas, "Data telah ditambahkan", eToastPosition.MiddleCenter);
            }
        }

        private void RMIUbahLFas_Klik(object sender, EventArgs e)
        {
            if (DGLaporanFasilitas.SelectedRows.Count == 0)
            {
                ToastNotification.Show(this, "Tidak ada data yang dipilih", null, 3000, eToastGlowColor.Orange);
                return;
            }

            string[] dataubah = new string[DGLaporanFasilitas.CurrentRow.Cells.Count];
            int i = 0;
            foreach (DataGridViewCell cell in DGLaporanFasilitas.CurrentRow.Cells)
            {
                dataubah[i] = cell.Value.ToString();
                i++;
            }
            Form_AturLaporanFasilitas f = new Form_AturLaporanFasilitas(Form_AturLaporanFasilitas.Aksi.UbahLFasilitas, dataubah, DGLaporanFasilitas, DGPengguna);
            f.DataGridSegarkan += new Form_AturLaporanFasilitas.DataGridSegarkanHandler(F_DataGridSegarkan);
            f.sem = DGLaporanFasilitas.CurrentCellAddress.Y;
            f.max = DGLaporanFasilitas.RowCount - 1;
            f.ShowDialog();
            if (f.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGLaporanFasilitas, "Data telah diubah", eToastPosition.MiddleCenter);
            }
        }

        private void RMIHapusLFas_Klik(object sender, EventArgs e)
        {
            HapusDataUtama(DGLaporanFasilitas.SelectedRows, "Laporan Fasilitas", "laporanfasilitas", false, "lfid");
        }

        private void RMIDetailLFas_Klik(object sender, EventArgs e)
        {
            Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Fasilitas, DGLaporanFasilitas.CurrentRow.Cells[0].Value.ToString());
            f.ShowDialog();
        }

        private void DGLaporanFasilitas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Fasilitas, DGLaporanFasilitas.CurrentRow.Cells[0].Value.ToString());
            f.ShowDialog();
        }


        private void RMITambahPlgr_Klik(object sender, EventArgs e)
        {
            Form_AturLaporanFasilitas f = new Form_AturLaporanFasilitas(Form_AturLaporanFasilitas.Aksi.TambahLPelanggaran);
            f.DataGridSegarkan += F_DataGridSegarkan;
            f.ShowDialog();
            if (f.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGLaporanPelanggaran, "Data telah ditambahkan", eToastPosition.MiddleCenter);
            }
        }

        private void RMIUbahPlgr_Klik(object sender, EventArgs e)
        {
            if (DGLaporanPelanggaran.SelectedRows.Count == 0)
            {
                ToastNotification.Show(this, "Tidak ada data yang dipilih", null, 3000, eToastGlowColor.Orange);
                return;
            }

            string[] dataubah = new string[DGLaporanPelanggaran.CurrentRow.Cells.Count];
            int i = 0;
            foreach (DataGridViewCell cell in DGLaporanPelanggaran.CurrentRow.Cells)
            {
                dataubah[i] = cell.Value.ToString();
                i++;
            }
            Form_AturLaporanFasilitas f = new Form_AturLaporanFasilitas(Form_AturLaporanFasilitas.Aksi.UbahLPelanggaran, dataubah, DGLaporanPelanggaran, DGPengguna);
            f.DataGridSegarkan += new Form_AturLaporanFasilitas.DataGridSegarkanHandler(F_DataGridSegarkan);
            f.sem = DGLaporanPelanggaran.CurrentCellAddress.Y;
            f.max = DGLaporanPelanggaran.RowCount - 1;
            f.ShowDialog();
            if (f.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGLaporanPelanggaran, "Data telah diubah", eToastPosition.MiddleCenter);
            }
        }

        private void RMIHapusPlgr_Klik(object sender, EventArgs e)
        {
            HapusDataUtama(DGLaporanPelanggaran.SelectedRows, "Laporan Pelanggaran", "laporanpelanggaran", false, "lpid");
        }

        private void RMIDetailLPlgr_Klik(object sender, EventArgs e)
        {
            try
            {
                Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Pelanggaran, DGLaporanPelanggaran.CurrentRow.Cells[0].Value.ToString());
                f.ShowDialog();
            }
            catch (Exception)
            {
                
            }
        }

        private void DGLaporanPelanggaran_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Pelanggaran, DGLaporanPelanggaran.CurrentRow.Cells[0].Value.ToString());
                f.ShowDialog();
            }
            catch (Exception)
            {
                
            }
        }
        
        private void BLFasDilaporkan_Click(object sender, EventArgs e)
        {
            Form_LaporanDilaporkan f = new Form_LaporanDilaporkan(Form_LaporanDilaporkan.Bagian.Fasilitas);
            f.ShowDialog();
        }

        private void BLPlgrDilaporkan_Click(object sender, EventArgs e)
        {
            Form_LaporanDilaporkan f = new Form_LaporanDilaporkan(Form_LaporanDilaporkan.Bagian.Pelanggaran);
            f.ShowDialog();
        }

        private void DGLKomentarFas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Fasilitas, DGLKomentarFas.CurrentRow.Cells[1].Value.ToString());
                f.ShowDialog();
            }
            catch (Exception)
            {

            }
        }

        private void DGLKomentarPlgr_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Pelanggaran, DGLKomentarPlgr.CurrentRow.Cells[1].Value.ToString());
                f.ShowDialog();
            }
            catch (Exception)
            {

            }
        }

        private void Form_Utama_Deactivate(object sender, MouseEventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.FromArgb(87, 171, 183);
        }

        List<Form_Obrolan> form = new List<Form_Obrolan>();

        private void DGObrolan_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (form.Count == 0)
            {
                Form_Obrolan f = new Form_Obrolan(DGObrolan.CurrentRow.Cells[0].Value.ToString(), DGObrolan.CurrentRow.Cells[1].Value.ToString());
                f.FormClosing += F_FormClosing;
                f.Show();
                form.Add(f);
            }
            else
            {
                Form_Obrolan f = new Form_Obrolan(DGObrolan.CurrentRow.Cells[0].Value.ToString(), DGObrolan.CurrentRow.Cells[1].Value.ToString());
                f.FormClosing += F_FormClosing1;
                bool tampilkan = true;
                Form_Obrolan form2 = null;
                foreach (Form_Obrolan item in form)
                {
                    if (item.Text == f.Text)
                    {
                        tampilkan = false;
                        form2 = item;
                    }
                }

                if (tampilkan)
                {
                    f.Show();
                    form.Add(f);
                }
                else
                {
                    if (form2.WindowState == FormWindowState.Minimized)
                    {
                        form2.WindowState = FormWindowState.Normal;
                    }
                    else
                    {
                        form2.BringToFront();
                    }
                }
            }
        }

        private void F_FormClosing1(object sender, FormClosingEventArgs e)
        {
            Form_Obrolan f = (Form_Obrolan) sender;
            form.Remove(f);
        }

        private void F_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form_Obrolan f = (Form_Obrolan)sender;
            form.Remove(f);
        }


        private void RMITambahSebagai_Klik(object sender, EventArgs e)
        {
            Form_AturSebagai f = new Form_AturSebagai(Form_AturSebagai.Aksi.Tambah);
            f.DataGridSegarkan += new Form_AturSebagai.DataGridSegarkanHandler(F_DataGridSegarkan);
            f.ShowDialog();
            if (f.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGPengguna, "Data telah ditambahkan", eToastPosition.MiddleCenter);
            }
        }

        private void RMIUbahSebagai_Klik(object sender, EventArgs e)
        {
            if (DGSebagai.SelectedRows.Count == 0)
            {
                ToastNotification.Show(this, "Tidak ada data yang dipilih", null, 3000, eToastGlowColor.Orange);
                return;
            }

            string[] dataubah = new string[DGSebagai.CurrentRow.Cells.Count];
            int i = 0;
            foreach (DataGridViewCell cell in DGSebagai.CurrentRow.Cells)
            {
                dataubah[i] = cell.Value.ToString();
                i++;
            }
            Form_AturSebagai f = new Form_AturSebagai(Form_AturSebagai.Aksi.Ubah, dataubah, DGSebagai);
            f.DataGridSegarkan += new Form_AturSebagai.DataGridSegarkanHandler(F_DataGridSegarkan);
            f.sem = DGSebagai.CurrentCellAddress.Y;
            f.max = DGSebagai.RowCount - 1;
            f.ShowDialog();
            if (f.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGSebagai, "Data telah diubah", eToastPosition.MiddleCenter);
            }
        }

        private void RMIHapusSebagai_Klik(object sender, EventArgs e)
        {
            HapusDataUtama(DGSebagai.SelectedRows, "Pengaturan Sebagai", "sebagai", false, "sbid");
        }

        private void TSlideShow_Tick(object sender, EventArgs e)
        {
            Slide();
        }

        private void PBFeedFas_Klik(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            if (pb.Image != null)
            {
                Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Fasilitas, curidfas);
                f.ShowDialog();
            }
        }

        private void PBFeedPlgr_Klik(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            if (pb.Image != null)
            {
                Form_DetailLaporan f = new Form_DetailLaporan(Form_DetailLaporan.Bagian.Pelanggaran, curidplgr);
                f.ShowDialog();
            }
        }

        private void TSlideShowPlgr_Tick(object sender, EventArgs e)
        {
            Slide2();
        }

        private void radialMenu1_MouseEnter(object sender, EventArgs e)
        {
            RMIMenuUtama.SymbolSize = 10;
        }

        private void RMIMenuUtama_MouseLeave(object sender, EventArgs e)
        {
            RMIMenuUtama.SymbolSize = 14;
        }

        private void RMITentangAplikasi_Klik(object sender, EventArgs e)
        {
            Form_Tentang f = new Form_Tentang();
            f.ShowDialog();
        }

        private void RMIFacebook_Klik(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.facebook.com/dilittlesnoopy");

        }

        private void RMIBantuan_Klik(object sender, EventArgs e)
        {
            Form_Bantuan f = new Form_Bantuan();
            f.ShowDialog();
        }

        private void RMITwitter_Klik(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://twitter.com/Agateophobea");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (refresh.KoneksiBagus)
            {
                LStatusKoneksi.Symbol = "";
            }
            else
            {
                LStatusKoneksi.Symbol = "";
            }

            if (this.Focused)
            {
                LStatusKoneksi.BackColor = Color.FromArgb(87, 171, 183);
            }
        }

        private void DGPengguna_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            TSSLNimAnggota.Text = DGPengguna.CurrentRow.Cells[0].Value.ToString();
            LDSAnggota.Text = DGPengguna.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
        }

        private void DGLaporanFasilitas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            TSSLNimStruktur.Text = DGLaporanFasilitas.CurrentRow.Cells[0].Value.ToString();
            LDSStruktur.Text = DGLaporanFasilitas.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
        }

        private void DGLaporanPelanggaran_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            TSSLNimPiket.Text = DGLaporanPelanggaran.CurrentRow.Cells[0].Value.ToString();
            LDSPiket.Text = DGLaporanPelanggaran.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
        }

        private void DGLKomentarFas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;
            TSSLNamaRapat.Text = dg.CurrentRow.Cells[0].Value.ToString();
            LDSRapat.Text = dg.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
        }

        private void DGSebagai_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            TSSLIdKeuangan.Text = DGSebagai.CurrentRow.Cells[0].Value.ToString();
            LDSKeuangan.Text = ConvertToBoolean(DGSebagai.CurrentRow.Cells[e.ColumnIndex].Value.ToString()).ToString();
        }

        private void DGPengguna_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            TSSLBanyakDataAnggota.Text = DGPengguna.RowCount.ToString();
        }

        private void DGLaporanFasilitas_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            TSSLBanyakDataStruktur.Text = DGLaporanFasilitas.RowCount.ToString();
        }

        private void DGLaporanPelanggaran_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            TSSLBanyakDataPiket.Text = (DGLKomentarFas.RowCount + DGLKomentarPlgr.RowCount).ToString();
        }

        private void DGLKomentarFas_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            TSSLBanyakDataRapat.Text = DGLKomentarFas.RowCount.ToString();
        }

        private void DGObrolan_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            TSSLBanyakDataProker.Text = DGObrolan.RowCount.ToString();
        }

        private void DGSebagai_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            TSSLTotalDana.Text = DGSebagai.RowCount.ToString();
        }

        private bool ConvertToBoolean(string nilai)
        {
            bool hasil = false;
            if (nilai == "1")
            {
                hasil = true;
            }
            return hasil;
        }
    }
}
