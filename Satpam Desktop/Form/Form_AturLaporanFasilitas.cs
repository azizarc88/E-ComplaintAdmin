﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Satpam_Desktop.Class;
using System.Collections.Specialized;

namespace Satpam_Desktop.Form
{
    public partial class Form_AturLaporanFasilitas : DevComponents.DotNetBar.Metro.MetroForm
    {
        public int sem { set; get; }
        public int max { set; get; }
        private DataGridView DataGrid, DataGridPengguna;
        private Aksi aksi;
        private RESTService rs = new RESTService();
        private string[] dataubah;
        private string pgid = "tidak ada";
        private string alamatfoto;

        public delegate void DataGridSegarkanHandler(object sender, DataGridSegarkanEventArgs e);
        public event DataGridSegarkanHandler DataGridSegarkan;

        public enum Aksi
        {
            TambahLFasilitas = 0,
            UbahLFasilitas = 1,
            TambahLPelanggaran = 2,
            UbahLPelanggaran = 3
        }

        private void Bersihkan()
        {
            TBPesan.Text = "";
            TBLat.Text = "";
            TBLing.Text = "";
            TBAlamat.Text = "";
            CBPrioritas.Text = "";
            CBLaporanRhs.Checked = false;
            LNamaPengguna.Text = "Belum ditentukan";
            PBFoto.Image = null;
            DTDilaporkanPada.Text = null;
        }

        private bool isAllValid()
        {
            bool valid = true;
            string pesan = "";
            float result;
            string lat, ling;
            lat = TBLat.Text.Replace(".", ",");
            ling = TBLing.Text.Replace(".", ",");

            if (TBPesan.Text == "")
            {
                pesan = "Keluhan wajib diisi";
                TBPesan.Focus();
                valid = false;
            }
            else if (TBLat.Text == "")
            {
                pesan = "Lokasi latitude wajib diisi";
                TBLat.Focus();
                valid = false;
            }
            else if (TBLing.Text == "")
            {
                pesan = "Lokasi longitude wajib diisi";
                TBLing.Focus();
                valid = false;
            }
            else if (CBPrioritas.SelectedItem.ToString() == "")
            {
                pesan = "Prioritas wajib diisi";
                CBPrioritas.Focus();
                valid = false;
            }
            else if (pgid == "tidak ada")
            {
                pesan = "Dilaporkan oleh wajib diisi";
                BPilihPengguna.Focus();
                valid = false;
            }
            else if (!float.TryParse(lat, out result))
            {
                pesan = "Latitude harus angka";
                TBLat.Focus();
                valid = false;
            }
            else if (!float.TryParse(ling, out result))
            {
                pesan = "Longitude harus angka";
                TBLing.Focus();
                valid = false;
            }
            else if (alamatfoto == "")
            {
                pesan = "Foto laporan fasilitas wajib diisi";
                BPilihFoto.Focus();
                valid = false;
            }
            else if (PBFoto.Image == PBFoto.ErrorImage)
            {
                pesan = "Pengambilan foto gagal, silakan ulangi, atau pilih foto baru";
                BPilihFoto.Focus();
                valid = false;
            }

            if (!valid)
            {
                ToastNotification.Show(this, pesan, null, 3000, eToastGlowColor.Orange);
            }

            return valid;
        }

        public Form_AturLaporanFasilitas(Aksi aksi, string[] dataubah = null, DataGridView DataGrid = null, DataGridView DataGridPengguna = null)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
            this.aksi = aksi;

            if (aksi == Aksi.TambahLFasilitas)
            {
                this.Text = "Tambah Laporan Fasilitas";
                BSimpan.Text = "Tambah";
                BNext.Enabled = false;
                BPrev.Enabled = false;
                DTDilaporkanPada.Enabled = false;
                IITotalDukungan.Enabled = false;
                IITotalKomentar.Enabled = false;
                BRefreshTDukungan.Enabled = false;
                BRefreshTKomentar.Enabled = false;
            }
            else if (aksi == Aksi.UbahLFasilitas)
            {
                this.dataubah = dataubah;
                this.DataGrid = DataGrid;
                this.DataGridPengguna = DataGridPengguna;
                this.Text = "Ubah Laporan Kecelakaan";
                BSimpan.Text = "Ubah";
                BNext.Enabled = true;
                BPrev.Enabled = true;

                MasukkanData();
            }
            else if (aksi == Aksi.TambahLPelanggaran)
            {
                this.Text = "Tambah Laporan Pelanggaran";
                LPesan.Text = "Pelanggaran";
                BSimpan.Text = "Tambah";
                BNext.Enabled = false;
                BPrev.Enabled = false;
                DTDilaporkanPada.Enabled = false;
                IITotalDukungan.Enabled = false;
                IITotalKomentar.Enabled = false;
                BRefreshTDukungan.Enabled = false;
                BRefreshTKomentar.Enabled = false;
            }
            else if (aksi == Aksi.UbahLPelanggaran)
            {
                this.dataubah = dataubah;
                this.DataGrid = DataGrid;
                this.DataGridPengguna = DataGridPengguna;
                this.Text = "Ubah Laporan Pelanggaran";
                LPesan.Text = "Pelanggaran";
                BSimpan.Text = "Ubah";
                BNext.Enabled = true;
                BPrev.Enabled = true;

                MasukkanData();
            }
        }

        private string AmbilNama(string pgid)
        {
            string hasil = "";

            foreach (DataGridViewRow baris in DataGridPengguna.Rows)
            {
                if (baris.Cells[0].Value.ToString() == pgid)
                {
                    hasil = baris.Cells[1].Value.ToString();
                }
            }

            return hasil;
        }

        private void MasukkanData()
        {
            TBPesan.Text = dataubah[2];
            TBLat.Text = dataubah[3];
            TBLing.Text = dataubah[4];
            TBAlamat.Text = dataubah[5];
            CBPrioritas.Text = dataubah[6];
            if (dataubah[9] == "1")
            {
                CBLaporanRhs.Checked = true;
            }
            else
            {
                CBLaporanRhs.Checked = false;
            }
            pgid = dataubah[8];
            LNamaPengguna.Text = AmbilNama(pgid);
            PBFoto.LoadAsync(dataubah[1]);
            DTDilaporkanPada.Text = dataubah[7];
            IITotalDukungan.Text = dataubah[10];
            IITotalKomentar.Text = dataubah[11];
        }

        private void Form_AturLaporanFasilitas_Load(object sender, EventArgs e)
        {
            TBPesan.Focus();
            DTDilaporkanPada.MaxDate = DateTime.Now;
            rs.RequestComplete += Rs_RequestComplete; ;
        }

        private void Rs_RequestComplete(object sender, RequestCompleteEventArgs e)
        {
            string hasil = rs.GetString(e.Response, "hasil");

            if (e.Bagian == "LFasTambah")
            {
                if (hasil == "SUDAH ADA")
                {
                    ToastNotification.Show(this, "Data yang Anda masukkan sudah ada", null, 3000, eToastGlowColor.Orange);
                }
                else if (hasil == "GAGAL")
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
                else if (hasil == "SUKSES")
                {
                    DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Laporan Fasilitas");
                    DataGridSegarkan(this, args);

                    if (CBOtoClose.Checked == true)
                    {
                        this.Close();
                        this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                        return;
                    }

                    ToastNotification.Show(this, "Data telah ditambahkan", null, 3000, eToastGlowColor.Blue);
                    Bersihkan();
                    TBPesan.Focus();
                }
                else
                {
                    MessageBox.Show(e.Response);
                }
            }
            else if (e.Bagian == "LFasUbah")
            {
                if (hasil == "SUKSES")
                {
                    DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Laporan Fasilitas");
                    DataGridSegarkan(this, args);

                    if (CBOtoClose.Checked == true)
                    {
                        this.Close();
                        this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                        return;
                    }

                    ToastNotification.Show(this, "Data telah diubah", null, 3000, eToastGlowColor.Blue);
                    TBPesan.Focus();
                }
                else
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "LFasTDukungan")
            {
                if (hasil == "SUKSES")
                {
                    IITotalDukungan.Text = rs.GetString(e.Response, "total");
                    ToastNotification.Show(this, rs.GetString(e.Response, "total") + " Dukungan", null, 1000, eToastGlowColor.Blue);
                }
                else
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "LFasTKomentar")
            {
                if (hasil == "SUKSES")
                {
                    IITotalKomentar.Text = rs.GetString(e.Response, "total");
                    ToastNotification.Show(this, rs.GetString(e.Response, "total") + " Komentar", null, 1000, eToastGlowColor.Blue);
                }
                else
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "LPlgrTambah")
            {
                if (hasil == "SUDAH ADA")
                {
                    ToastNotification.Show(this, "Data yang Anda masukkan sudah ada", null, 3000, eToastGlowColor.Orange);
                }
                else if (hasil == "GAGAL")
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
                else if (hasil == "SUKSES")
                {
                    DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Laporan Pelanggaran");
                    DataGridSegarkan(this, args);

                    if (CBOtoClose.Checked == true)
                    {
                        this.Close();
                        this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                        return;
                    }

                    ToastNotification.Show(this, "Data telah ditambahkan", null, 3000, eToastGlowColor.Blue);
                    Bersihkan();
                    TBPesan.Focus();
                }
            }
            else if (e.Bagian == "LPlgrUbah")
            {
                if (hasil == "SUKSES")
                {
                    DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Laporan Pelanggaran");
                    DataGridSegarkan(this, args);

                    if (CBOtoClose.Checked == true)
                    {
                        this.Close();
                        this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                        return;
                    }

                    ToastNotification.Show(this, "Data telah diubah", null, 3000, eToastGlowColor.Blue);
                    TBPesan.Focus();
                }
                else
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "LPlgrTDukungan")
            {
                if (hasil == "SUKSES")
                {
                    IITotalDukungan.Text = rs.GetString(e.Response, "total");
                    ToastNotification.Show(this, rs.GetString(e.Response, "total") + " Dukungan", null, 1000, eToastGlowColor.Blue);
                }
                else
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "LPlgrTKomentar")
            {
                if (hasil == "SUKSES")
                {
                    IITotalKomentar.Text = rs.GetString(e.Response, "total");
                    ToastNotification.Show(this, rs.GetString(e.Response, "total") + " Komentar", null, 1000, eToastGlowColor.Blue);
                }
                else
                {
                    ToastNotification.Show(this, "Terjadi kesalahan, silakan coba lagi - " + e.Bagian, null, 3000, eToastGlowColor.Red);
                }
            }
        }

        private void BPrev_Click(object sender, EventArgs e)
        {
            sem--;
            if (sem < 0)
            {
                sem = max;
            }
            TBPesan.Focus();

            string[] dataubah = new string[DataGrid.Rows[sem].Cells.Count];
            int i = 0;
            foreach (DataGridViewCell cell in DataGrid.Rows[sem].Cells)
            {
                dataubah[i] = cell.Value.ToString();
                i++;
            }
            this.dataubah = dataubah;
            MasukkanData();
        }

        private void BNext_Click(object sender, EventArgs e)
        {
            sem++;
            if (sem > max)
            {
                sem = 0;
            }
            TBPesan.Focus();

            string[] dataubah = new string[DataGrid.Rows[sem].Cells.Count];
            int i = 0;
            foreach (DataGridViewCell cell in DataGrid.Rows[sem].Cells)
            {
                dataubah[i] = cell.Value.ToString();
                i++;
            }
            this.dataubah = dataubah;
            MasukkanData();
        }

        private void BPilihFoto_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                alamatfoto = openFileDialog1.FileName;
                PBFoto.Image = Image.FromFile(alamatfoto);
            }
        }

        private void BPilihPengguna_Click(object sender, EventArgs e)
        {
            Form_PilihAnggota FrmPilihItem = new Form_PilihAnggota();
            FrmPilihItem.ItemTerpilih += new Form_PilihAnggota.ItemTerpilihHandler(ItemTerpilih);
            FrmPilihItem.ShowDialog();
        }

        private void ItemTerpilih(object sender, ItemTerpilihEventArgs e)
        {
            pgid = e.Daftar[0];
            LNamaPengguna.Text = e.Daftar[1];
        }

        private void BSimpan_Click(object sender, EventArgs e)
        {
            proses();
        }

        private void BRefreshTDukungan_Click(object sender, EventArgs e)
        {
            if (this.Text.Contains("Fasilitas"))
            {
                NameValueCollection formData = new NameValueCollection();
                formData["tabel"] = "dukunganfasilitas";
                formData["id"] = dataubah[0];
                rs.MakeRequestAsync("ambil_total_dukungan.php", formData, "LFasTDukungan");
            }
            else if (this.Text.Contains("Pelanggaran"))
            {
                NameValueCollection formData = new NameValueCollection();
                formData["tabel"] = "dukunganpelanggaran";
                formData["id"] = dataubah[0];
                rs.MakeRequestAsync("ambil_total_dukungan.php", formData, "LPlgrTDukungan");
            }
        }

        private void BRefreshTKomentar_Click(object sender, EventArgs e)
        {
            if (this.Text.Contains("Fasilitas"))
            {
                NameValueCollection formData = new NameValueCollection();
                formData["tabel"] = "komentar_fasilitas";
                formData["id"] = dataubah[0];
                rs.MakeRequestAsync("ambil_total_komentar.php", formData, "LFasTKomentar");
            }
            else if (this.Text.Contains("Pelanggaran"))
            {
                NameValueCollection formData = new NameValueCollection();
                formData["tabel"] = "komentar_pelanggaran";
                formData["id"] = dataubah[0];
                rs.MakeRequestAsync("ambil_total_komentar.php", formData, "LPlgrTKomentar");
            }
        }

        public String getStringImage(Image bmp)
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            bmp.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
            byte[] imageBytes = stream.ToArray();
            
            return Convert.ToBase64String(imageBytes);
        }

        private void proses()
        {
            if (aksi == Aksi.TambahLFasilitas)
            {
                if (isAllValid())
                {
                    NameValueCollection formData = new NameValueCollection();
                    formData["foto"] = getStringImage(Image.FromFile(alamatfoto));
                    formData["keluhan"] = TBPesan.Text;
                    formData["lat"] = TBLat.Text;
                    formData["ling"] = TBLing.Text;
                    formData["alamat"] = TBAlamat.Text;
                    formData["prioritas"] = CBPrioritas.SelectedItem.ToString();
                    formData["dilaporkanpada"] = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
                    formData["dilaporkanoleh"] = pgid;
                    string l = "0";
                    if (CBLaporanRhs.Checked)
                    {
                        l = "1";
                    }
                    else
                    {
                        l = "0";
                    }
                    formData["laporanrahasia"] = l;
                    rs.MakeRequestAsync("tambah_laporanfasilitas.php", formData, "LFasTambah");
                }
            }
            else if (aksi == Aksi.UbahLFasilitas)
            {
                if (isAllValid())
                {
                    NameValueCollection formData = new NameValueCollection();
                    formData["lfid"] = dataubah[0];
                    try
                    {
                        formData["foto"] = formData["foto"] = getStringImage(Image.FromFile(alamatfoto));
                    }
                    catch (Exception)
                    {
                        formData["foto"] = getStringImage(PBFoto.Image);
                    }
                    formData["keluhan"] = TBPesan.Text;
                    formData["lat"] = TBLat.Text;
                    formData["ling"] = TBLing.Text;
                    formData["alamat"] = TBAlamat.Text;
                    formData["prioritas"] = CBPrioritas.SelectedItem.ToString();
                    formData["dilaporkanpada"] = DTDilaporkanPada.Value.Year + "-" + DTDilaporkanPada.Value.Month + "-" + DTDilaporkanPada.Value.Day + " " + DTDilaporkanPada.Value.Hour + ":" + DTDilaporkanPada.Value.Minute + ":" + DTDilaporkanPada.Value.Second;
                    formData["dilaporkanoleh"] = pgid;
                    string l = "0";
                    if (CBLaporanRhs.Checked)
                    {
                        l = "1";
                    }
                    else
                    {
                        l = "0";
                    }
                    formData["laporanrahasia"] = l;
                    formData["dukungan"] = IITotalDukungan.Value.ToString();
                    formData["komentar"] = IITotalKomentar.Value.ToString();
                    rs.MakeRequestAsync("ubah_laporanfasilitas.php", formData, "LFasUbah");
                }
            }
            else if (aksi == Aksi.TambahLPelanggaran)
            {
                if (isAllValid())
                {
                    NameValueCollection formData = new NameValueCollection();
                    formData["foto"] = getStringImage(Image.FromFile(alamatfoto));
                    formData["pelanggaran"] = TBPesan.Text;
                    formData["lat"] = TBLat.Text;
                    formData["ling"] = TBLing.Text;
                    formData["alamat"] = TBAlamat.Text;
                    formData["prioritas"] = CBPrioritas.SelectedItem.ToString();
                    formData["dilaporkanpada"] = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
                    formData["dilaporkanoleh"] = pgid;
                    string l = "0";
                    if (CBLaporanRhs.Checked)
                    {
                        l = "1";
                    }
                    else
                    {
                        l = "0";
                    }
                    formData["laporanrahasia"] = l;
                    rs.MakeRequestAsync("tambah_laporanpelanggaran.php", formData, "LPlgrTambah");
                }
            }
            else if (aksi == Aksi.UbahLPelanggaran)
            {
                if (isAllValid())
                {
                    NameValueCollection formData = new NameValueCollection();
                    formData["lpid"] = dataubah[0];
                    try
                    {
                        formData["foto"] = getStringImage(Image.FromFile(alamatfoto));
                    }
                    catch (Exception)
                    {
                        formData["foto"] = getStringImage(PBFoto.Image);
                    }
                    formData["pelanggaran"] = TBPesan.Text;
                    formData["lat"] = TBLat.Text;
                    formData["ling"] = TBLing.Text;
                    formData["alamat"] = TBAlamat.Text;
                    formData["prioritas"] = CBPrioritas.SelectedItem.ToString();
                    formData["dilaporkanpada"] = DTDilaporkanPada.Value.Year + "-" + DTDilaporkanPada.Value.Month + "-" + DTDilaporkanPada.Value.Day + " " + DTDilaporkanPada.Value.Hour + ":" + DTDilaporkanPada.Value.Minute + ":" + DTDilaporkanPada.Value.Second;
                    formData["dilaporkanoleh"] = pgid;
                    string l = "0";
                    if (CBLaporanRhs.Checked)
                    {
                        l = "1";
                    }
                    else
                    {
                        l = "0";
                    }
                    formData["laporanrahasia"] = l;
                    formData["dukungan"] = IITotalDukungan.Value.ToString();
                    formData["komentar"] = IITotalKomentar.Value.ToString();
                    rs.MakeRequestAsync("ubah_laporanpelanggaran.php", formData, "LPlgrUbah");
                }
            }
        }
    }
}
