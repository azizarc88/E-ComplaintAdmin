﻿namespace Satpam_Desktop.Form
{
    partial class Form_Pemberitahuan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BHide = new DevComponents.DotNetBar.ButtonX();
            this.TBInfo = new System.Windows.Forms.TextBox();
            this.LAlamat = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.BDetail = new DevComponents.DotNetBar.ButtonX();
            this.LPrioritas = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BHide
            // 
            this.BHide.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BHide.AntiAlias = true;
            this.BHide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.BHide.CausesValidation = false;
            this.BHide.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BHide.Dock = System.Windows.Forms.DockStyle.Left;
            this.BHide.EnableMarkup = false;
            this.BHide.FocusCuesEnabled = false;
            this.BHide.Location = new System.Drawing.Point(0, 0);
            this.BHide.Name = "BHide";
            this.BHide.Size = new System.Drawing.Size(23, 186);
            this.BHide.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.BHide.Symbol = "";
            this.BHide.SymbolSize = 18F;
            this.BHide.TabIndex = 9;
            this.BHide.TextColor = System.Drawing.Color.DarkGray;
            this.BHide.Click += new System.EventHandler(this.BHide_Click);
            // 
            // TBInfo
            // 
            this.TBInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.TBInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBInfo.CausesValidation = false;
            this.TBInfo.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.TBInfo.Font = new System.Drawing.Font("Bailey Sans ITC TT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBInfo.ForeColor = System.Drawing.Color.Black;
            this.TBInfo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.TBInfo.Location = new System.Drawing.Point(24, 19);
            this.TBInfo.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TBInfo.Multiline = true;
            this.TBInfo.Name = "TBInfo";
            this.TBInfo.ReadOnly = true;
            this.TBInfo.ShortcutsEnabled = false;
            this.TBInfo.Size = new System.Drawing.Size(351, 146);
            this.TBInfo.TabIndex = 10;
            this.TBInfo.TabStop = false;
            this.TBInfo.Text = "ullala ulala kjgjhg jhg kjhg kj kjh h kjgh kv mhc yur xckl iyg lkihg liyh";
            this.TBInfo.Click += new System.EventHandler(this.TBInfo_Click);
            // 
            // LAlamat
            // 
            this.LAlamat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LAlamat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LAlamat.CausesValidation = false;
            this.LAlamat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LAlamat.ForeColor = System.Drawing.Color.Black;
            this.LAlamat.Location = new System.Drawing.Point(209, 170);
            this.LAlamat.Name = "LAlamat";
            this.LAlamat.Size = new System.Drawing.Size(163, 13);
            this.LAlamat.TabIndex = 12;
            this.LAlamat.Text = "Satpam";
            this.LAlamat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // timer1
            // 
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // BDetail
            // 
            this.BDetail.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BDetail.AntiAlias = true;
            this.BDetail.BackColor = System.Drawing.SystemColors.Control;
            this.BDetail.CausesValidation = false;
            this.BDetail.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BDetail.EnableMarkup = false;
            this.BDetail.FocusCuesEnabled = false;
            this.BDetail.Location = new System.Drawing.Point(24, 169);
            this.BDetail.Name = "BDetail";
            this.BDetail.Size = new System.Drawing.Size(15, 14);
            this.BDetail.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.BDetail.Symbol = "";
            this.BDetail.SymbolSize = 11F;
            this.BDetail.TabIndex = 13;
            this.BDetail.TextColor = System.Drawing.Color.DarkGray;
            this.BDetail.Click += new System.EventHandler(this.BDetail_Click);
            // 
            // LPrioritas
            // 
            this.LPrioritas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LPrioritas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LPrioritas.CausesValidation = false;
            this.LPrioritas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LPrioritas.ForeColor = System.Drawing.Color.Black;
            this.LPrioritas.Location = new System.Drawing.Point(209, 3);
            this.LPrioritas.Name = "LPrioritas";
            this.LPrioritas.Size = new System.Drawing.Size(163, 13);
            this.LPrioritas.TabIndex = 14;
            this.LPrioritas.Text = "Tinggi";
            this.LPrioritas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form_Pemberitahuan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 186);
            this.ControlBox = false;
            this.Controls.Add(this.LPrioritas);
            this.Controls.Add(this.BDetail);
            this.Controls.Add(this.LAlamat);
            this.Controls.Add(this.TBInfo);
            this.Controls.Add(this.BHide);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form_Pemberitahuan";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.Form_Pemberitahuan_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal DevComponents.DotNetBar.ButtonX BHide;
        internal System.Windows.Forms.TextBox TBInfo;
        internal System.Windows.Forms.Label LAlamat;
        private System.Windows.Forms.Timer timer1;
        internal DevComponents.DotNetBar.ButtonX BDetail;
        internal System.Windows.Forms.Label LPrioritas;
    }
}