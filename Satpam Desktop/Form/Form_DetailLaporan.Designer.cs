﻿namespace Satpam_Desktop.Form
{
    partial class Form_DetailLaporan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GroupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.PBFoto = new System.Windows.Forms.PictureBox();
            this.TBStatus = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.GroupPanel5 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.LLokasi = new DevComponents.DotNetBar.LabelX();
            this.LInformasi = new System.Windows.Forms.Label();
            this.LWaktu = new System.Windows.Forms.Label();
            this.LNama = new System.Windows.Forms.Label();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.DGKomentar = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.idlaporan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.komentarku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reflectionLabel1 = new DevComponents.DotNetBar.Controls.ReflectionLabel();
            this.BKirimKomen = new DevComponents.DotNetBar.ButtonX();
            this.TBKomentar = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.GroupPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBFoto)).BeginInit();
            this.GroupPanel5.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGKomentar)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupPanel4
            // 
            this.GroupPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GroupPanel4.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GroupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.GroupPanel4.Controls.Add(this.PBFoto);
            this.GroupPanel4.Controls.Add(this.TBStatus);
            this.GroupPanel4.Controls.Add(this.GroupPanel5);
            this.GroupPanel4.Controls.Add(this.LWaktu);
            this.GroupPanel4.Controls.Add(this.LNama);
            this.GroupPanel4.DrawTitleBox = false;
            this.GroupPanel4.Location = new System.Drawing.Point(12, 12);
            this.GroupPanel4.Name = "GroupPanel4";
            this.GroupPanel4.Size = new System.Drawing.Size(519, 348);
            // 
            // 
            // 
            this.GroupPanel4.Style.BackColor = System.Drawing.Color.White;
            this.GroupPanel4.Style.BackColor2 = System.Drawing.Color.White;
            this.GroupPanel4.Style.BackColorGradientAngle = 90;
            this.GroupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel4.Style.BorderBottomWidth = 1;
            this.GroupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.GroupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel4.Style.BorderLeftWidth = 1;
            this.GroupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel4.Style.BorderRightWidth = 1;
            this.GroupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel4.Style.BorderTopWidth = 1;
            this.GroupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.GroupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.GroupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.GroupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.GroupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.GroupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.GroupPanel4.TabIndex = 10;
            // 
            // PBFoto
            // 
            this.PBFoto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PBFoto.BackColor = System.Drawing.Color.White;
            this.PBFoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PBFoto.ForeColor = System.Drawing.Color.Black;
            this.PBFoto.Location = new System.Drawing.Point(7, 110);
            this.PBFoto.Name = "PBFoto";
            this.PBFoto.Size = new System.Drawing.Size(495, 189);
            this.PBFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PBFoto.TabIndex = 11;
            this.PBFoto.TabStop = false;
            this.PBFoto.Click += new System.EventHandler(this.PBFoto_Click);
            // 
            // TBStatus
            // 
            this.TBStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBStatus.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TBStatus.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TBStatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.TBStatus.ForeColor = System.Drawing.Color.Black;
            this.TBStatus.Location = new System.Drawing.Point(7, 50);
            this.TBStatus.Multiline = true;
            this.TBStatus.Name = "TBStatus";
            this.TBStatus.ReadOnly = true;
            this.TBStatus.Size = new System.Drawing.Size(495, 54);
            this.TBStatus.TabIndex = 10;
            // 
            // GroupPanel5
            // 
            this.GroupPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupPanel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GroupPanel5.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GroupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.GroupPanel5.Controls.Add(this.LLokasi);
            this.GroupPanel5.Controls.Add(this.LInformasi);
            this.GroupPanel5.DrawTitleBox = false;
            this.GroupPanel5.Location = new System.Drawing.Point(-1, 305);
            this.GroupPanel5.Name = "GroupPanel5";
            this.GroupPanel5.Size = new System.Drawing.Size(511, 34);
            // 
            // 
            // 
            this.GroupPanel5.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(251)))), ((int)(((byte)(251)))));
            this.GroupPanel5.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(251)))), ((int)(((byte)(251)))));
            this.GroupPanel5.Style.BackColorGradientAngle = 90;
            this.GroupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel5.Style.BorderBottomWidth = 1;
            this.GroupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.GroupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel5.Style.BorderLeftWidth = 1;
            this.GroupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel5.Style.BorderRightWidth = 1;
            this.GroupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel5.Style.BorderTopWidth = 1;
            this.GroupPanel5.Style.CornerDiameter = 4;
            this.GroupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.GroupPanel5.Style.CornerTypeBottomLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.GroupPanel5.Style.CornerTypeBottomRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.GroupPanel5.Style.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Square;
            this.GroupPanel5.Style.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Square;
            this.GroupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.GroupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.GroupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.GroupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.GroupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.GroupPanel5.TabIndex = 9;
            // 
            // LLokasi
            // 
            this.LLokasi.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.LLokasi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LLokasi.Location = new System.Drawing.Point(191, 6);
            this.LLokasi.Name = "LLokasi";
            this.LLokasi.Size = new System.Drawing.Size(311, 23);
            this.LLokasi.TabIndex = 2;
            this.LLokasi.Text = "labelX1";
            this.LLokasi.TextAlignment = System.Drawing.StringAlignment.Far;
            this.LLokasi.Click += new System.EventHandler(this.LLokasi_Click);
            // 
            // LInformasi
            // 
            this.LInformasi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LInformasi.AutoSize = true;
            this.LInformasi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LInformasi.Font = new System.Drawing.Font("Arial", 9F);
            this.LInformasi.ForeColor = System.Drawing.Color.Black;
            this.LInformasi.Location = new System.Drawing.Point(6, 10);
            this.LInformasi.Name = "LInformasi";
            this.LInformasi.Size = new System.Drawing.Size(35, 15);
            this.LInformasi.TabIndex = 0;
            this.LInformasi.Text = "Suka";
            this.LInformasi.Click += new System.EventHandler(this.LInformasi_Click);
            // 
            // LWaktu
            // 
            this.LWaktu.AutoSize = true;
            this.LWaktu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LWaktu.Font = new System.Drawing.Font("Arial", 7F);
            this.LWaktu.ForeColor = System.Drawing.Color.Black;
            this.LWaktu.Location = new System.Drawing.Point(5, 28);
            this.LWaktu.Name = "LWaktu";
            this.LWaktu.Size = new System.Drawing.Size(33, 13);
            this.LWaktu.TabIndex = 8;
            this.LWaktu.Text = "waktu";
            // 
            // LNama
            // 
            this.LNama.AutoSize = true;
            this.LNama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LNama.Font = new System.Drawing.Font("Arial", 10.5F, System.Drawing.FontStyle.Bold);
            this.LNama.ForeColor = System.Drawing.Color.Black;
            this.LNama.Location = new System.Drawing.Point(4, 10);
            this.LNama.Name = "LNama";
            this.LNama.Size = new System.Drawing.Size(121, 16);
            this.LNama.TabIndex = 7;
            this.LNama.Text = "Nama Pengguna";
            // 
            // groupPanel1
            // 
            this.groupPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.DGKomentar);
            this.groupPanel1.Controls.Add(this.reflectionLabel1);
            this.groupPanel1.Controls.Add(this.BKirimKomen);
            this.groupPanel1.Controls.Add(this.TBKomentar);
            this.groupPanel1.DrawTitleBox = false;
            this.groupPanel1.Location = new System.Drawing.Point(12, 366);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(519, 175);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor = System.Drawing.Color.White;
            this.groupPanel1.Style.BackColor2 = System.Drawing.Color.White;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 11;
            // 
            // DGKomentar
            // 
            this.DGKomentar.AllowUserToAddRows = false;
            this.DGKomentar.AllowUserToDeleteRows = false;
            this.DGKomentar.AllowUserToResizeColumns = false;
            this.DGKomentar.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.DGKomentar.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGKomentar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGKomentar.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.DGKomentar.BackgroundColor = System.Drawing.Color.White;
            this.DGKomentar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DGKomentar.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.DGKomentar.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.DGKomentar.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGKomentar.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGKomentar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGKomentar.ColumnHeadersVisible = false;
            this.DGKomentar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idlaporan,
            this.komentarku,
            this.Column1,
            this.Column2});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGKomentar.DefaultCellStyle = dataGridViewCellStyle3;
            this.DGKomentar.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DGKomentar.EnableHeadersVisualStyles = false;
            this.DGKomentar.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGKomentar.HighlightSelectedColumnHeaders = false;
            this.DGKomentar.Location = new System.Drawing.Point(3, 3);
            this.DGKomentar.MultiSelect = false;
            this.DGKomentar.Name = "DGKomentar";
            this.DGKomentar.PaintEnhancedSelection = false;
            this.DGKomentar.ReadOnly = true;
            this.DGKomentar.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGKomentar.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DGKomentar.RowHeadersVisible = false;
            this.DGKomentar.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.DGKomentar.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.DGKomentar.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.DGKomentar.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
            this.DGKomentar.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.DGKomentar.ScrollBarAppearance = DevComponents.DotNetBar.eScrollBarAppearance.Default;
            this.DGKomentar.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGKomentar.SelectAllSignVisible = false;
            this.DGKomentar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGKomentar.ShowCellErrors = false;
            this.DGKomentar.ShowCellToolTips = false;
            this.DGKomentar.ShowEditingIcon = false;
            this.DGKomentar.ShowRowErrors = false;
            this.DGKomentar.Size = new System.Drawing.Size(503, 129);
            this.DGKomentar.TabIndex = 0;
            this.DGKomentar.UseCustomBackgroundColor = true;
            this.DGKomentar.Visible = false;
            this.DGKomentar.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGKomentar_CellClick);
            // 
            // idlaporan
            // 
            this.idlaporan.HeaderText = "Column3";
            this.idlaporan.Name = "idlaporan";
            this.idlaporan.ReadOnly = true;
            this.idlaporan.Visible = false;
            this.idlaporan.Width = 5;
            // 
            // komentarku
            // 
            this.komentarku.HeaderText = "KOMENTARKU";
            this.komentarku.Name = "komentarku";
            this.komentarku.ReadOnly = true;
            this.komentarku.Visible = false;
            this.komentarku.Width = 5;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.FillWeight = 5F;
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 5;
            // 
            // reflectionLabel1
            // 
            this.reflectionLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reflectionLabel1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.reflectionLabel1.BackgroundStyle.BackColor = System.Drawing.Color.White;
            this.reflectionLabel1.BackgroundStyle.BackColor2 = System.Drawing.Color.White;
            this.reflectionLabel1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.reflectionLabel1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.reflectionLabel1.BackgroundStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(195)))), ((int)(((byte)(198)))));
            this.reflectionLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.reflectionLabel1.Location = new System.Drawing.Point(3, 3);
            this.reflectionLabel1.Name = "reflectionLabel1";
            this.reflectionLabel1.Size = new System.Drawing.Size(503, 71);
            this.reflectionLabel1.TabIndex = 8;
            this.reflectionLabel1.Text = "Tidak ada komentar";
            // 
            // BKirimKomen
            // 
            this.BKirimKomen.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BKirimKomen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BKirimKomen.AntiAlias = true;
            this.BKirimKomen.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BKirimKomen.Location = new System.Drawing.Point(439, 138);
            this.BKirimKomen.Name = "BKirimKomen";
            this.BKirimKomen.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BKirimKomen.Size = new System.Drawing.Size(67, 24);
            this.BKirimKomen.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BKirimKomen.SymbolSize = 10F;
            this.BKirimKomen.TabIndex = 7;
            this.BKirimKomen.Text = "Kirim";
            this.BKirimKomen.Click += new System.EventHandler(this.BKirimKomen_Click);
            // 
            // TBKomentar
            // 
            this.TBKomentar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBKomentar.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TBKomentar.Border.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TBKomentar.Border.BackColor2 = System.Drawing.Color.White;
            this.TBKomentar.Border.Class = "TextBoxBorder";
            this.TBKomentar.Border.CornerDiameter = 4;
            this.TBKomentar.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBKomentar.ForeColor = System.Drawing.Color.Black;
            this.TBKomentar.Location = new System.Drawing.Point(3, 138);
            this.TBKomentar.Name = "TBKomentar";
            this.TBKomentar.Size = new System.Drawing.Size(430, 24);
            this.TBKomentar.TabIndex = 1;
            // 
            // Form_DetailLaporan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 554);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.GroupPanel4);
            this.DoubleBuffered = true;
            this.Name = "Form_DetailLaporan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_DetailLaporan";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_DetailLaporan_FormClosed);
            this.Load += new System.EventHandler(this.Form_DetailLaporan_Load);
            this.Shown += new System.EventHandler(this.Form_DetailLaporan_Shown);
            this.GroupPanel4.ResumeLayout(false);
            this.GroupPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBFoto)).EndInit();
            this.GroupPanel5.ResumeLayout(false);
            this.GroupPanel5.PerformLayout();
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGKomentar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.Controls.GroupPanel GroupPanel4;
        internal DevComponents.DotNetBar.Controls.GroupPanel GroupPanel5;
        internal System.Windows.Forms.Label LInformasi;
        public System.Windows.Forms.Label LWaktu;
        public System.Windows.Forms.Label LNama;
        private DevComponents.DotNetBar.Controls.TextBoxX TBStatus;
        private System.Windows.Forms.PictureBox PBFoto;
        internal DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.DataGridViewX DGKomentar;
        private DevComponents.DotNetBar.Controls.TextBoxX TBKomentar;
        internal DevComponents.DotNetBar.ButtonX BKirimKomen;
        private DevComponents.DotNetBar.LabelX LLokasi;
        private DevComponents.DotNetBar.Controls.ReflectionLabel reflectionLabel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idlaporan;
        private System.Windows.Forms.DataGridViewTextBoxColumn komentarku;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}