﻿namespace Satpam_Desktop.Form
{
    partial class Form_AturLaporanFasilitas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LPesan = new DevComponents.DotNetBar.LabelX();
            this.LabelX2 = new DevComponents.DotNetBar.LabelX();
            this.TBAlamat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.LabelX1 = new DevComponents.DotNetBar.LabelX();
            this.TBLat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TBLing = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.DTDilaporkanPada = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.LNamaPengguna = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.BRefreshTDukungan = new DevComponents.DotNetBar.ButtonX();
            this.IITotalDukungan = new DevComponents.Editors.IntegerInput();
            this.IITotalKomentar = new DevComponents.Editors.IntegerInput();
            this.BRefreshTKomentar = new DevComponents.DotNetBar.ButtonX();
            this.BPilihPengguna = new DevComponents.DotNetBar.ButtonX();
            this.BPrev = new DevComponents.DotNetBar.ButtonX();
            this.BNext = new DevComponents.DotNetBar.ButtonX();
            this.BSimpan = new DevComponents.DotNetBar.ButtonX();
            this.CBOtoClose = new DevComponents.DotNetBar.CheckBoxItem();
            this.BTutup = new DevComponents.DotNetBar.ButtonX();
            this.CBLaporanRhs = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.PBFoto = new System.Windows.Forms.PictureBox();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.BPilihFoto = new DevComponents.DotNetBar.ButtonX();
            this.CBPrioritas = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.TBPesan = new DevComponents.DotNetBar.Controls.TextBoxX();
            ((System.ComponentModel.ISupportInitialize)(this.DTDilaporkanPada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IITotalDukungan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IITotalKomentar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBFoto)).BeginInit();
            this.SuspendLayout();
            // 
            // LPesan
            // 
            this.LPesan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LPesan.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LPesan.ForeColor = System.Drawing.Color.Black;
            this.LPesan.Location = new System.Drawing.Point(7, 12);
            this.LPesan.Name = "LPesan";
            this.LPesan.Size = new System.Drawing.Size(100, 23);
            this.LPesan.TabIndex = 78;
            this.LPesan.Text = "Keluhan";
            // 
            // LabelX2
            // 
            this.LabelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX2.ForeColor = System.Drawing.Color.Black;
            this.LabelX2.Location = new System.Drawing.Point(7, 162);
            this.LabelX2.Name = "LabelX2";
            this.LabelX2.Size = new System.Drawing.Size(100, 23);
            this.LabelX2.TabIndex = 77;
            this.LabelX2.Text = "Alamat";
            // 
            // TBAlamat
            // 
            this.TBAlamat.AutoSelectAll = true;
            this.TBAlamat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBAlamat.Border.Class = "TextBoxBorder";
            this.TBAlamat.Border.CornerDiameter = 2;
            this.TBAlamat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBAlamat.ForeColor = System.Drawing.Color.Black;
            this.TBAlamat.Location = new System.Drawing.Point(107, 162);
            this.TBAlamat.MaxLength = 100;
            this.TBAlamat.Name = "TBAlamat";
            this.TBAlamat.Size = new System.Drawing.Size(206, 22);
            this.TBAlamat.TabIndex = 3;
            // 
            // LabelX1
            // 
            this.LabelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX1.ForeColor = System.Drawing.Color.Black;
            this.LabelX1.Location = new System.Drawing.Point(7, 134);
            this.LabelX1.Name = "LabelX1";
            this.LabelX1.Size = new System.Drawing.Size(100, 23);
            this.LabelX1.TabIndex = 76;
            this.LabelX1.Text = "Lokasi";
            // 
            // TBLat
            // 
            this.TBLat.AutoSelectAll = true;
            this.TBLat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBLat.Border.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBLat.Border.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBLat.Border.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBLat.Border.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBLat.Border.Class = "TextBoxBorder";
            this.TBLat.Border.CornerDiameter = 2;
            this.TBLat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBLat.ForeColor = System.Drawing.Color.Black;
            this.TBLat.Location = new System.Drawing.Point(107, 134);
            this.TBLat.MaxLength = 20;
            this.TBLat.Name = "TBLat";
            this.TBLat.Size = new System.Drawing.Size(100, 22);
            this.TBLat.TabIndex = 1;
            // 
            // TBLing
            // 
            this.TBLing.AutoSelectAll = true;
            this.TBLing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBLing.Border.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBLing.Border.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBLing.Border.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBLing.Border.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBLing.Border.Class = "TextBoxBorder";
            this.TBLing.Border.CornerDiameter = 2;
            this.TBLing.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBLing.ForeColor = System.Drawing.Color.Black;
            this.TBLing.Location = new System.Drawing.Point(213, 134);
            this.TBLing.MaxLength = 20;
            this.TBLing.Name = "TBLing";
            this.TBLing.Size = new System.Drawing.Size(100, 22);
            this.TBLing.TabIndex = 2;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(7, 190);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(100, 23);
            this.labelX4.TabIndex = 77;
            this.labelX4.Text = "Prioritas";
            // 
            // DTDilaporkanPada
            // 
            this.DTDilaporkanPada.AntiAlias = true;
            this.DTDilaporkanPada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.DTDilaporkanPada.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DTDilaporkanPada.BackgroundStyle.CornerDiameter = 2;
            this.DTDilaporkanPada.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.DTDilaporkanPada.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DTDilaporkanPada.ButtonDropDown.Visible = true;
            this.DTDilaporkanPada.CustomFormat = "dddd, dd MMM yyyy h:mm:ss tt";
            this.DTDilaporkanPada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.DTDilaporkanPada.ForeColor = System.Drawing.Color.Black;
            this.DTDilaporkanPada.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DTDilaporkanPada.IsPopupCalendarOpen = false;
            this.DTDilaporkanPada.Location = new System.Drawing.Point(431, 146);
            // 
            // 
            // 
            this.DTDilaporkanPada.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTDilaporkanPada.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTDilaporkanPada.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.DTDilaporkanPada.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DTDilaporkanPada.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTDilaporkanPada.MonthCalendar.DisplayMonth = new System.DateTime(2016, 2, 1, 0, 0, 0, 0);
            this.DTDilaporkanPada.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.DTDilaporkanPada.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DTDilaporkanPada.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTDilaporkanPada.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DTDilaporkanPada.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DTDilaporkanPada.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DTDilaporkanPada.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTDilaporkanPada.MonthCalendar.TodayButtonVisible = true;
            this.DTDilaporkanPada.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DTDilaporkanPada.Name = "DTDilaporkanPada";
            this.DTDilaporkanPada.Size = new System.Drawing.Size(206, 22);
            this.DTDilaporkanPada.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DTDilaporkanPada.TabIndex = 8;
            this.DTDilaporkanPada.Value = new System.DateTime(2016, 2, 15, 23, 41, 30, 3);
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(331, 146);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(100, 23);
            this.labelX5.TabIndex = 77;
            this.labelX5.Text = "Dilaporkan Pada";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(7, 247);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(100, 23);
            this.labelX6.TabIndex = 84;
            this.labelX6.Text = "Dilaporkan Oleh";
            // 
            // LNamaPengguna
            // 
            this.LNamaPengguna.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNamaPengguna.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNamaPengguna.ForeColor = System.Drawing.Color.Black;
            this.LNamaPengguna.Location = new System.Drawing.Point(107, 247);
            this.LNamaPengguna.Name = "LNamaPengguna";
            this.LNamaPengguna.Size = new System.Drawing.Size(136, 23);
            this.LNamaPengguna.TabIndex = 84;
            this.LNamaPengguna.Text = "Belum ditentukan";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(331, 174);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(100, 23);
            this.labelX8.TabIndex = 77;
            this.labelX8.Text = "Total Dukungan";
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(331, 202);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(100, 23);
            this.labelX10.TabIndex = 77;
            this.labelX10.Text = "Total Komentar";
            // 
            // BRefreshTDukungan
            // 
            this.BRefreshTDukungan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BRefreshTDukungan.AntiAlias = true;
            this.BRefreshTDukungan.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BRefreshTDukungan.Location = new System.Drawing.Point(619, 174);
            this.BRefreshTDukungan.Name = "BRefreshTDukungan";
            this.BRefreshTDukungan.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BRefreshTDukungan.Size = new System.Drawing.Size(18, 22);
            this.BRefreshTDukungan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BRefreshTDukungan.Symbol = "";
            this.BRefreshTDukungan.SymbolSize = 10F;
            this.BRefreshTDukungan.TabIndex = 10;
            this.BRefreshTDukungan.Click += new System.EventHandler(this.BRefreshTDukungan_Click);
            // 
            // IITotalDukungan
            // 
            this.IITotalDukungan.AllowEmptyState = false;
            this.IITotalDukungan.AntiAlias = true;
            this.IITotalDukungan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.IITotalDukungan.BackgroundStyle.Class = "DateTimeInputBackground";
            this.IITotalDukungan.BackgroundStyle.CornerDiameter = 2;
            this.IITotalDukungan.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.IITotalDukungan.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.IITotalDukungan.DisplayFormat = "0 Dukungan";
            this.IITotalDukungan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.IITotalDukungan.ForeColor = System.Drawing.Color.Black;
            this.IITotalDukungan.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            this.IITotalDukungan.Location = new System.Drawing.Point(431, 174);
            this.IITotalDukungan.MinValue = 0;
            this.IITotalDukungan.Name = "IITotalDukungan";
            this.IITotalDukungan.ShowUpDown = true;
            this.IITotalDukungan.Size = new System.Drawing.Size(182, 22);
            this.IITotalDukungan.TabIndex = 9;
            // 
            // IITotalKomentar
            // 
            this.IITotalKomentar.AllowEmptyState = false;
            this.IITotalKomentar.AntiAlias = true;
            this.IITotalKomentar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.IITotalKomentar.BackgroundStyle.Class = "DateTimeInputBackground";
            this.IITotalKomentar.BackgroundStyle.CornerDiameter = 2;
            this.IITotalKomentar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.IITotalKomentar.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.IITotalKomentar.DisplayFormat = "0 Komentar";
            this.IITotalKomentar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.IITotalKomentar.ForeColor = System.Drawing.Color.Black;
            this.IITotalKomentar.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            this.IITotalKomentar.Location = new System.Drawing.Point(431, 202);
            this.IITotalKomentar.MinValue = 0;
            this.IITotalKomentar.Name = "IITotalKomentar";
            this.IITotalKomentar.ShowUpDown = true;
            this.IITotalKomentar.Size = new System.Drawing.Size(182, 22);
            this.IITotalKomentar.TabIndex = 11;
            // 
            // BRefreshTKomentar
            // 
            this.BRefreshTKomentar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BRefreshTKomentar.AntiAlias = true;
            this.BRefreshTKomentar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BRefreshTKomentar.Location = new System.Drawing.Point(619, 202);
            this.BRefreshTKomentar.Name = "BRefreshTKomentar";
            this.BRefreshTKomentar.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BRefreshTKomentar.Size = new System.Drawing.Size(18, 22);
            this.BRefreshTKomentar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BRefreshTKomentar.Symbol = "";
            this.BRefreshTKomentar.SymbolSize = 10F;
            this.BRefreshTKomentar.TabIndex = 12;
            this.BRefreshTKomentar.Click += new System.EventHandler(this.BRefreshTKomentar_Click);
            // 
            // BPilihPengguna
            // 
            this.BPilihPengguna.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPilihPengguna.AntiAlias = true;
            this.BPilihPengguna.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BPilihPengguna.Location = new System.Drawing.Point(249, 248);
            this.BPilihPengguna.Name = "BPilihPengguna";
            this.BPilihPengguna.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BPilihPengguna.Size = new System.Drawing.Size(64, 22);
            this.BPilihPengguna.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPilihPengguna.SymbolSize = 10F;
            this.BPilihPengguna.TabIndex = 6;
            this.BPilihPengguna.Text = "Pilih";
            this.BPilihPengguna.Click += new System.EventHandler(this.BPilihPengguna_Click);
            // 
            // BPrev
            // 
            this.BPrev.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPrev.Enabled = false;
            this.BPrev.Location = new System.Drawing.Point(329, 236);
            this.BPrev.Name = "BPrev";
            this.BPrev.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BPrev.Size = new System.Drawing.Size(22, 34);
            this.BPrev.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPrev.Symbol = "";
            this.BPrev.SymbolSize = 15F;
            this.BPrev.TabIndex = 13;
            this.BPrev.Click += new System.EventHandler(this.BPrev_Click);
            // 
            // BNext
            // 
            this.BNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BNext.Enabled = false;
            this.BNext.Location = new System.Drawing.Point(615, 236);
            this.BNext.Name = "BNext";
            this.BNext.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BNext.Size = new System.Drawing.Size(22, 34);
            this.BNext.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BNext.Symbol = "";
            this.BNext.SymbolSize = 15F;
            this.BNext.TabIndex = 16;
            this.BNext.Click += new System.EventHandler(this.BNext_Click);
            // 
            // BSimpan
            // 
            this.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BSimpan.Location = new System.Drawing.Point(487, 236);
            this.BSimpan.Name = "BSimpan";
            this.BSimpan.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BSimpan.Size = new System.Drawing.Size(126, 34);
            this.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BSimpan.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.CBOtoClose});
            this.BSimpan.SubItemsExpandWidth = 18;
            this.BSimpan.TabIndex = 15;
            this.BSimpan.Text = "Tambah";
            this.BSimpan.Click += new System.EventHandler(this.BSimpan_Click);
            // 
            // CBOtoClose
            // 
            this.CBOtoClose.Checked = true;
            this.CBOtoClose.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBOtoClose.GlobalItem = false;
            this.CBOtoClose.Name = "CBOtoClose";
            this.CBOtoClose.Stretch = true;
            this.CBOtoClose.Text = "Tutup otomatis";
            // 
            // BTutup
            // 
            this.BTutup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTutup.Location = new System.Drawing.Point(353, 236);
            this.BTutup.Name = "BTutup";
            this.BTutup.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BTutup.Size = new System.Drawing.Size(126, 34);
            this.BTutup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BTutup.TabIndex = 14;
            this.BTutup.Text = "Batal";
            // 
            // CBLaporanRhs
            // 
            this.CBLaporanRhs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.CBLaporanRhs.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CBLaporanRhs.ForeColor = System.Drawing.Color.Black;
            this.CBLaporanRhs.Location = new System.Drawing.Point(107, 218);
            this.CBLaporanRhs.Name = "CBLaporanRhs";
            this.CBLaporanRhs.Size = new System.Drawing.Size(206, 23);
            this.CBLaporanRhs.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBLaporanRhs.TabIndex = 5;
            this.CBLaporanRhs.Text = "Laporan Rahasia";
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(331, 13);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(100, 23);
            this.labelX9.TabIndex = 77;
            this.labelX9.Text = "Foto";
            // 
            // PBFoto
            // 
            this.PBFoto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.PBFoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PBFoto.ForeColor = System.Drawing.Color.Black;
            this.PBFoto.Location = new System.Drawing.Point(431, 42);
            this.PBFoto.Name = "PBFoto";
            this.PBFoto.Size = new System.Drawing.Size(206, 98);
            this.PBFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PBFoto.TabIndex = 93;
            this.PBFoto.TabStop = false;
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(7, 218);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(100, 23);
            this.labelX11.TabIndex = 77;
            this.labelX11.Text = "Privasi";
            // 
            // BPilihFoto
            // 
            this.BPilihFoto.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPilihFoto.AntiAlias = true;
            this.BPilihFoto.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BPilihFoto.Location = new System.Drawing.Point(431, 13);
            this.BPilihFoto.Name = "BPilihFoto";
            this.BPilihFoto.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BPilihFoto.Size = new System.Drawing.Size(206, 22);
            this.BPilihFoto.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPilihFoto.SymbolSize = 10F;
            this.BPilihFoto.TabIndex = 7;
            this.BPilihFoto.Text = "Pilih Foto";
            this.BPilihFoto.Click += new System.EventHandler(this.BPilihFoto_Click);
            // 
            // CBPrioritas
            // 
            this.CBPrioritas.DisplayMember = "Text";
            this.CBPrioritas.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CBPrioritas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBPrioritas.ForeColor = System.Drawing.Color.Black;
            this.CBPrioritas.FormattingEnabled = true;
            this.CBPrioritas.ItemHeight = 14;
            this.CBPrioritas.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem3});
            this.CBPrioritas.Location = new System.Drawing.Point(107, 191);
            this.CBPrioritas.Name = "CBPrioritas";
            this.CBPrioritas.Size = new System.Drawing.Size(206, 20);
            this.CBPrioritas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBPrioritas.TabIndex = 4;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "Tinggi";
            this.comboItem1.Value = "Tinggi";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Sedang";
            this.comboItem2.Value = "Sedang";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "Rendah";
            this.comboItem3.Value = "Rendah";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "File Gambar | *.jpg; *.bmp; *.png";
            // 
            // TBPesan
            // 
            this.TBPesan.AutoSelectAll = true;
            this.TBPesan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBPesan.Border.Class = "TextBoxBorder";
            this.TBPesan.Border.CornerDiameter = 2;
            this.TBPesan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBPesan.ForeColor = System.Drawing.Color.Black;
            this.TBPesan.Location = new System.Drawing.Point(107, 12);
            this.TBPesan.MaxLength = 99999;
            this.TBPesan.Multiline = true;
            this.TBPesan.Name = "TBPesan";
            this.TBPesan.Size = new System.Drawing.Size(206, 116);
            this.TBPesan.TabIndex = 0;
            // 
            // Form_AturLaporanFasilitas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 277);
            this.ControlBox = false;
            this.Controls.Add(this.TBPesan);
            this.Controls.Add(this.CBPrioritas);
            this.Controls.Add(this.PBFoto);
            this.Controls.Add(this.CBLaporanRhs);
            this.Controls.Add(this.BPrev);
            this.Controls.Add(this.BNext);
            this.Controls.Add(this.BSimpan);
            this.Controls.Add(this.BTutup);
            this.Controls.Add(this.BPilihFoto);
            this.Controls.Add(this.BPilihPengguna);
            this.Controls.Add(this.BRefreshTKomentar);
            this.Controls.Add(this.BRefreshTDukungan);
            this.Controls.Add(this.IITotalKomentar);
            this.Controls.Add(this.IITotalDukungan);
            this.Controls.Add(this.LNamaPengguna);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.DTDilaporkanPada);
            this.Controls.Add(this.TBLing);
            this.Controls.Add(this.TBLat);
            this.Controls.Add(this.LPesan);
            this.Controls.Add(this.labelX9);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX10);
            this.Controls.Add(this.labelX11);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX8);
            this.Controls.Add(this.LabelX2);
            this.Controls.Add(this.TBAlamat);
            this.Controls.Add(this.LabelX1);
            this.DoubleBuffered = true;
            this.Name = "Form_AturLaporanFasilitas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_AturLaporanFasilitas";
            this.Load += new System.EventHandler(this.Form_AturLaporanFasilitas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DTDilaporkanPada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IITotalDukungan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IITotalKomentar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBFoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        internal DevComponents.DotNetBar.LabelX LPesan;
        internal DevComponents.DotNetBar.LabelX LabelX2;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBAlamat;
        internal DevComponents.DotNetBar.LabelX LabelX1;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBLat;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBLing;
        internal DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DTDilaporkanPada;
        internal DevComponents.DotNetBar.LabelX labelX5;
        internal DevComponents.DotNetBar.LabelX labelX6;
        internal DevComponents.DotNetBar.LabelX LNamaPengguna;
        internal DevComponents.DotNetBar.LabelX labelX8;
        internal DevComponents.DotNetBar.LabelX labelX10;
        internal DevComponents.DotNetBar.ButtonX BRefreshTDukungan;
        private DevComponents.Editors.IntegerInput IITotalDukungan;
        private DevComponents.Editors.IntegerInput IITotalKomentar;
        internal DevComponents.DotNetBar.ButtonX BRefreshTKomentar;
        internal DevComponents.DotNetBar.ButtonX BPilihPengguna;
        internal DevComponents.DotNetBar.ButtonX BPrev;
        internal DevComponents.DotNetBar.ButtonX BNext;
        internal DevComponents.DotNetBar.ButtonX BSimpan;
        internal DevComponents.DotNetBar.CheckBoxItem CBOtoClose;
        internal DevComponents.DotNetBar.ButtonX BTutup;
        private DevComponents.DotNetBar.Controls.CheckBoxX CBLaporanRhs;
        internal DevComponents.DotNetBar.LabelX labelX9;
        private System.Windows.Forms.PictureBox PBFoto;
        internal DevComponents.DotNetBar.LabelX labelX11;
        internal DevComponents.DotNetBar.ButtonX BPilihFoto;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CBPrioritas;
        internal DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBPesan;
    }
}