﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Satpam_Desktop.Form;
using Satpam_Desktop.Class;
using System.Collections.Specialized;
using MessageHandle;

namespace Satpam_Desktop
{
    public partial class Form_Login : DevComponents.DotNetBar.Metro.MetroForm
    {
        string requestUrl = "ambil_sebagai.php?sbid=";
        RESTService rs;

        public Form_Login()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
        }

        private void BTutup_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BLogin_Click(object sender, EventArgs e)
        {
            BLogin.Enabled = false;

            NameValueCollection formData = new NameValueCollection();
            formData["email"] = TBEmail.Text;
            formData["password"] = TBPass.Text;
            rs.MakeRequestAsync("login_pengguna.php", formData, "login");
        }

        private bool ConvertToBoolean(string nilai)
        {
            bool hasil = false;
            if (nilai == "1")
            {
                hasil = true;
            }
            return hasil;
        }

        private void RequestComplete_Event(object sender, RequestCompleteEventArgs e)
        {
            string hasil = rs.GetString(e.Response, "hasil");
            if (e.Bagian == "login")
            {
                if (hasil == "ADA")
                {
                    Program.Id = TBEmail.Text;
                    Program.Pgid = rs.GetString(e.Response, "pgid");
                    Program.Nama = rs.GetString(e.Response, "nama");
                    Program.Sebagai = rs.GetString(e.Response, "sebagai");
                    string sbid = rs.GetString(e.Response, "sbid");
                    rs.MakeRequestAsync(requestUrl + sbid, null, "ambil sebagai");
                }
                else if (hasil == "TIDAK ADA")
                {
                    BLogin.Enabled = true;
                    ToastNotification.Show(this, "Email / Kata sandi salah", null, 3000, eToastGlowColor.Orange);
                }
                else
                {
                    BLogin.Enabled = true;
                    ToastNotification.Show(this, "Tidak dapat terhubung, periksa koneksi", null, 3000, eToastGlowColor.Red);
                }
            }
            else if (e.Bagian == "ambil sebagai")
            {
                if (hasil == "SUKSES")
                {
                    Program.cpanel = ConvertToBoolean(rs.GetString(e.Response, "cpanel"));
                    Program.cpengguna = ConvertToBoolean(rs.GetString(e.Response, "cpengguna"));
                    Program.claporanfas = ConvertToBoolean(rs.GetString(e.Response, "claporanfas"));
                    Program.claporanplgr = ConvertToBoolean(rs.GetString(e.Response, "claporanplgr"));
                    Program.claporankmt = ConvertToBoolean(rs.GetString(e.Response, "claporankmt"));
                    Program.csebagai = ConvertToBoolean(rs.GetString(e.Response, "csebagai"));

                    if (Program.cpanel == false)
                    {
                        KotakPesan.Show(this, "Anda tidak memiliki hak untuk menggunakan aplikasi ini.", "Satpam Desktop", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan);
                        TBPass.Text = "";
                        TBEmail.Text = "";
                        BLogin.Enabled = true;
                        TBEmail.Focus();
                        return;
                    }

                    Form_Utama f = new Form_Utama();
                    f.Show();
                    this.Hide();
                }
                else if (hasil == "TIDAK ADA")
                {
                    BLogin.Enabled = true;
                    ToastNotification.Show(this, "Tidak dapat terhubung, periksa koneksi", null, 3000, eToastGlowColor.Red);
                }
            }
        }

        private void Form_Login_Load(object sender, EventArgs e)
        {
            rs = new RESTService();
            rs.RequestComplete += new RESTService.RequestCompleteHandler(RequestComplete_Event);
        }

        private void Form_Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
