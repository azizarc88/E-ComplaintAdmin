﻿namespace Satpam_Desktop.Form
{
    partial class Form_TampilPendukung
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LBPendukung = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // LBPendukung
            // 
            this.LBPendukung.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LBPendukung.BackColor = System.Drawing.Color.White;
            this.LBPendukung.FormattingEnabled = true;
            this.LBPendukung.Location = new System.Drawing.Point(12, 12);
            this.LBPendukung.Name = "LBPendukung";
            this.LBPendukung.Size = new System.Drawing.Size(252, 342);
            this.LBPendukung.TabIndex = 0;
            // 
            // Form_TampilPendukung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(276, 375);
            this.Controls.Add(this.LBPendukung);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.Name = "Form_TampilPendukung";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pendukung";
            this.Load += new System.EventHandler(this.Form_TampilPendukung_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox LBPendukung;
    }
}