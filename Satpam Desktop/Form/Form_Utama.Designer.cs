﻿namespace Satpam_Desktop.Form
{
    partial class Form_Utama
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.LStatusKoneksi = new DevComponents.DotNetBar.LabelX();
            this.LNamaP = new DevComponents.DotNetBar.LabelItem();
            this.LId = new DevComponents.DotNetBar.LabelItem();
            this.LSebagai = new DevComponents.DotNetBar.LabelItem();
            this.LWaktuSekarang = new DevComponents.DotNetBar.LabelItem();
            this.controlContainerItem1 = new DevComponents.DotNetBar.ControlContainerItem();
            this.tabControl1 = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.BLFasDilaporkan = new DevComponents.DotNetBar.ButtonX();
            this.DGLaporanFasilitas = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.lfid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.foto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.keluhan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ling = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alamat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prioritas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dilaporkanpada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dilaporkanoleh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laporanrahasia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dukungan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.omentar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.TSSLBanyakDataStruktur = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLNimStruktur = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSStruktur = new System.Windows.Forms.ToolStripStatusLabel();
            this.RMStruktur = new DevComponents.DotNetBar.RadialMenu();
            this.RMITambahLFas = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIUbahLFas = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIHapusLFas = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIOlahLFas = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIDetailLFas = new DevComponents.DotNetBar.RadialMenuItem();
            this.TBILFasilitas = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.BLPlgrDilaporkan = new DevComponents.DotNetBar.ButtonX();
            this.DGLaporanPelanggaran = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip3 = new System.Windows.Forms.StatusStrip();
            this.TSSLBanyakDataPiket = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel7 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLNimPiket = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSPiket = new System.Windows.Forms.ToolStripStatusLabel();
            this.RMPiket = new DevComponents.DotNetBar.RadialMenu();
            this.RMITambahPlgr = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIUbahPlgr = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIHapusPlgr = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIOlahPlgr = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIDetailLPlgr = new DevComponents.DotNetBar.RadialMenuItem();
            this.TBILPelanggaran = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.RMPengguna = new DevComponents.DotNetBar.RadialMenu();
            this.RMITambahPengguna = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIUbahPengguna = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIHapusPengguna = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIOlahPengguna = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIBlokirPengguna = new DevComponents.DotNetBar.RadialMenuItem();
            this.radialMenuItem1 = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIUBlokirPengguna = new DevComponents.DotNetBar.RadialMenuItem();
            this.DGPengguna = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.NimDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NamaanggotaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KelasDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlamatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JkelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NohpDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tanggallahir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totallaporan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nohp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.password = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.blokir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.TSSLBanyakDataAnggota = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLNimAnggota = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSAnggota = new System.Windows.Forms.ToolStripStatusLabel();
            this.TBIPengguna = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel4 = new DevComponents.DotNetBar.TabControlPanel();
            this.statusStrip4 = new System.Windows.Forms.StatusStrip();
            this.TSSLBanyakDataRapat = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel11 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLNamaRapat = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSRapat = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.DGLKomentarFas = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.kdid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pgid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lpid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.waktu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.komentar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DGLKomentarPlgr = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.TBILaporanKomentar = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel7 = new DevComponents.DotNetBar.TabControlPanel();
            this.DGSebagai = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.sbid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sebagai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cpanel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cpengguna = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.claporanfas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.claporanplgr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.claporankmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.csebagai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip7 = new System.Windows.Forms.StatusStrip();
            this.TSSLTotalDana = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel23 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLIdKeuangan = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSKeuangan = new System.Windows.Forms.ToolStripStatusLabel();
            this.RMKeuangan = new DevComponents.DotNetBar.RadialMenu();
            this.RMITambahSebagai = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIUbahSebagai = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIHapusSebagai = new DevComponents.DotNetBar.RadialMenuItem();
            this.TBIKeuangan = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel5 = new DevComponents.DotNetBar.TabControlPanel();
            this.DGObrolan = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namadari = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip5 = new System.Windows.Forms.StatusStrip();
            this.TSSLBanyakDataProker = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel15 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLIdProker = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSProker = new System.Windows.Forms.ToolStripStatusLabel();
            this.TBIProker = new DevComponents.DotNetBar.TabItem(this.components);
            this.TBIInventaris = new DevComponents.DotNetBar.TabItem(this.components);
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.LTotalAngg = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.LTotalLPlgrDil = new DevComponents.DotNetBar.LabelX();
            this.LTotalLFasDil = new DevComponents.DotNetBar.LabelX();
            this.LTotalLPlgr = new DevComponents.DotNetBar.LabelX();
            this.LTotalLFas = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.circularProgress1 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.PBFas2 = new System.Windows.Forms.PictureBox();
            this.PBFas1 = new System.Windows.Forms.PictureBox();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.ReflectionLabel1 = new DevComponents.DotNetBar.Controls.ReflectionLabel();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.circularProgress2 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.PBPlgr2 = new System.Windows.Forms.PictureBox();
            this.PBPlgr1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TSlideShowFas = new System.Windows.Forms.Timer(this.components);
            this.TSlideShowPlgr = new System.Windows.Forms.Timer(this.components);
            this.RMIMenuUtama = new DevComponents.DotNetBar.RadialMenu();
            this.RMITentangAplikasi = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIFacebook = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIBantuan = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMITwitter = new DevComponents.DotNetBar.RadialMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.metroStatusBar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGLaporanFasilitas)).BeginInit();
            this.statusStrip2.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGLaporanPelanggaran)).BeginInit();
            this.statusStrip3.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGPengguna)).BeginInit();
            this.StatusStrip1.SuspendLayout();
            this.tabControlPanel4.SuspendLayout();
            this.statusStrip4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGLKomentarFas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGLKomentarPlgr)).BeginInit();
            this.tabControlPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGSebagai)).BeginInit();
            this.statusStrip7.SuspendLayout();
            this.tabControlPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGObrolan)).BeginInit();
            this.statusStrip5.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBFas2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBFas1)).BeginInit();
            this.groupPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBPlgr2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBPlgr1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Controls.Add(this.LStatusKoneksi);
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.LNamaP,
            this.LId,
            this.LSebagai,
            this.LWaktuSekarang,
            this.controlContainerItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 544);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(905, 22);
            this.metroStatusBar1.TabIndex = 0;
            this.metroStatusBar1.Text = "metroStatusBar1";
            // 
            // LStatusKoneksi
            // 
            this.LStatusKoneksi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LStatusKoneksi.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.LStatusKoneksi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LStatusKoneksi.BackgroundStyle.TextColor = System.Drawing.Color.White;
            this.LStatusKoneksi.ForeColor = System.Drawing.Color.Black;
            this.LStatusKoneksi.Location = new System.Drawing.Point(878, 4);
            this.LStatusKoneksi.Name = "LStatusKoneksi";
            this.LStatusKoneksi.Size = new System.Drawing.Size(14, 15);
            this.LStatusKoneksi.Symbol = "";
            this.LStatusKoneksi.SymbolColor = System.Drawing.Color.White;
            this.LStatusKoneksi.SymbolSize = 10F;
            this.LStatusKoneksi.TabIndex = 15;
            // 
            // LNamaP
            // 
            this.LNamaP.ImageTextSpacing = 5;
            this.LNamaP.Name = "LNamaP";
            this.LNamaP.Stretch = true;
            this.LNamaP.Symbol = "";
            this.LNamaP.SymbolSize = 13F;
            this.LNamaP.Text = "Nama_Pengguna";
            this.LNamaP.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // LId
            // 
            this.LId.ImageTextSpacing = 5;
            this.LId.Name = "LId";
            this.LId.Stretch = true;
            this.LId.Symbol = "";
            this.LId.SymbolSize = 13F;
            this.LId.Text = "email@email.com";
            this.LId.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // LSebagai
            // 
            this.LSebagai.ImageTextSpacing = 5;
            this.LSebagai.Name = "LSebagai";
            this.LSebagai.Stretch = true;
            this.LSebagai.Symbol = "";
            this.LSebagai.SymbolSize = 13F;
            this.LSebagai.Text = "Jabatan";
            this.LSebagai.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // LWaktuSekarang
            // 
            this.LWaktuSekarang.ImageTextSpacing = 5;
            this.LWaktuSekarang.Name = "LWaktuSekarang";
            this.LWaktuSekarang.Stretch = true;
            this.LWaktuSekarang.Symbol = "";
            this.LWaktuSekarang.SymbolSize = 13F;
            this.LWaktuSekarang.Text = "Hari, 00 Bulan 0000";
            this.LWaktuSekarang.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // controlContainerItem1
            // 
            this.controlContainerItem1.AllowItemResize = false;
            this.controlContainerItem1.Control = this.LStatusKoneksi;
            this.controlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem1.Name = "controlContainerItem1";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.AntiAlias = true;
            this.tabControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControl1.CanReorderTabs = true;
            this.tabControl1.Controls.Add(this.tabControlPanel1);
            this.tabControl1.Controls.Add(this.tabControlPanel2);
            this.tabControl1.Controls.Add(this.tabControlPanel3);
            this.tabControl1.Controls.Add(this.tabControlPanel4);
            this.tabControl1.Controls.Add(this.tabControlPanel7);
            this.tabControl1.Controls.Add(this.tabControlPanel5);
            this.tabControl1.FixedTabSize = new System.Drawing.Size(140, 0);
            this.tabControl1.ForeColor = System.Drawing.Color.Black;
            this.tabControl1.Location = new System.Drawing.Point(12, 190);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabControl1.SelectedTabIndex = 0;
            this.tabControl1.ShowFocusRectangle = false;
            this.tabControl1.Size = new System.Drawing.Size(881, 348);
            this.tabControl1.Style = DevComponents.DotNetBar.eTabStripStyle.Metro;
            this.tabControl1.TabIndex = 0;
            this.tabControl1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabControl1.Tabs.Add(this.TBIPengguna);
            this.tabControl1.Tabs.Add(this.TBILFasilitas);
            this.tabControl1.Tabs.Add(this.TBILPelanggaran);
            this.tabControl1.Tabs.Add(this.TBILaporanKomentar);
            this.tabControl1.Tabs.Add(this.TBIProker);
            this.tabControl1.Tabs.Add(this.TBIKeuangan);
            this.tabControl1.Text = "tabControl1";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.BLFasDilaporkan);
            this.tabControlPanel2.Controls.Add(this.DGLaporanFasilitas);
            this.tabControlPanel2.Controls.Add(this.statusStrip2);
            this.tabControlPanel2.Controls.Add(this.RMStruktur);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(881, 321);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 0;
            this.tabControlPanel2.TabItem = this.TBILFasilitas;
            // 
            // BLFasDilaporkan
            // 
            this.BLFasDilaporkan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BLFasDilaporkan.AntiAlias = true;
            this.BLFasDilaporkan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.BLFasDilaporkan.EnableMarkup = false;
            this.BLFasDilaporkan.Location = new System.Drawing.Point(78, 11);
            this.BLFasDilaporkan.Name = "BLFasDilaporkan";
            this.BLFasDilaporkan.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.BLFasDilaporkan.Size = new System.Drawing.Size(28, 28);
            this.BLFasDilaporkan.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.BLFasDilaporkan.Symbol = "";
            this.BLFasDilaporkan.SymbolSize = 14F;
            this.BLFasDilaporkan.TabIndex = 2;
            this.BLFasDilaporkan.TextColor = System.Drawing.Color.AliceBlue;
            this.BLFasDilaporkan.Tooltip = "Lihat laporan fasilitas yang dilaporkan";
            this.BLFasDilaporkan.Click += new System.EventHandler(this.BLFasDilaporkan_Click);
            // 
            // DGLaporanFasilitas
            // 
            this.DGLaporanFasilitas.AllowUserToAddRows = false;
            this.DGLaporanFasilitas.AllowUserToDeleteRows = false;
            this.DGLaporanFasilitas.AllowUserToResizeRows = false;
            this.DGLaporanFasilitas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGLaporanFasilitas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGLaporanFasilitas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGLaporanFasilitas.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGLaporanFasilitas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.DGLaporanFasilitas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGLaporanFasilitas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.lfid,
            this.foto,
            this.keluhan,
            this.lat,
            this.ling,
            this.alamat,
            this.prioritas,
            this.dilaporkanpada,
            this.dilaporkanoleh,
            this.laporanrahasia,
            this.dukungan,
            this.omentar});
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGLaporanFasilitas.DefaultCellStyle = dataGridViewCellStyle23;
            this.DGLaporanFasilitas.EnableHeadersVisualStyles = false;
            this.DGLaporanFasilitas.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGLaporanFasilitas.Location = new System.Drawing.Point(0, 48);
            this.DGLaporanFasilitas.Name = "DGLaporanFasilitas";
            this.DGLaporanFasilitas.ReadOnly = true;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGLaporanFasilitas.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.DGLaporanFasilitas.RowHeadersVisible = false;
            this.DGLaporanFasilitas.RowHeadersWidth = 100;
            this.DGLaporanFasilitas.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGLaporanFasilitas.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.DGLaporanFasilitas.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.DGLaporanFasilitas.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.DGLaporanFasilitas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGLaporanFasilitas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGLaporanFasilitas.Size = new System.Drawing.Size(881, 249);
            this.DGLaporanFasilitas.TabIndex = 1;
            this.DGLaporanFasilitas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGLaporanFasilitas_CellClick);
            this.DGLaporanFasilitas.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGLaporanFasilitas_CellDoubleClick);
            this.DGLaporanFasilitas.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DGLaporanFasilitas_RowsAdded);
            // 
            // lfid
            // 
            this.lfid.HeaderText = "LFID";
            this.lfid.Name = "lfid";
            this.lfid.ReadOnly = true;
            this.lfid.Visible = false;
            // 
            // foto
            // 
            this.foto.FillWeight = 70F;
            this.foto.HeaderText = "URL Foto";
            this.foto.Name = "foto";
            this.foto.ReadOnly = true;
            this.foto.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // keluhan
            // 
            this.keluhan.HeaderText = "Keluhan";
            this.keluhan.Name = "keluhan";
            this.keluhan.ReadOnly = true;
            // 
            // lat
            // 
            this.lat.FillWeight = 50F;
            this.lat.HeaderText = "Latitude";
            this.lat.Name = "lat";
            this.lat.ReadOnly = true;
            // 
            // ling
            // 
            this.ling.FillWeight = 50F;
            this.ling.HeaderText = "Longitude";
            this.ling.Name = "ling";
            this.ling.ReadOnly = true;
            // 
            // alamat
            // 
            this.alamat.FillWeight = 80F;
            this.alamat.HeaderText = "Alamat";
            this.alamat.Name = "alamat";
            this.alamat.ReadOnly = true;
            // 
            // prioritas
            // 
            this.prioritas.FillWeight = 40F;
            this.prioritas.HeaderText = "Prioritas";
            this.prioritas.Name = "prioritas";
            this.prioritas.ReadOnly = true;
            // 
            // dilaporkanpada
            // 
            this.dilaporkanpada.FillWeight = 70F;
            this.dilaporkanpada.HeaderText = "Waktu";
            this.dilaporkanpada.Name = "dilaporkanpada";
            this.dilaporkanpada.ReadOnly = true;
            this.dilaporkanpada.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dilaporkanoleh
            // 
            this.dilaporkanoleh.FillWeight = 25F;
            this.dilaporkanoleh.HeaderText = "Oleh";
            this.dilaporkanoleh.Name = "dilaporkanoleh";
            this.dilaporkanoleh.ReadOnly = true;
            // 
            // laporanrahasia
            // 
            this.laporanrahasia.FillWeight = 30F;
            this.laporanrahasia.HeaderText = "Rahasia";
            this.laporanrahasia.Name = "laporanrahasia";
            this.laporanrahasia.ReadOnly = true;
            // 
            // dukungan
            // 
            this.dukungan.FillWeight = 35F;
            this.dukungan.HeaderText = "Dukungan";
            this.dukungan.Name = "dukungan";
            this.dukungan.ReadOnly = true;
            // 
            // omentar
            // 
            this.omentar.FillWeight = 35F;
            this.omentar.HeaderText = "Komentar";
            this.omentar.Name = "omentar";
            this.omentar.ReadOnly = true;
            // 
            // statusStrip2
            // 
            this.statusStrip2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.statusStrip2.ForeColor = System.Drawing.Color.Black;
            this.statusStrip2.Location = new System.Drawing.Point(1, 298);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip2.Size = new System.Drawing.Size(879, 22);
            this.statusStrip2.SizingGrip = false;
            this.statusStrip2.TabIndex = 0;
            this.statusStrip2.Text = "SSAnggota";
            // 
            // TSSLBanyakDataStruktur
            // 
            this.TSSLBanyakDataStruktur.Name = "TSSLBanyakDataStruktur";
            this.TSSLBanyakDataStruktur.Size = new System.Drawing.Size(14, 17);
            this.TSSLBanyakDataStruktur.Text = "0";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel3.Text = " | ";
            // 
            // TSSLNimStruktur
            // 
            this.TSSLNimStruktur.Name = "TSSLNimStruktur";
            this.TSSLNimStruktur.Size = new System.Drawing.Size(10, 17);
            this.TSSLNimStruktur.Text = " ";
            // 
            // LDSStruktur
            // 
            this.LDSStruktur.Name = "LDSStruktur";
            this.LDSStruktur.Size = new System.Drawing.Size(821, 17);
            this.LDSStruktur.Spring = true;
            // 
            // RMStruktur
            // 
            this.RMStruktur.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.RMStruktur.ForeColor = System.Drawing.Color.Black;
            this.RMStruktur.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMITambahLFas,
            this.RMIUbahLFas,
            this.RMIHapusLFas,
            this.RMIOlahLFas});
            this.RMStruktur.Location = new System.Drawing.Point(44, 11);
            this.RMStruktur.Name = "RMStruktur";
            this.RMStruktur.Size = new System.Drawing.Size(28, 28);
            this.RMStruktur.Symbol = "";
            this.RMStruktur.SymbolSize = 14F;
            this.RMStruktur.TabIndex = 0;
            this.RMStruktur.Text = "RadialMenu2";
            this.RMStruktur.MouseEnter += new System.EventHandler(this.RM_MouseEnter);
            this.RMStruktur.MouseLeave += new System.EventHandler(this.RM_MouseLeave);
            // 
            // RMITambahLFas
            // 
            this.RMITambahLFas.Name = "RMITambahLFas";
            this.RMITambahLFas.Symbol = "";
            this.RMITambahLFas.Text = "Tambah";
            this.RMITambahLFas.Click += new System.EventHandler(this.RMITambahLFas_Klik);
            // 
            // RMIUbahLFas
            // 
            this.RMIUbahLFas.Name = "RMIUbahLFas";
            this.RMIUbahLFas.Symbol = "";
            this.RMIUbahLFas.Text = "Ubah";
            this.RMIUbahLFas.Click += new System.EventHandler(this.RMIUbahLFas_Klik);
            // 
            // RMIHapusLFas
            // 
            this.RMIHapusLFas.Name = "RMIHapusLFas";
            this.RMIHapusLFas.Symbol = "";
            this.RMIHapusLFas.Text = "Hapus";
            this.RMIHapusLFas.Click += new System.EventHandler(this.RMIHapusLFas_Klik);
            // 
            // RMIOlahLFas
            // 
            this.RMIOlahLFas.Name = "RMIOlahLFas";
            this.RMIOlahLFas.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMIDetailLFas});
            this.RMIOlahLFas.Symbol = "";
            this.RMIOlahLFas.Text = "Olah";
            // 
            // RMIDetailLFas
            // 
            this.RMIDetailLFas.Name = "RMIDetailLFas";
            this.RMIDetailLFas.Symbol = "";
            this.RMIDetailLFas.Text = "Detail";
            this.RMIDetailLFas.Click += new System.EventHandler(this.RMIDetailLFas_Klik);
            // 
            // TBILFasilitas
            // 
            this.TBILFasilitas.AttachedControl = this.tabControlPanel2;
            this.TBILFasilitas.Name = "TBILFasilitas";
            this.TBILFasilitas.Text = "Laporan Fasilitas";
            this.TBILFasilitas.Click += new System.EventHandler(this.TBIPengguna_Click);
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.Controls.Add(this.BLPlgrDilaporkan);
            this.tabControlPanel3.Controls.Add(this.DGLaporanPelanggaran);
            this.tabControlPanel3.Controls.Add(this.statusStrip3);
            this.tabControlPanel3.Controls.Add(this.RMPiket);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(881, 321);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 0;
            this.tabControlPanel3.TabItem = this.TBILPelanggaran;
            // 
            // BLPlgrDilaporkan
            // 
            this.BLPlgrDilaporkan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BLPlgrDilaporkan.AntiAlias = true;
            this.BLPlgrDilaporkan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.BLPlgrDilaporkan.EnableMarkup = false;
            this.BLPlgrDilaporkan.Location = new System.Drawing.Point(78, 11);
            this.BLPlgrDilaporkan.Name = "BLPlgrDilaporkan";
            this.BLPlgrDilaporkan.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.BLPlgrDilaporkan.Size = new System.Drawing.Size(28, 28);
            this.BLPlgrDilaporkan.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.BLPlgrDilaporkan.Symbol = "";
            this.BLPlgrDilaporkan.SymbolSize = 14F;
            this.BLPlgrDilaporkan.TabIndex = 3;
            this.BLPlgrDilaporkan.TextColor = System.Drawing.Color.AliceBlue;
            this.BLPlgrDilaporkan.Tooltip = "Lihat laporan pelanggaran yang dilaporkan";
            this.BLPlgrDilaporkan.Click += new System.EventHandler(this.BLPlgrDilaporkan_Click);
            // 
            // DGLaporanPelanggaran
            // 
            this.DGLaporanPelanggaran.AllowUserToAddRows = false;
            this.DGLaporanPelanggaran.AllowUserToDeleteRows = false;
            this.DGLaporanPelanggaran.AllowUserToResizeRows = false;
            this.DGLaporanPelanggaran.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGLaporanPelanggaran.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGLaporanPelanggaran.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGLaporanPelanggaran.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGLaporanPelanggaran.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.DGLaporanPelanggaran.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGLaporanPelanggaran.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12});
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGLaporanPelanggaran.DefaultCellStyle = dataGridViewCellStyle26;
            this.DGLaporanPelanggaran.EnableHeadersVisualStyles = false;
            this.DGLaporanPelanggaran.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGLaporanPelanggaran.Location = new System.Drawing.Point(0, 48);
            this.DGLaporanPelanggaran.Name = "DGLaporanPelanggaran";
            this.DGLaporanPelanggaran.ReadOnly = true;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGLaporanPelanggaran.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.DGLaporanPelanggaran.RowHeadersVisible = false;
            this.DGLaporanPelanggaran.RowHeadersWidth = 100;
            this.DGLaporanPelanggaran.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGLaporanPelanggaran.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.DGLaporanPelanggaran.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.DGLaporanPelanggaran.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.DGLaporanPelanggaran.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGLaporanPelanggaran.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGLaporanPelanggaran.Size = new System.Drawing.Size(881, 249);
            this.DGLaporanPelanggaran.TabIndex = 2;
            this.DGLaporanPelanggaran.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGLaporanPelanggaran_CellClick);
            this.DGLaporanPelanggaran.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGLaporanPelanggaran_CellDoubleClick);
            this.DGLaporanPelanggaran.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DGLaporanPelanggaran_RowsAdded);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "LFID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.FillWeight = 70F;
            this.dataGridViewTextBoxColumn2.HeaderText = "URL Foto";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Pelanggaran";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.FillWeight = 50F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Latitude";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.FillWeight = 50F;
            this.dataGridViewTextBoxColumn5.HeaderText = "Longitude";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.FillWeight = 80F;
            this.dataGridViewTextBoxColumn6.HeaderText = "Alamat";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.FillWeight = 40F;
            this.dataGridViewTextBoxColumn7.HeaderText = "Prioritas";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.FillWeight = 70F;
            this.dataGridViewTextBoxColumn8.HeaderText = "Waktu";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.FillWeight = 25F;
            this.dataGridViewTextBoxColumn9.HeaderText = "Oleh";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.FillWeight = 30F;
            this.dataGridViewTextBoxColumn10.HeaderText = "Rahasia";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.FillWeight = 35F;
            this.dataGridViewTextBoxColumn11.HeaderText = "Dukungan";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.FillWeight = 35F;
            this.dataGridViewTextBoxColumn12.HeaderText = "Komentar";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // statusStrip3
            // 
            this.statusStrip3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.statusStrip3.ForeColor = System.Drawing.Color.Black;
            this.statusStrip3.Location = new System.Drawing.Point(1, 298);
            this.statusStrip3.Name = "statusStrip3";
            this.statusStrip3.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip3.Size = new System.Drawing.Size(879, 22);
            this.statusStrip3.SizingGrip = false;
            this.statusStrip3.TabIndex = 0;
            this.statusStrip3.Text = "SSAnggota";
            // 
            // TSSLBanyakDataPiket
            // 
            this.TSSLBanyakDataPiket.Name = "TSSLBanyakDataPiket";
            this.TSSLBanyakDataPiket.Size = new System.Drawing.Size(14, 17);
            this.TSSLBanyakDataPiket.Text = "0";
            // 
            // toolStripStatusLabel7
            // 
            this.toolStripStatusLabel7.Name = "toolStripStatusLabel7";
            this.toolStripStatusLabel7.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel7.Text = " | ";
            // 
            // TSSLNimPiket
            // 
            this.TSSLNimPiket.Name = "TSSLNimPiket";
            this.TSSLNimPiket.Size = new System.Drawing.Size(10, 17);
            this.TSSLNimPiket.Text = " ";
            // 
            // LDSPiket
            // 
            this.LDSPiket.Name = "LDSPiket";
            this.LDSPiket.Size = new System.Drawing.Size(821, 17);
            this.LDSPiket.Spring = true;
            // 
            // RMPiket
            // 
            this.RMPiket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.RMPiket.ForeColor = System.Drawing.Color.Black;
            this.RMPiket.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMITambahPlgr,
            this.RMIUbahPlgr,
            this.RMIHapusPlgr,
            this.RMIOlahPlgr});
            this.RMPiket.Location = new System.Drawing.Point(44, 11);
            this.RMPiket.Name = "RMPiket";
            this.RMPiket.Size = new System.Drawing.Size(28, 28);
            this.RMPiket.Symbol = "";
            this.RMPiket.SymbolSize = 14F;
            this.RMPiket.TabIndex = 0;
            this.RMPiket.Text = "RadialMenu2";
            this.RMPiket.MouseEnter += new System.EventHandler(this.RM_MouseEnter);
            this.RMPiket.MouseLeave += new System.EventHandler(this.RM_MouseLeave);
            // 
            // RMITambahPlgr
            // 
            this.RMITambahPlgr.Name = "RMITambahPlgr";
            this.RMITambahPlgr.Symbol = "";
            this.RMITambahPlgr.Text = "Tambah";
            this.RMITambahPlgr.Click += new System.EventHandler(this.RMITambahPlgr_Klik);
            // 
            // RMIUbahPlgr
            // 
            this.RMIUbahPlgr.Name = "RMIUbahPlgr";
            this.RMIUbahPlgr.Symbol = "";
            this.RMIUbahPlgr.Text = "Ubah";
            this.RMIUbahPlgr.Click += new System.EventHandler(this.RMIUbahPlgr_Klik);
            // 
            // RMIHapusPlgr
            // 
            this.RMIHapusPlgr.Name = "RMIHapusPlgr";
            this.RMIHapusPlgr.Symbol = "";
            this.RMIHapusPlgr.Text = "Hapus";
            this.RMIHapusPlgr.Click += new System.EventHandler(this.RMIHapusPlgr_Klik);
            // 
            // RMIOlahPlgr
            // 
            this.RMIOlahPlgr.Name = "RMIOlahPlgr";
            this.RMIOlahPlgr.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMIDetailLPlgr});
            this.RMIOlahPlgr.Symbol = "";
            this.RMIOlahPlgr.Text = "Olah";
            // 
            // RMIDetailLPlgr
            // 
            this.RMIDetailLPlgr.Name = "RMIDetailLPlgr";
            this.RMIDetailLPlgr.Symbol = "";
            this.RMIDetailLPlgr.Text = "Detail";
            this.RMIDetailLPlgr.Click += new System.EventHandler(this.RMIDetailLPlgr_Klik);
            // 
            // TBILPelanggaran
            // 
            this.TBILPelanggaran.AttachedControl = this.tabControlPanel3;
            this.TBILPelanggaran.Name = "TBILPelanggaran";
            this.TBILPelanggaran.Text = "Laporan Pelanggaran";
            this.TBILPelanggaran.Click += new System.EventHandler(this.TBIPengguna_Click);
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.RMPengguna);
            this.tabControlPanel1.Controls.Add(this.DGPengguna);
            this.tabControlPanel1.Controls.Add(this.StatusStrip1);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(881, 321);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 0;
            this.tabControlPanel1.TabItem = this.TBIPengguna;
            // 
            // RMPengguna
            // 
            this.RMPengguna.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.RMPengguna.ForeColor = System.Drawing.Color.Black;
            this.RMPengguna.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMITambahPengguna,
            this.RMIUbahPengguna,
            this.RMIHapusPengguna,
            this.RMIOlahPengguna});
            this.RMPengguna.Location = new System.Drawing.Point(44, 11);
            this.RMPengguna.Name = "RMPengguna";
            this.RMPengguna.Size = new System.Drawing.Size(28, 28);
            this.RMPengguna.Symbol = "";
            this.RMPengguna.SymbolSize = 14F;
            this.RMPengguna.TabIndex = 0;
            this.RMPengguna.Text = "RadialMenu2";
            this.RMPengguna.MouseEnter += new System.EventHandler(this.RM_MouseEnter);
            this.RMPengguna.MouseLeave += new System.EventHandler(this.RM_MouseLeave);
            // 
            // RMITambahPengguna
            // 
            this.RMITambahPengguna.Name = "RMITambahPengguna";
            this.RMITambahPengguna.Symbol = "";
            this.RMITambahPengguna.Text = "Tambah";
            this.RMITambahPengguna.Click += new System.EventHandler(this.RMITambahPengguna_Klik);
            // 
            // RMIUbahPengguna
            // 
            this.RMIUbahPengguna.Name = "RMIUbahPengguna";
            this.RMIUbahPengguna.Symbol = "";
            this.RMIUbahPengguna.Text = "Ubah";
            this.RMIUbahPengguna.Click += new System.EventHandler(this.RMIUbahPengguna_Klik);
            // 
            // RMIHapusPengguna
            // 
            this.RMIHapusPengguna.Name = "RMIHapusPengguna";
            this.RMIHapusPengguna.Symbol = "";
            this.RMIHapusPengguna.Text = "Hapus";
            this.RMIHapusPengguna.Click += new System.EventHandler(this.RMIHapusPengguna_Klik);
            // 
            // RMIOlahPengguna
            // 
            this.RMIOlahPengguna.AutoCollapseOnClick = false;
            this.RMIOlahPengguna.Name = "RMIOlahPengguna";
            this.RMIOlahPengguna.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMIBlokirPengguna,
            this.radialMenuItem1,
            this.RMIUBlokirPengguna});
            this.RMIOlahPengguna.Symbol = "";
            this.RMIOlahPengguna.Text = "Olah";
            // 
            // RMIBlokirPengguna
            // 
            this.RMIBlokirPengguna.Name = "RMIBlokirPengguna";
            this.RMIBlokirPengguna.Symbol = "";
            this.RMIBlokirPengguna.Text = "Blokir";
            this.RMIBlokirPengguna.Click += new System.EventHandler(this.RMIBlokirPengguna_Klik);
            // 
            // radialMenuItem1
            // 
            this.radialMenuItem1.AutoCollapseOnClick = false;
            this.radialMenuItem1.Name = "radialMenuItem1";
            // 
            // RMIUBlokirPengguna
            // 
            this.RMIUBlokirPengguna.Name = "RMIUBlokirPengguna";
            this.RMIUBlokirPengguna.Symbol = "";
            this.RMIUBlokirPengguna.Text = "UnBlokir";
            this.RMIUBlokirPengguna.Click += new System.EventHandler(this.RMIUBlokirPengguna_Klik);
            // 
            // DGPengguna
            // 
            this.DGPengguna.AllowUserToAddRows = false;
            this.DGPengguna.AllowUserToDeleteRows = false;
            this.DGPengguna.AllowUserToResizeRows = false;
            this.DGPengguna.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGPengguna.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGPengguna.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGPengguna.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGPengguna.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.DGPengguna.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGPengguna.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NimDataGridViewTextBoxColumn,
            this.NamaanggotaDataGridViewTextBoxColumn,
            this.KelasDataGridViewTextBoxColumn,
            this.AlamatDataGridViewTextBoxColumn,
            this.JkelDataGridViewTextBoxColumn,
            this.NohpDataGridViewTextBoxColumn,
            this.tanggallahir,
            this.totallaporan,
            this.nohp,
            this.email,
            this.password,
            this.blokir});
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGPengguna.DefaultCellStyle = dataGridViewCellStyle29;
            this.DGPengguna.EnableHeadersVisualStyles = false;
            this.DGPengguna.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGPengguna.Location = new System.Drawing.Point(0, 48);
            this.DGPengguna.Name = "DGPengguna";
            this.DGPengguna.ReadOnly = true;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGPengguna.RowHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.DGPengguna.RowHeadersVisible = false;
            this.DGPengguna.RowHeadersWidth = 80;
            this.DGPengguna.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGPengguna.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.DGPengguna.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.DGPengguna.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.DGPengguna.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGPengguna.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGPengguna.Size = new System.Drawing.Size(881, 249);
            this.DGPengguna.TabIndex = 0;
            this.DGPengguna.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGPengguna_CellClick);
            this.DGPengguna.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DGPengguna_RowsAdded);
            // 
            // NimDataGridViewTextBoxColumn
            // 
            this.NimDataGridViewTextBoxColumn.DataPropertyName = "pgid";
            this.NimDataGridViewTextBoxColumn.FillWeight = 40F;
            this.NimDataGridViewTextBoxColumn.HeaderText = "Pgid";
            this.NimDataGridViewTextBoxColumn.Name = "NimDataGridViewTextBoxColumn";
            this.NimDataGridViewTextBoxColumn.ReadOnly = true;
            this.NimDataGridViewTextBoxColumn.Visible = false;
            // 
            // NamaanggotaDataGridViewTextBoxColumn
            // 
            this.NamaanggotaDataGridViewTextBoxColumn.DataPropertyName = "nama_pengguna";
            this.NamaanggotaDataGridViewTextBoxColumn.HeaderText = "Nama Pengguna";
            this.NamaanggotaDataGridViewTextBoxColumn.Name = "NamaanggotaDataGridViewTextBoxColumn";
            this.NamaanggotaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // KelasDataGridViewTextBoxColumn
            // 
            this.KelasDataGridViewTextBoxColumn.DataPropertyName = "sebagai";
            this.KelasDataGridViewTextBoxColumn.FillWeight = 60F;
            this.KelasDataGridViewTextBoxColumn.HeaderText = "Sebagai";
            this.KelasDataGridViewTextBoxColumn.Name = "KelasDataGridViewTextBoxColumn";
            this.KelasDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // AlamatDataGridViewTextBoxColumn
            // 
            this.AlamatDataGridViewTextBoxColumn.DataPropertyName = "alamat";
            this.AlamatDataGridViewTextBoxColumn.HeaderText = "Alamat";
            this.AlamatDataGridViewTextBoxColumn.Name = "AlamatDataGridViewTextBoxColumn";
            this.AlamatDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // JkelDataGridViewTextBoxColumn
            // 
            this.JkelDataGridViewTextBoxColumn.DataPropertyName = "desa";
            this.JkelDataGridViewTextBoxColumn.FillWeight = 40F;
            this.JkelDataGridViewTextBoxColumn.HeaderText = "Desa";
            this.JkelDataGridViewTextBoxColumn.Name = "JkelDataGridViewTextBoxColumn";
            this.JkelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // NohpDataGridViewTextBoxColumn
            // 
            this.NohpDataGridViewTextBoxColumn.DataPropertyName = "kota";
            this.NohpDataGridViewTextBoxColumn.FillWeight = 60F;
            this.NohpDataGridViewTextBoxColumn.HeaderText = "Kota";
            this.NohpDataGridViewTextBoxColumn.Name = "NohpDataGridViewTextBoxColumn";
            this.NohpDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tanggallahir
            // 
            this.tanggallahir.FillWeight = 60F;
            this.tanggallahir.HeaderText = "Tanggal Lahir";
            this.tanggallahir.Name = "tanggallahir";
            this.tanggallahir.ReadOnly = true;
            // 
            // totallaporan
            // 
            this.totallaporan.FillWeight = 42F;
            this.totallaporan.HeaderText = "Total Laporan";
            this.totallaporan.Name = "totallaporan";
            this.totallaporan.ReadOnly = true;
            // 
            // nohp
            // 
            this.nohp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.nohp.FillWeight = 60F;
            this.nohp.HeaderText = "Handphone";
            this.nohp.Name = "nohp";
            this.nohp.ReadOnly = true;
            this.nohp.Width = 88;
            // 
            // email
            // 
            this.email.HeaderText = "Email";
            this.email.Name = "email";
            this.email.ReadOnly = true;
            // 
            // password
            // 
            this.password.HeaderText = "Password";
            this.password.Name = "password";
            this.password.ReadOnly = true;
            // 
            // blokir
            // 
            this.blokir.FillWeight = 30F;
            this.blokir.HeaderText = "Blokir";
            this.blokir.Name = "blokir";
            this.blokir.ReadOnly = true;
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.StatusStrip1.ForeColor = System.Drawing.Color.Black;
            this.StatusStrip1.Location = new System.Drawing.Point(1, 298);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.StatusStrip1.Size = new System.Drawing.Size(879, 22);
            this.StatusStrip1.SizingGrip = false;
            this.StatusStrip1.TabIndex = 0;
            this.StatusStrip1.Text = "SSAnggota";
            // 
            // TSSLBanyakDataAnggota
            // 
            this.TSSLBanyakDataAnggota.Name = "TSSLBanyakDataAnggota";
            this.TSSLBanyakDataAnggota.Size = new System.Drawing.Size(14, 17);
            this.TSSLBanyakDataAnggota.Text = "0";
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(19, 17);
            this.ToolStripStatusLabel1.Text = " | ";
            // 
            // TSSLNimAnggota
            // 
            this.TSSLNimAnggota.Name = "TSSLNimAnggota";
            this.TSSLNimAnggota.Size = new System.Drawing.Size(0, 17);
            // 
            // LDSAnggota
            // 
            this.LDSAnggota.Name = "LDSAnggota";
            this.LDSAnggota.Size = new System.Drawing.Size(831, 17);
            this.LDSAnggota.Spring = true;
            // 
            // TBIPengguna
            // 
            this.TBIPengguna.AttachedControl = this.tabControlPanel1;
            this.TBIPengguna.Name = "TBIPengguna";
            this.TBIPengguna.Text = "Pengguna";
            this.TBIPengguna.Click += new System.EventHandler(this.TBIPengguna_Click);
            // 
            // tabControlPanel4
            // 
            this.tabControlPanel4.Controls.Add(this.statusStrip4);
            this.tabControlPanel4.Controls.Add(this.tableLayoutPanel2);
            this.tabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel4.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel4.Name = "tabControlPanel4";
            this.tabControlPanel4.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel4.Size = new System.Drawing.Size(881, 321);
            this.tabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tabControlPanel4.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel4.Style.GradientAngle = 90;
            this.tabControlPanel4.TabIndex = 0;
            this.tabControlPanel4.TabItem = this.TBILaporanKomentar;
            // 
            // statusStrip4
            // 
            this.statusStrip4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.statusStrip4.ForeColor = System.Drawing.Color.Black;
            this.statusStrip4.Location = new System.Drawing.Point(1, 298);
            this.statusStrip4.Name = "statusStrip4";
            this.statusStrip4.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip4.Size = new System.Drawing.Size(879, 22);
            this.statusStrip4.SizingGrip = false;
            this.statusStrip4.TabIndex = 0;
            this.statusStrip4.Text = "SSAnggota";
            // 
            // TSSLBanyakDataRapat
            // 
            this.TSSLBanyakDataRapat.Name = "TSSLBanyakDataRapat";
            this.TSSLBanyakDataRapat.Size = new System.Drawing.Size(14, 17);
            this.TSSLBanyakDataRapat.Text = "0";
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel11.Text = " | ";
            // 
            // TSSLNamaRapat
            // 
            this.TSSLNamaRapat.Name = "TSSLNamaRapat";
            this.TSSLNamaRapat.Size = new System.Drawing.Size(10, 17);
            this.TSSLNamaRapat.Text = " ";
            // 
            // LDSRapat
            // 
            this.LDSRapat.AutoSize = false;
            this.LDSRapat.Name = "LDSRapat";
            this.LDSRapat.Size = new System.Drawing.Size(821, 17);
            this.LDSRapat.Spring = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.DGLKomentarFas, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.DGLKomentarPlgr, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelX1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelX3, 1, 0);
            this.tableLayoutPanel2.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(-3, 38);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(887, 262);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // DGLKomentarFas
            // 
            this.DGLKomentarFas.AllowUserToAddRows = false;
            this.DGLKomentarFas.AllowUserToDeleteRows = false;
            this.DGLKomentarFas.AllowUserToResizeRows = false;
            this.DGLKomentarFas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGLKomentarFas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGLKomentarFas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGLKomentarFas.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGLKomentarFas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.DGLKomentarFas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGLKomentarFas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kdid,
            this.pgid,
            this.lpid,
            this.waktu,
            this.komentar,
            this.nama});
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGLKomentarFas.DefaultCellStyle = dataGridViewCellStyle32;
            this.DGLKomentarFas.EnableHeadersVisualStyles = false;
            this.DGLKomentarFas.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGLKomentarFas.Location = new System.Drawing.Point(3, 33);
            this.DGLKomentarFas.Name = "DGLKomentarFas";
            this.DGLKomentarFas.ReadOnly = true;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGLKomentarFas.RowHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this.DGLKomentarFas.RowHeadersVisible = false;
            this.DGLKomentarFas.RowHeadersWidth = 100;
            this.DGLKomentarFas.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGLKomentarFas.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.DGLKomentarFas.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.DGLKomentarFas.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.DGLKomentarFas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGLKomentarFas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGLKomentarFas.Size = new System.Drawing.Size(437, 226);
            this.DGLKomentarFas.TabIndex = 3;
            this.DGLKomentarFas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGLKomentarFas_CellClick);
            this.DGLKomentarFas.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGLKomentarFas_CellDoubleClick);
            this.DGLKomentarFas.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DGLKomentarFas_RowsAdded);
            // 
            // kdid
            // 
            this.kdid.HeaderText = "KDID";
            this.kdid.Name = "kdid";
            this.kdid.ReadOnly = true;
            this.kdid.Visible = false;
            // 
            // pgid
            // 
            this.pgid.HeaderText = "ID";
            this.pgid.Name = "pgid";
            this.pgid.ReadOnly = true;
            this.pgid.Visible = false;
            // 
            // lpid
            // 
            this.lpid.HeaderText = "PGID";
            this.lpid.Name = "lpid";
            this.lpid.ReadOnly = true;
            this.lpid.Visible = false;
            // 
            // waktu
            // 
            this.waktu.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.waktu.FillWeight = 35F;
            this.waktu.HeaderText = "Waktu";
            this.waktu.Name = "waktu";
            this.waktu.ReadOnly = true;
            // 
            // komentar
            // 
            this.komentar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.komentar.HeaderText = "Komentar";
            this.komentar.Name = "komentar";
            this.komentar.ReadOnly = true;
            // 
            // nama
            // 
            this.nama.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nama.FillWeight = 35F;
            this.nama.HeaderText = "Dilaporkan Oleh";
            this.nama.Name = "nama";
            this.nama.ReadOnly = true;
            // 
            // DGLKomentarPlgr
            // 
            this.DGLKomentarPlgr.AllowUserToAddRows = false;
            this.DGLKomentarPlgr.AllowUserToDeleteRows = false;
            this.DGLKomentarPlgr.AllowUserToResizeRows = false;
            this.DGLKomentarPlgr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGLKomentarPlgr.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGLKomentarPlgr.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGLKomentarPlgr.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGLKomentarPlgr.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.DGLKomentarPlgr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGLKomentarPlgr.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24});
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGLKomentarPlgr.DefaultCellStyle = dataGridViewCellStyle35;
            this.DGLKomentarPlgr.EnableHeadersVisualStyles = false;
            this.DGLKomentarPlgr.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGLKomentarPlgr.Location = new System.Drawing.Point(446, 33);
            this.DGLKomentarPlgr.Name = "DGLKomentarPlgr";
            this.DGLKomentarPlgr.ReadOnly = true;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGLKomentarPlgr.RowHeadersDefaultCellStyle = dataGridViewCellStyle36;
            this.DGLKomentarPlgr.RowHeadersVisible = false;
            this.DGLKomentarPlgr.RowHeadersWidth = 100;
            this.DGLKomentarPlgr.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGLKomentarPlgr.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.DGLKomentarPlgr.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.DGLKomentarPlgr.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.DGLKomentarPlgr.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGLKomentarPlgr.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGLKomentarPlgr.Size = new System.Drawing.Size(438, 226);
            this.DGLKomentarPlgr.TabIndex = 4;
            this.DGLKomentarPlgr.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGLKomentarFas_CellClick);
            this.DGLKomentarPlgr.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGLKomentarPlgr_CellDoubleClick);
            this.DGLKomentarPlgr.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DGLKomentarFas_RowsAdded);
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "KDID";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Visible = false;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "ID";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "PGID";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Visible = false;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn20.FillWeight = 35F;
            this.dataGridViewTextBoxColumn20.HeaderText = "Waktu";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn23.HeaderText = "Komentar";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn24.FillWeight = 35F;
            this.dataGridViewTextBoxColumn24.HeaderText = "Dilaporkan Oleh";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.BackgroundStyle.MarginTop = 5;
            this.labelX1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Far;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.labelX1.ImageTextSpacing = 5;
            this.labelX1.Location = new System.Drawing.Point(3, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(437, 24);
            this.labelX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX1.Symbol = "";
            this.labelX1.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.labelX1.SymbolSize = 15F;
            this.labelX1.TabIndex = 5;
            this.labelX1.Text = "Laporan Komentar L. Fasilitas";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX1.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.BackgroundStyle.MarginTop = 5;
            this.labelX3.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Far;
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.ImageTextSpacing = 5;
            this.labelX3.Location = new System.Drawing.Point(446, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(438, 24);
            this.labelX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX3.Symbol = "";
            this.labelX3.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.labelX3.SymbolSize = 15F;
            this.labelX3.TabIndex = 6;
            this.labelX3.Text = "Laporan Komentar L. Pelanggaran";
            this.labelX3.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // TBILaporanKomentar
            // 
            this.TBILaporanKomentar.AttachedControl = this.tabControlPanel4;
            this.TBILaporanKomentar.Name = "TBILaporanKomentar";
            this.TBILaporanKomentar.Text = "Laporan Komentar";
            this.TBILaporanKomentar.Click += new System.EventHandler(this.TBIPengguna_Click);
            // 
            // tabControlPanel7
            // 
            this.tabControlPanel7.Controls.Add(this.DGSebagai);
            this.tabControlPanel7.Controls.Add(this.statusStrip7);
            this.tabControlPanel7.Controls.Add(this.RMKeuangan);
            this.tabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel7.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel7.Name = "tabControlPanel7";
            this.tabControlPanel7.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel7.Size = new System.Drawing.Size(881, 321);
            this.tabControlPanel7.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControlPanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel7.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tabControlPanel7.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel7.Style.GradientAngle = 90;
            this.tabControlPanel7.TabIndex = 0;
            this.tabControlPanel7.TabItem = this.TBIKeuangan;
            // 
            // DGSebagai
            // 
            this.DGSebagai.AllowUserToAddRows = false;
            this.DGSebagai.AllowUserToDeleteRows = false;
            this.DGSebagai.AllowUserToResizeRows = false;
            this.DGSebagai.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGSebagai.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGSebagai.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGSebagai.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGSebagai.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle37;
            this.DGSebagai.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGSebagai.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sbid,
            this.sebagai,
            this.cpanel,
            this.cpengguna,
            this.claporanfas,
            this.claporanplgr,
            this.claporankmt,
            this.csebagai});
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGSebagai.DefaultCellStyle = dataGridViewCellStyle38;
            this.DGSebagai.EnableHeadersVisualStyles = false;
            this.DGSebagai.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGSebagai.Location = new System.Drawing.Point(0, 48);
            this.DGSebagai.Name = "DGSebagai";
            this.DGSebagai.ReadOnly = true;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGSebagai.RowHeadersDefaultCellStyle = dataGridViewCellStyle39;
            this.DGSebagai.RowHeadersVisible = false;
            this.DGSebagai.RowHeadersWidth = 80;
            this.DGSebagai.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGSebagai.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.DGSebagai.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.DGSebagai.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.DGSebagai.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGSebagai.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGSebagai.Size = new System.Drawing.Size(881, 249);
            this.DGSebagai.TabIndex = 1;
            this.DGSebagai.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGSebagai_CellClick);
            this.DGSebagai.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DGSebagai_RowsAdded);
            // 
            // sbid
            // 
            this.sbid.HeaderText = "SBID";
            this.sbid.Name = "sbid";
            this.sbid.ReadOnly = true;
            this.sbid.Visible = false;
            // 
            // sebagai
            // 
            this.sebagai.HeaderText = "Sebagai";
            this.sebagai.Name = "sebagai";
            this.sebagai.ReadOnly = true;
            // 
            // cpanel
            // 
            this.cpanel.HeaderText = "C_Panel";
            this.cpanel.Name = "cpanel";
            this.cpanel.ReadOnly = true;
            // 
            // cpengguna
            // 
            this.cpengguna.HeaderText = "C_Pengguna";
            this.cpengguna.Name = "cpengguna";
            this.cpengguna.ReadOnly = true;
            // 
            // claporanfas
            // 
            this.claporanfas.HeaderText = "C_LaporanFas";
            this.claporanfas.Name = "claporanfas";
            this.claporanfas.ReadOnly = true;
            // 
            // claporanplgr
            // 
            this.claporanplgr.HeaderText = "C_LaporanPlgr";
            this.claporanplgr.Name = "claporanplgr";
            this.claporanplgr.ReadOnly = true;
            // 
            // claporankmt
            // 
            this.claporankmt.HeaderText = "C_LaporanKmt";
            this.claporankmt.Name = "claporankmt";
            this.claporankmt.ReadOnly = true;
            // 
            // csebagai
            // 
            this.csebagai.HeaderText = "C_Sebagai";
            this.csebagai.Name = "csebagai";
            this.csebagai.ReadOnly = true;
            // 
            // statusStrip7
            // 
            this.statusStrip7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.statusStrip7.ForeColor = System.Drawing.Color.Black;
            this.statusStrip7.Location = new System.Drawing.Point(1, 298);
            this.statusStrip7.Name = "statusStrip7";
            this.statusStrip7.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip7.Size = new System.Drawing.Size(879, 22);
            this.statusStrip7.SizingGrip = false;
            this.statusStrip7.TabIndex = 0;
            this.statusStrip7.Text = "SSAnggota";
            // 
            // TSSLTotalDana
            // 
            this.TSSLTotalDana.Name = "TSSLTotalDana";
            this.TSSLTotalDana.Size = new System.Drawing.Size(14, 17);
            this.TSSLTotalDana.Text = "0";
            // 
            // toolStripStatusLabel23
            // 
            this.toolStripStatusLabel23.Name = "toolStripStatusLabel23";
            this.toolStripStatusLabel23.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel23.Text = " | ";
            // 
            // TSSLIdKeuangan
            // 
            this.TSSLIdKeuangan.Name = "TSSLIdKeuangan";
            this.TSSLIdKeuangan.Size = new System.Drawing.Size(10, 17);
            this.TSSLIdKeuangan.Text = " ";
            // 
            // LDSKeuangan
            // 
            this.LDSKeuangan.Name = "LDSKeuangan";
            this.LDSKeuangan.Size = new System.Drawing.Size(821, 17);
            this.LDSKeuangan.Spring = true;
            // 
            // RMKeuangan
            // 
            this.RMKeuangan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.RMKeuangan.ForeColor = System.Drawing.Color.Black;
            this.RMKeuangan.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMITambahSebagai,
            this.RMIUbahSebagai,
            this.RMIHapusSebagai});
            this.RMKeuangan.Location = new System.Drawing.Point(44, 11);
            this.RMKeuangan.Name = "RMKeuangan";
            this.RMKeuangan.Size = new System.Drawing.Size(28, 28);
            this.RMKeuangan.Symbol = "";
            this.RMKeuangan.SymbolSize = 14F;
            this.RMKeuangan.TabIndex = 0;
            this.RMKeuangan.Text = "RadialMenu2";
            // 
            // RMITambahSebagai
            // 
            this.RMITambahSebagai.Name = "RMITambahSebagai";
            this.RMITambahSebagai.Symbol = "";
            this.RMITambahSebagai.Text = "Tambah";
            this.RMITambahSebagai.Click += new System.EventHandler(this.RMITambahSebagai_Klik);
            // 
            // RMIUbahSebagai
            // 
            this.RMIUbahSebagai.Name = "RMIUbahSebagai";
            this.RMIUbahSebagai.Symbol = "";
            this.RMIUbahSebagai.Text = "Ubah";
            this.RMIUbahSebagai.Click += new System.EventHandler(this.RMIUbahSebagai_Klik);
            // 
            // RMIHapusSebagai
            // 
            this.RMIHapusSebagai.Name = "RMIHapusSebagai";
            this.RMIHapusSebagai.Symbol = "";
            this.RMIHapusSebagai.Text = "Hapus";
            this.RMIHapusSebagai.Click += new System.EventHandler(this.RMIHapusSebagai_Klik);
            // 
            // TBIKeuangan
            // 
            this.TBIKeuangan.AttachedControl = this.tabControlPanel7;
            this.TBIKeuangan.Name = "TBIKeuangan";
            this.TBIKeuangan.Text = "Pengaturan Sebagai";
            this.TBIKeuangan.Click += new System.EventHandler(this.TBIPengguna_Click);
            // 
            // tabControlPanel5
            // 
            this.tabControlPanel5.Controls.Add(this.DGObrolan);
            this.tabControlPanel5.Controls.Add(this.statusStrip5);
            this.tabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel5.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel5.Name = "tabControlPanel5";
            this.tabControlPanel5.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel5.Size = new System.Drawing.Size(881, 321);
            this.tabControlPanel5.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel5.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tabControlPanel5.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel5.Style.GradientAngle = 90;
            this.tabControlPanel5.TabIndex = 0;
            this.tabControlPanel5.TabItem = this.TBIProker;
            // 
            // DGObrolan
            // 
            this.DGObrolan.AllowUserToAddRows = false;
            this.DGObrolan.AllowUserToDeleteRows = false;
            this.DGObrolan.AllowUserToResizeRows = false;
            this.DGObrolan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGObrolan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGObrolan.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGObrolan.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle40.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGObrolan.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle40;
            this.DGObrolan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGObrolan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn18,
            this.namadari});
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGObrolan.DefaultCellStyle = dataGridViewCellStyle41;
            this.DGObrolan.EnableHeadersVisualStyles = false;
            this.DGObrolan.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGObrolan.Location = new System.Drawing.Point(0, 48);
            this.DGObrolan.Name = "DGObrolan";
            this.DGObrolan.ReadOnly = true;
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle42.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGObrolan.RowHeadersDefaultCellStyle = dataGridViewCellStyle42;
            this.DGObrolan.RowHeadersVisible = false;
            this.DGObrolan.RowHeadersWidth = 100;
            this.DGObrolan.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGObrolan.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.DGObrolan.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.DGObrolan.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.DGObrolan.RowTemplate.DividerHeight = 1;
            this.DGObrolan.RowTemplate.Height = 30;
            this.DGObrolan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGObrolan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGObrolan.Size = new System.Drawing.Size(881, 249);
            this.DGObrolan.TabIndex = 4;
            this.DGObrolan.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGObrolan_CellDoubleClick);
            this.DGObrolan.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DGObrolan_RowsAdded);
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "PGID";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Visible = false;
            // 
            // namadari
            // 
            this.namadari.HeaderText = "Obrolan Dari";
            this.namadari.Name = "namadari";
            this.namadari.ReadOnly = true;
            // 
            // statusStrip5
            // 
            this.statusStrip5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.statusStrip5.ForeColor = System.Drawing.Color.Black;
            this.statusStrip5.Location = new System.Drawing.Point(1, 298);
            this.statusStrip5.Name = "statusStrip5";
            this.statusStrip5.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip5.Size = new System.Drawing.Size(879, 22);
            this.statusStrip5.SizingGrip = false;
            this.statusStrip5.TabIndex = 0;
            this.statusStrip5.Text = "SSAnggota";
            // 
            // TSSLBanyakDataProker
            // 
            this.TSSLBanyakDataProker.Name = "TSSLBanyakDataProker";
            this.TSSLBanyakDataProker.Size = new System.Drawing.Size(14, 17);
            this.TSSLBanyakDataProker.Text = "0";
            // 
            // toolStripStatusLabel15
            // 
            this.toolStripStatusLabel15.Name = "toolStripStatusLabel15";
            this.toolStripStatusLabel15.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel15.Text = " | ";
            // 
            // TSSLIdProker
            // 
            this.TSSLIdProker.Name = "TSSLIdProker";
            this.TSSLIdProker.Size = new System.Drawing.Size(10, 17);
            this.TSSLIdProker.Text = " ";
            // 
            // LDSProker
            // 
            this.LDSProker.AutoSize = false;
            this.LDSProker.Name = "LDSProker";
            this.LDSProker.Size = new System.Drawing.Size(821, 17);
            this.LDSProker.Spring = true;
            // 
            // TBIProker
            // 
            this.TBIProker.AttachedControl = this.tabControlPanel5;
            this.TBIProker.Name = "TBIProker";
            this.TBIProker.Text = "Obrolan";
            this.TBIProker.Click += new System.EventHandler(this.TBIPengguna_Click);
            // 
            // TBIInventaris
            // 
            this.TBIInventaris.Name = "TBIInventaris";
            this.TBIInventaris.Text = "Inventaris";
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.LTotalAngg);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.LTotalLPlgrDil);
            this.groupPanel1.Controls.Add(this.LTotalLFasDil);
            this.groupPanel1.Controls.Add(this.LTotalLPlgr);
            this.groupPanel1.Controls.Add(this.LTotalLFas);
            this.groupPanel1.Controls.Add(this.labelX11);
            this.groupPanel1.Controls.Add(this.labelX12);
            this.groupPanel1.Controls.Add(this.labelX13);
            this.groupPanel1.Controls.Add(this.labelX14);
            this.groupPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupPanel1.Location = new System.Drawing.Point(272, 3);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(336, 172);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "Informasi";
            // 
            // LTotalAngg
            // 
            this.LTotalAngg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LTotalAngg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LTotalAngg.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LTotalAngg.ForeColor = System.Drawing.Color.Black;
            this.LTotalAngg.Location = new System.Drawing.Point(230, 121);
            this.LTotalAngg.Name = "LTotalAngg";
            this.LTotalAngg.Size = new System.Drawing.Size(92, 23);
            this.LTotalAngg.TabIndex = 0;
            this.LTotalAngg.Text = "Memuat...";
            this.LTotalAngg.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.BackgroundStyle.MarginTop = 5;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.ImageTextSpacing = 5;
            this.labelX2.Location = new System.Drawing.Point(4, 121);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(220, 23);
            this.labelX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX2.Symbol = "";
            this.labelX2.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.labelX2.SymbolSize = 15F;
            this.labelX2.TabIndex = 0;
            this.labelX2.Text = "Total Pengguna";
            this.labelX2.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // LTotalLPlgrDil
            // 
            this.LTotalLPlgrDil.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LTotalLPlgrDil.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LTotalLPlgrDil.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LTotalLPlgrDil.ForeColor = System.Drawing.Color.Black;
            this.LTotalLPlgrDil.Location = new System.Drawing.Point(230, 92);
            this.LTotalLPlgrDil.Name = "LTotalLPlgrDil";
            this.LTotalLPlgrDil.Size = new System.Drawing.Size(92, 23);
            this.LTotalLPlgrDil.TabIndex = 0;
            this.LTotalLPlgrDil.Text = "Memuat...";
            this.LTotalLPlgrDil.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // LTotalLFasDil
            // 
            this.LTotalLFasDil.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LTotalLFasDil.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LTotalLFasDil.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LTotalLFasDil.ForeColor = System.Drawing.Color.Black;
            this.LTotalLFasDil.Location = new System.Drawing.Point(230, 63);
            this.LTotalLFasDil.Name = "LTotalLFasDil";
            this.LTotalLFasDil.Size = new System.Drawing.Size(92, 23);
            this.LTotalLFasDil.TabIndex = 0;
            this.LTotalLFasDil.Text = "Memuat...";
            this.LTotalLFasDil.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // LTotalLPlgr
            // 
            this.LTotalLPlgr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LTotalLPlgr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LTotalLPlgr.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LTotalLPlgr.ForeColor = System.Drawing.Color.Black;
            this.LTotalLPlgr.Location = new System.Drawing.Point(230, 34);
            this.LTotalLPlgr.Name = "LTotalLPlgr";
            this.LTotalLPlgr.Size = new System.Drawing.Size(92, 23);
            this.LTotalLPlgr.TabIndex = 0;
            this.LTotalLPlgr.Text = "Memuat...";
            this.LTotalLPlgr.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // LTotalLFas
            // 
            this.LTotalLFas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LTotalLFas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LTotalLFas.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LTotalLFas.ForeColor = System.Drawing.Color.Black;
            this.LTotalLFas.Location = new System.Drawing.Point(230, 5);
            this.LTotalLFas.Name = "LTotalLFas";
            this.LTotalLFas.Size = new System.Drawing.Size(92, 23);
            this.LTotalLFas.TabIndex = 0;
            this.LTotalLFas.Text = "Memuat...";
            this.LTotalLFas.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.BackgroundStyle.MarginTop = 5;
            this.labelX11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.ImageTextSpacing = 5;
            this.labelX11.Location = new System.Drawing.Point(4, 92);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(220, 23);
            this.labelX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX11.Symbol = "";
            this.labelX11.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.labelX11.SymbolSize = 15F;
            this.labelX11.TabIndex = 0;
            this.labelX11.Text = "Total L. Pelanggaran Dilaporkan";
            this.labelX11.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.BackgroundStyle.MarginTop = 5;
            this.labelX12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.ImageTextSpacing = 5;
            this.labelX12.Location = new System.Drawing.Point(4, 63);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(222, 23);
            this.labelX12.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX12.Symbol = "";
            this.labelX12.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.labelX12.SymbolSize = 15F;
            this.labelX12.TabIndex = 0;
            this.labelX12.Text = " Total L. Fasilitas Dilaporkan";
            this.labelX12.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.BackgroundStyle.MarginTop = 5;
            this.labelX13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.ImageTextSpacing = 5;
            this.labelX13.Location = new System.Drawing.Point(4, 34);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(220, 23);
            this.labelX13.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX13.Symbol = "";
            this.labelX13.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.labelX13.SymbolSize = 15F;
            this.labelX13.TabIndex = 0;
            this.labelX13.Text = "Total Laporan Pelanggaran";
            this.labelX13.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.BackgroundStyle.MarginTop = 5;
            this.labelX14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.ImageTextSpacing = 5;
            this.labelX14.Location = new System.Drawing.Point(4, 5);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(220, 23);
            this.labelX14.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX14.Symbol = "";
            this.labelX14.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(171)))), ((int)(((byte)(183)))));
            this.labelX14.SymbolSize = 15F;
            this.labelX14.TabIndex = 0;
            this.labelX14.Text = " Total Laporan Fasilitas";
            this.labelX14.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.circularProgress1);
            this.groupPanel2.Controls.Add(this.PBFas2);
            this.groupPanel2.Controls.Add(this.PBFas1);
            this.groupPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupPanel2.Location = new System.Drawing.Point(3, 3);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(263, 172);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 0;
            this.groupPanel2.Text = "Laporan Fasilitas Terbaru";
            // 
            // circularProgress1
            // 
            this.circularProgress1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.circularProgress1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.circularProgress1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress1.Location = new System.Drawing.Point(103, 50);
            this.circularProgress1.Name = "circularProgress1";
            this.circularProgress1.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Dot;
            this.circularProgress1.Size = new System.Drawing.Size(50, 50);
            this.circularProgress1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress1.TabIndex = 2;
            // 
            // PBFas2
            // 
            this.PBFas2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PBFas2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.PBFas2.ForeColor = System.Drawing.Color.Black;
            this.PBFas2.Location = new System.Drawing.Point(260, 3);
            this.PBFas2.Name = "PBFas2";
            this.PBFas2.Size = new System.Drawing.Size(251, 145);
            this.PBFas2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PBFas2.TabIndex = 1;
            this.PBFas2.TabStop = false;
            this.PBFas2.Click += new System.EventHandler(this.PBFeedFas_Klik);
            // 
            // PBFas1
            // 
            this.PBFas1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PBFas1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.PBFas1.ForeColor = System.Drawing.Color.Black;
            this.PBFas1.Location = new System.Drawing.Point(3, 3);
            this.PBFas1.Name = "PBFas1";
            this.PBFas1.Size = new System.Drawing.Size(251, 145);
            this.PBFas1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PBFas1.TabIndex = 0;
            this.PBFas1.TabStop = false;
            this.PBFas1.Click += new System.EventHandler(this.PBFeedFas_Klik);
            // 
            // labelX6
            // 
            this.labelX6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(858, 231);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(16, 23);
            this.labelX6.Symbol = "";
            this.labelX6.SymbolColor = System.Drawing.Color.Silver;
            this.labelX6.SymbolSize = 20F;
            this.labelX6.TabIndex = 0;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(31, 231);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(16, 23);
            this.labelX5.Symbol = "";
            this.labelX5.SymbolColor = System.Drawing.Color.Silver;
            this.labelX5.SymbolSize = 20F;
            this.labelX5.TabIndex = 0;
            // 
            // ReflectionLabel1
            // 
            this.ReflectionLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReflectionLabel1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ReflectionLabel1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.ReflectionLabel1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.ReflectionLabel1.BackgroundStyle.TextColor = System.Drawing.Color.LightSteelBlue;
            this.ReflectionLabel1.BackgroundStyle.TextShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ReflectionLabel1.BackgroundStyle.TextShadowOffset = new System.Drawing.Point(5, 5);
            this.ReflectionLabel1.Font = new System.Drawing.Font("Continuum Medium", 14.75F);
            this.ReflectionLabel1.ForeColor = System.Drawing.Color.Black;
            this.ReflectionLabel1.Location = new System.Drawing.Point(121, 221);
            this.ReflectionLabel1.Name = "ReflectionLabel1";
            this.ReflectionLabel1.Size = new System.Drawing.Size(661, 36);
            this.ReflectionLabel1.TabIndex = 0;
            this.ReflectionLabel1.Text = "Sistem Aplikasi Terpadu Pelaporan dan Pengaduan Masyarakat";
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.circularProgress2);
            this.groupPanel3.Controls.Add(this.PBPlgr2);
            this.groupPanel3.Controls.Add(this.PBPlgr1);
            this.groupPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupPanel3.Location = new System.Drawing.Point(614, 3);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(263, 172);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 1;
            this.groupPanel3.Text = "Laporan Pelanggaran Terbaru";
            // 
            // circularProgress2
            // 
            this.circularProgress2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.circularProgress2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.circularProgress2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress2.Location = new System.Drawing.Point(103, 50);
            this.circularProgress2.Name = "circularProgress2";
            this.circularProgress2.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Dot;
            this.circularProgress2.Size = new System.Drawing.Size(50, 50);
            this.circularProgress2.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress2.TabIndex = 3;
            // 
            // PBPlgr2
            // 
            this.PBPlgr2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PBPlgr2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.PBPlgr2.ForeColor = System.Drawing.Color.Black;
            this.PBPlgr2.Location = new System.Drawing.Point(-253, 3);
            this.PBPlgr2.Name = "PBPlgr2";
            this.PBPlgr2.Size = new System.Drawing.Size(251, 145);
            this.PBPlgr2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PBPlgr2.TabIndex = 2;
            this.PBPlgr2.TabStop = false;
            this.PBPlgr2.Click += new System.EventHandler(this.PBFeedPlgr_Klik);
            // 
            // PBPlgr1
            // 
            this.PBPlgr1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PBPlgr1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.PBPlgr1.ForeColor = System.Drawing.Color.Black;
            this.PBPlgr1.Location = new System.Drawing.Point(3, 3);
            this.PBPlgr1.Name = "PBPlgr1";
            this.PBPlgr1.Size = new System.Drawing.Size(251, 145);
            this.PBPlgr1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PBPlgr1.TabIndex = 1;
            this.PBPlgr1.TabStop = false;
            this.PBPlgr1.Click += new System.EventHandler(this.PBFeedPlgr_Klik);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 342F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.groupPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupPanel3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupPanel1, 1, 0);
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(880, 178);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // TSlideShowFas
            // 
            this.TSlideShowFas.Interval = 5000;
            this.TSlideShowFas.Tick += new System.EventHandler(this.TSlideShow_Tick);
            // 
            // TSlideShowPlgr
            // 
            this.TSlideShowPlgr.Interval = 5000;
            this.TSlideShowPlgr.Tick += new System.EventHandler(this.TSlideShowPlgr_Tick);
            // 
            // RMIMenuUtama
            // 
            this.RMIMenuUtama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.RMIMenuUtama.ForeColor = System.Drawing.Color.Black;
            this.RMIMenuUtama.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMITentangAplikasi,
            this.RMIFacebook,
            this.RMIBantuan,
            this.RMITwitter});
            this.RMIMenuUtama.Location = new System.Drawing.Point(825, 226);
            this.RMIMenuUtama.MenuType = DevComponents.DotNetBar.eRadialMenuType.Circular;
            this.RMIMenuUtama.Name = "RMIMenuUtama";
            this.RMIMenuUtama.Size = new System.Drawing.Size(28, 28);
            this.RMIMenuUtama.Symbol = "";
            this.RMIMenuUtama.SymbolSize = 14F;
            this.RMIMenuUtama.TabIndex = 3;
            this.RMIMenuUtama.Text = "radialMenu1";
            this.RMIMenuUtama.MouseEnter += new System.EventHandler(this.radialMenu1_MouseEnter);
            this.RMIMenuUtama.MouseLeave += new System.EventHandler(this.RMIMenuUtama_MouseLeave);
            // 
            // RMITentangAplikasi
            // 
            this.RMITentangAplikasi.Name = "RMITentangAplikasi";
            this.RMITentangAplikasi.Symbol = "";
            this.RMITentangAplikasi.SymbolSize = 15F;
            this.RMITentangAplikasi.Text = "Tentang";
            this.RMITentangAplikasi.Click += new System.EventHandler(this.RMITentangAplikasi_Klik);
            // 
            // RMIFacebook
            // 
            this.RMIFacebook.Name = "RMIFacebook";
            this.RMIFacebook.Symbol = "";
            this.RMIFacebook.SymbolSize = 15F;
            this.RMIFacebook.Text = "Facebook";
            this.RMIFacebook.Click += new System.EventHandler(this.RMIFacebook_Klik);
            // 
            // RMIBantuan
            // 
            this.RMIBantuan.Name = "RMIBantuan";
            this.RMIBantuan.Symbol = "";
            this.RMIBantuan.SymbolSize = 15F;
            this.RMIBantuan.Text = "Bantuan";
            this.RMIBantuan.Click += new System.EventHandler(this.RMIBantuan_Klik);
            // 
            // RMITwitter
            // 
            this.RMITwitter.Name = "RMITwitter";
            this.RMITwitter.Symbol = "";
            this.RMITwitter.SymbolSize = 15F;
            this.RMITwitter.Text = "Twitter";
            this.RMITwitter.Click += new System.EventHandler(this.RMITwitter_Klik);
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form_Utama
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 566);
            this.Controls.Add(this.RMIMenuUtama);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.ReflectionLabel1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.metroStatusBar1);
            this.MinimumSize = new System.Drawing.Size(730, 480);
            this.Name = "Form_Utama";
            this.SettingsButtonText = "LOGOUT";
            this.SettingsButtonVisible = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SATPAM";
            this.SettingsButtonClick += new System.EventHandler(this.Logout_Klik);
            this.Activated += new System.EventHandler(this.Form_Utama_Activated);
            this.Deactivate += new System.EventHandler(this.Form_Utama_Deactivate);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Utama_FormClosed);
            this.Load += new System.EventHandler(this.Form_Utama_Load);
            this.Shown += new System.EventHandler(this.Form_Utama_Shown);
            this.Enter += new System.EventHandler(this.Form_Utama_Deactivate);
            this.Leave += new System.EventHandler(this.Form_Utama_Activated);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form_Utama_Deactivate);
            this.metroStatusBar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabControlPanel2.ResumeLayout(false);
            this.tabControlPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGLaporanFasilitas)).EndInit();
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGLaporanPelanggaran)).EndInit();
            this.statusStrip3.ResumeLayout(false);
            this.statusStrip3.PerformLayout();
            this.tabControlPanel1.ResumeLayout(false);
            this.tabControlPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGPengguna)).EndInit();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.tabControlPanel4.ResumeLayout(false);
            this.tabControlPanel4.PerformLayout();
            this.statusStrip4.ResumeLayout(false);
            this.statusStrip4.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGLKomentarFas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGLKomentarPlgr)).EndInit();
            this.tabControlPanel7.ResumeLayout(false);
            this.tabControlPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGSebagai)).EndInit();
            this.statusStrip7.ResumeLayout(false);
            this.statusStrip7.PerformLayout();
            this.tabControlPanel5.ResumeLayout(false);
            this.tabControlPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGObrolan)).EndInit();
            this.statusStrip5.ResumeLayout(false);
            this.statusStrip5.PerformLayout();
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PBFas2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBFas1)).EndInit();
            this.groupPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PBPlgr2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBPlgr1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem LNamaP;
        private DevComponents.DotNetBar.LabelItem LId;
        private DevComponents.DotNetBar.LabelItem LSebagai;
        private DevComponents.DotNetBar.LabelItem LWaktuSekarang;
        private DevComponents.DotNetBar.TabControl tabControl1;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        internal DevComponents.DotNetBar.RadialMenu RMPengguna;
        public DevComponents.DotNetBar.Controls.DataGridViewX DGPengguna;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLBanyakDataAnggota;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLNimAnggota;
        internal System.Windows.Forms.ToolStripStatusLabel LDSAnggota;
        private DevComponents.DotNetBar.TabItem TBIAnggota;
        private DevComponents.DotNetBar.TabItem TBIInventaris;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel5;
        internal System.Windows.Forms.StatusStrip statusStrip5;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLBanyakDataProker;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel15;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLIdProker;
        private DevComponents.DotNetBar.TabItem TBIProker;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel4;
        internal System.Windows.Forms.StatusStrip statusStrip4;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLBanyakDataRapat;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel11;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLNamaRapat;
        internal System.Windows.Forms.ToolStripStatusLabel LDSRapat;
        private DevComponents.DotNetBar.TabItem TBILaporanKomentar;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel3;
        internal System.Windows.Forms.StatusStrip statusStrip3;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLBanyakDataPiket;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel7;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLNimPiket;
        internal System.Windows.Forms.ToolStripStatusLabel LDSPiket;
        internal DevComponents.DotNetBar.RadialMenu RMPiket;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        internal System.Windows.Forms.StatusStrip statusStrip2;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLBanyakDataStruktur;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLNimStruktur;
        internal System.Windows.Forms.ToolStripStatusLabel LDSStruktur;
        internal DevComponents.DotNetBar.RadialMenu RMStruktur;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel7;
        internal System.Windows.Forms.StatusStrip statusStrip7;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLTotalDana;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel23;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLIdKeuangan;
        internal System.Windows.Forms.ToolStripStatusLabel LDSKeuangan;
        internal DevComponents.DotNetBar.RadialMenu RMKeuangan;
        private DevComponents.DotNetBar.TabItem TBIKeuangan;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        internal DevComponents.DotNetBar.LabelX LTotalLPlgrDil;
        internal DevComponents.DotNetBar.LabelX LTotalLFasDil;
        internal DevComponents.DotNetBar.LabelX LTotalLPlgr;
        internal DevComponents.DotNetBar.LabelX LTotalLFas;
        internal DevComponents.DotNetBar.LabelX labelX11;
        internal DevComponents.DotNetBar.LabelX labelX12;
        internal DevComponents.DotNetBar.LabelX labelX13;
        internal DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        internal DevComponents.DotNetBar.LabelX LTotalAngg;
        internal DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NimDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NamaanggotaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn KelasDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlamatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn JkelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NohpDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tanggallahir;
        private System.Windows.Forms.DataGridViewTextBoxColumn totallaporan;
        private System.Windows.Forms.DataGridViewTextBoxColumn nohp;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.DataGridViewTextBoxColumn password;
        private System.Windows.Forms.DataGridViewTextBoxColumn blokir;
        private DevComponents.DotNetBar.RadialMenuItem RMITambahPengguna;
        private DevComponents.DotNetBar.RadialMenuItem RMIUbahPengguna;
        private DevComponents.DotNetBar.RadialMenuItem RMIHapusPengguna;
        private DevComponents.DotNetBar.RadialMenuItem RMIOlahPengguna;
        private DevComponents.DotNetBar.RadialMenuItem RMIBlokirPengguna;
        private DevComponents.DotNetBar.RadialMenuItem radialMenuItem1;
        private DevComponents.DotNetBar.RadialMenuItem RMIUBlokirPengguna;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX5;
        internal DevComponents.DotNetBar.Controls.ReflectionLabel ReflectionLabel1;
        private DevComponents.DotNetBar.TabItem TBIPengguna;
        public DevComponents.DotNetBar.Controls.DataGridViewX DGLaporanFasilitas;
        private DevComponents.DotNetBar.RadialMenuItem RMITambahLFas;
        private DevComponents.DotNetBar.RadialMenuItem RMIUbahLFas;
        private DevComponents.DotNetBar.RadialMenuItem RMIHapusLFas;
        private DevComponents.DotNetBar.RadialMenuItem RMIOlahLFas;
        private DevComponents.DotNetBar.RadialMenuItem RMIDetailLFas;
        public DevComponents.DotNetBar.Controls.DataGridViewX DGLaporanPelanggaran;
        private DevComponents.DotNetBar.RadialMenuItem RMITambahPlgr;
        private DevComponents.DotNetBar.RadialMenuItem RMIUbahPlgr;
        private DevComponents.DotNetBar.RadialMenuItem RMIHapusPlgr;
        private DevComponents.DotNetBar.RadialMenuItem RMIOlahPlgr;
        private DevComponents.DotNetBar.RadialMenuItem RMIDetailLPlgr;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        internal DevComponents.DotNetBar.ButtonX BLFasDilaporkan;
        internal DevComponents.DotNetBar.ButtonX BLPlgrDilaporkan;
        public DevComponents.DotNetBar.Controls.DataGridViewX DGLKomentarFas;
        public DevComponents.DotNetBar.Controls.DataGridViewX DGLKomentarPlgr;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        internal DevComponents.DotNetBar.LabelX labelX1;
        internal DevComponents.DotNetBar.LabelX labelX3;
        private System.Windows.Forms.DataGridViewTextBoxColumn kdid;
        private System.Windows.Forms.DataGridViewTextBoxColumn pgid;
        private System.Windows.Forms.DataGridViewTextBoxColumn lpid;
        private System.Windows.Forms.DataGridViewTextBoxColumn waktu;
        private System.Windows.Forms.DataGridViewTextBoxColumn komentar;
        private System.Windows.Forms.DataGridViewTextBoxColumn nama;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        public DevComponents.DotNetBar.Controls.DataGridViewX DGObrolan;
        public DevComponents.DotNetBar.Controls.DataGridViewX DGSebagai;
        private System.Windows.Forms.DataGridViewTextBoxColumn sbid;
        private System.Windows.Forms.DataGridViewTextBoxColumn sebagai;
        private System.Windows.Forms.DataGridViewTextBoxColumn cpanel;
        private System.Windows.Forms.DataGridViewTextBoxColumn cpengguna;
        private System.Windows.Forms.DataGridViewTextBoxColumn claporanfas;
        private System.Windows.Forms.DataGridViewTextBoxColumn claporanplgr;
        private System.Windows.Forms.DataGridViewTextBoxColumn claporankmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn csebagai;
        private DevComponents.DotNetBar.RadialMenuItem RMITambahSebagai;
        private DevComponents.DotNetBar.RadialMenuItem RMIUbahSebagai;
        private DevComponents.DotNetBar.RadialMenuItem RMIHapusSebagai;
        private System.Windows.Forms.PictureBox PBFas2;
        private System.Windows.Forms.PictureBox PBFas1;
        private System.Windows.Forms.PictureBox PBPlgr2;
        private System.Windows.Forms.PictureBox PBPlgr1;
        private System.Windows.Forms.Timer TSlideShowFas;
        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress1;
        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress2;
        public DevComponents.DotNetBar.TabItem TBILPelanggaran;
        public DevComponents.DotNetBar.TabItem TBILFasilitas;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn namadari;
        private System.Windows.Forms.Timer TSlideShowPlgr;
        private DevComponents.DotNetBar.RadialMenu RMIMenuUtama;
        private DevComponents.DotNetBar.RadialMenuItem RMITentangAplikasi;
        private DevComponents.DotNetBar.RadialMenuItem RMIFacebook;
        private DevComponents.DotNetBar.RadialMenuItem RMIBantuan;
        private DevComponents.DotNetBar.RadialMenuItem RMITwitter;
        private DevComponents.DotNetBar.LabelX LStatusKoneksi;
        private System.Windows.Forms.Timer timer1;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem1;
        internal System.Windows.Forms.ToolStripStatusLabel LDSProker;
        private System.Windows.Forms.DataGridViewTextBoxColumn lfid;
        private System.Windows.Forms.DataGridViewTextBoxColumn foto;
        private System.Windows.Forms.DataGridViewTextBoxColumn keluhan;
        private System.Windows.Forms.DataGridViewTextBoxColumn lat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ling;
        private System.Windows.Forms.DataGridViewTextBoxColumn alamat;
        private System.Windows.Forms.DataGridViewTextBoxColumn prioritas;
        private System.Windows.Forms.DataGridViewTextBoxColumn dilaporkanpada;
        private System.Windows.Forms.DataGridViewTextBoxColumn dilaporkanoleh;
        private System.Windows.Forms.DataGridViewTextBoxColumn laporanrahasia;
        private System.Windows.Forms.DataGridViewTextBoxColumn dukungan;
        private System.Windows.Forms.DataGridViewTextBoxColumn omentar;
    }
}