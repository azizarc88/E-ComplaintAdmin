﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Satpam_Desktop.Class;

namespace Satpam_Desktop.Form
{
    public partial class Form_PilihLaporan : DevComponents.DotNetBar.Metro.MetroForm
    {
        DataGridManager DGM = new DataGridManager();
        private Bagian bagian;

        public delegate void ItemTerpilihHandler(object sender, ItemTerpilihEventArgs e);
        public event ItemTerpilihHandler ItemTerpilih;

        public enum Bagian
        {
            Fasilitas = 0,
            Pelanggaran = 1

        }

        public Form_PilihLaporan(Bagian bagian)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
            this.bagian = bagian;

            if (bagian == Bagian.Pelanggaran)
            {
                keluhan.HeaderText = "Pelanggaran";
            }
        }

        private void DGPengguna_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int y = DGLaporan.CurrentCellAddress.Y;
                LCNimDA.Text = DGLaporan[0, y].Value.ToString();
                LContentDA.Text = DGLaporan.CurrentCell.Value.ToString();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void TBCari_TextChanged(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void BPilih_Click(object sender, EventArgs e)
        {
            List<string> Daftar = new List<string>();

            foreach (DataGridViewRow baris in DGLaporan.SelectedRows)
            {
                foreach (DataGridViewCell cell in baris.Cells)
                {
                    Daftar.Add(cell.Value.ToString());
                }
            }

            ItemTerpilihEventArgs args = new ItemTerpilihEventArgs(Daftar);
            ItemTerpilih(this, args);

            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Cari(TBCari.Text);
            timer1.Enabled = false;
        }

        private void Cari(string TeksCari)
        {
            foreach (DataGridViewRow baris in DGLaporan.Rows)
            {
                foreach (DataGridViewCell cell in baris.Cells)
                {
                    cell.Style.BackColor = Color.WhiteSmoke;
                }
            }

            if (TeksCari != "")
            {
                foreach (DataGridViewRow baris in DGLaporan.Rows)
                {
                    foreach (DataGridViewCell cell in baris.Cells)
                    {
                        if (cell.Value.ToString().ToLower().Contains(TeksCari.ToString().ToLower()))
                        {
                            cell.Style.BackColor = Color.LightBlue;
                            DGLaporan.CurrentCell = cell;
                        }
                    }
                }
            }
        }

        private void Form_PilihAnggota_Load(object sender, EventArgs e)
        {
            if (bagian == Bagian.Fasilitas)
            {
                DGM.IsiDG(DGLaporan, "Laporan Fasilitas");
            }
            else
            {
                DGM.IsiDG(DGLaporan, "Laporan Pelanggaran");
            }
        }

        private void Form_PilihAnggota_Shown(object sender, EventArgs e)
        {
            TBCari.Focus();
        }

        private void DGPengguna_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            List<string> Daftar = new List<string>();

            foreach (DataGridViewRow baris in DGLaporan.SelectedRows)
            {
                foreach (DataGridViewCell cell in baris.Cells)
                {
                    Daftar.Add(cell.Value.ToString());
                }
            }

            ItemTerpilihEventArgs args = new ItemTerpilihEventArgs(Daftar);
            ItemTerpilih(this, args);

            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
        }
    }
}
