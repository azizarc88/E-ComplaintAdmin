﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Satpam_Desktop.Class;

namespace Satpam_Desktop.Form
{
    public partial class Form_PilihAnggota : DevComponents.DotNetBar.Metro.MetroForm
    {
        DataGridManager DGM = new DataGridManager();

        public delegate void ItemTerpilihHandler(object sender, ItemTerpilihEventArgs e);
        public event ItemTerpilihHandler ItemTerpilih;

        public Form_PilihAnggota()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.Icon_Satpam;
        }

        private void DGPengguna_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int y = DGPengguna.CurrentCellAddress.Y;
                LCNimDA.Text = DGPengguna[0, y].Value.ToString();
                LContentDA.Text = DGPengguna.CurrentCell.Value.ToString();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void TBCari_TextChanged(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void BPilih_Click(object sender, EventArgs e)
        {
            List<string> Daftar = new List<string>();

            foreach (DataGridViewRow baris in DGPengguna.SelectedRows)
            {
                foreach (DataGridViewCell cell in baris.Cells)
                {
                    Daftar.Add(cell.Value.ToString());
                }
            }

            ItemTerpilihEventArgs args = new ItemTerpilihEventArgs(Daftar);
            ItemTerpilih(this, args);

            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Cari(TBCari.Text);
            timer1.Enabled = false;
        }

        private void Cari(string TeksCari)
        {
            foreach (DataGridViewRow baris in DGPengguna.Rows)
            {
                foreach (DataGridViewCell cell in baris.Cells)
                {
                    cell.Style.BackColor = Color.WhiteSmoke;
                }
            }

            if (TeksCari != "")
            {
                foreach (DataGridViewRow baris in DGPengguna.Rows)
                {
                    foreach (DataGridViewCell cell in baris.Cells)
                    {
                        if (cell.Value.ToString().ToLower().Contains(TeksCari.ToString().ToLower()))
                        {
                            cell.Style.BackColor = Color.LightBlue;
                            DGPengguna.CurrentCell = cell;
                        }
                    }
                }
            }
        }

        private void Form_PilihAnggota_Load(object sender, EventArgs e)
        {
            DGM.IsiDG(DGPengguna, "Pengguna");
        }

        private void Form_PilihAnggota_Shown(object sender, EventArgs e)
        {
            TBCari.Focus();
        }

        private void DGPengguna_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            List<string> Daftar = new List<string>();

            foreach (DataGridViewRow baris in DGPengguna.SelectedRows)
            {
                foreach (DataGridViewCell cell in baris.Cells)
                {
                    Daftar.Add(cell.Value.ToString());
                }
            }

            ItemTerpilihEventArgs args = new ItemTerpilihEventArgs(Daftar);
            ItemTerpilih(this, args);

            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
        }
    }
}
