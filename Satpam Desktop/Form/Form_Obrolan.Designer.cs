﻿namespace Satpam_Desktop.Form
{
    partial class Form_Obrolan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.DGObrolan = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BKirimPesan = new DevComponents.DotNetBar.ButtonX();
            this.TBPesan = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.reflectionLabel1 = new DevComponents.DotNetBar.Controls.ReflectionLabel();
            this.LNama = new DevComponents.DotNetBar.LabelX();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.LBaru = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGObrolan)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.DGObrolan);
            this.groupPanel1.Controls.Add(this.BKirimPesan);
            this.groupPanel1.Controls.Add(this.TBPesan);
            this.groupPanel1.Controls.Add(this.reflectionLabel1);
            this.groupPanel1.DrawTitleBox = false;
            this.groupPanel1.Location = new System.Drawing.Point(12, 41);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(311, 498);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor = System.Drawing.Color.White;
            this.groupPanel1.Style.BackColor2 = System.Drawing.Color.White;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 12;
            // 
            // DGObrolan
            // 
            this.DGObrolan.AllowUserToAddRows = false;
            this.DGObrolan.AllowUserToDeleteRows = false;
            this.DGObrolan.AllowUserToResizeColumns = false;
            this.DGObrolan.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.DGObrolan.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGObrolan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGObrolan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.DGObrolan.BackgroundColor = System.Drawing.Color.White;
            this.DGObrolan.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DGObrolan.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.DGObrolan.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.DGObrolan.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGObrolan.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGObrolan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGObrolan.ColumnHeadersVisible = false;
            this.DGObrolan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGObrolan.DefaultCellStyle = dataGridViewCellStyle3;
            this.DGObrolan.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DGObrolan.EnableHeadersVisualStyles = false;
            this.DGObrolan.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGObrolan.HighlightSelectedColumnHeaders = false;
            this.DGObrolan.Location = new System.Drawing.Point(3, 3);
            this.DGObrolan.MultiSelect = false;
            this.DGObrolan.Name = "DGObrolan";
            this.DGObrolan.PaintEnhancedSelection = false;
            this.DGObrolan.ReadOnly = true;
            this.DGObrolan.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGObrolan.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DGObrolan.RowHeadersVisible = false;
            this.DGObrolan.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.DGObrolan.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.DGObrolan.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.DGObrolan.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.DGObrolan.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
            this.DGObrolan.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.DGObrolan.RowTemplate.ReadOnly = true;
            this.DGObrolan.ScrollBarAppearance = DevComponents.DotNetBar.eScrollBarAppearance.Default;
            this.DGObrolan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGObrolan.SelectAllSignVisible = false;
            this.DGObrolan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.DGObrolan.ShowCellErrors = false;
            this.DGObrolan.ShowCellToolTips = false;
            this.DGObrolan.ShowEditingIcon = false;
            this.DGObrolan.ShowRowErrors = false;
            this.DGObrolan.Size = new System.Drawing.Size(295, 448);
            this.DGObrolan.TabIndex = 0;
            this.DGObrolan.UseCustomBackgroundColor = true;
            this.DGObrolan.Visible = false;
            this.DGObrolan.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DGObrolan_CellMouseUp);
            this.DGObrolan.Scroll += new System.Windows.Forms.ScrollEventHandler(this.DGObrolan_Scroll);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // BKirimPesan
            // 
            this.BKirimPesan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BKirimPesan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BKirimPesan.AntiAlias = true;
            this.BKirimPesan.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BKirimPesan.Location = new System.Drawing.Point(231, 460);
            this.BKirimPesan.Name = "BKirimPesan";
            this.BKirimPesan.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BKirimPesan.Size = new System.Drawing.Size(67, 24);
            this.BKirimPesan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BKirimPesan.SymbolSize = 10F;
            this.BKirimPesan.TabIndex = 7;
            this.BKirimPesan.Text = "Kirim";
            this.BKirimPesan.Click += new System.EventHandler(this.BKirimKomen_Click);
            // 
            // TBPesan
            // 
            this.TBPesan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBPesan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBPesan.Border.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TBPesan.Border.BackColor2 = System.Drawing.Color.White;
            this.TBPesan.Border.Class = "TextBoxBorder";
            this.TBPesan.Border.CornerDiameter = 4;
            this.TBPesan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBPesan.ForeColor = System.Drawing.Color.Black;
            this.TBPesan.Location = new System.Drawing.Point(3, 460);
            this.TBPesan.Name = "TBPesan";
            this.TBPesan.Size = new System.Drawing.Size(222, 24);
            this.TBPesan.TabIndex = 1;
            // 
            // reflectionLabel1
            // 
            this.reflectionLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reflectionLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.reflectionLabel1.BackgroundStyle.BackColor = System.Drawing.Color.White;
            this.reflectionLabel1.BackgroundStyle.BackColor2 = System.Drawing.Color.White;
            this.reflectionLabel1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.reflectionLabel1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.reflectionLabel1.BackgroundStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(195)))), ((int)(((byte)(198)))));
            this.reflectionLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.reflectionLabel1.ForeColor = System.Drawing.Color.Black;
            this.reflectionLabel1.Location = new System.Drawing.Point(3, 3);
            this.reflectionLabel1.Name = "reflectionLabel1";
            this.reflectionLabel1.Size = new System.Drawing.Size(295, 448);
            this.reflectionLabel1.TabIndex = 8;
            this.reflectionLabel1.Text = "Tidak ada obrolan";
            // 
            // LNama
            // 
            this.LNama.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LNama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNama.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNama.ForeColor = System.Drawing.Color.Black;
            this.LNama.Location = new System.Drawing.Point(12, 12);
            this.LNama.Name = "LNama";
            this.LNama.Size = new System.Drawing.Size(311, 23);
            this.LNama.TabIndex = 13;
            this.LNama.Text = "labelX1";
            this.LNama.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // LBaru
            // 
            this.LBaru.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LBaru.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LBaru.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LBaru.BackgroundStyle.TextColor = System.Drawing.Color.MediumTurquoise;
            this.LBaru.ForeColor = System.Drawing.Color.Black;
            this.LBaru.Location = new System.Drawing.Point(306, 16);
            this.LBaru.Name = "LBaru";
            this.LBaru.Size = new System.Drawing.Size(17, 23);
            this.LBaru.Symbol = "";
            this.LBaru.SymbolColor = System.Drawing.Color.MediumTurquoise;
            this.LBaru.SymbolSize = 12F;
            this.LBaru.TabIndex = 14;
            this.LBaru.Visible = false;
            // 
            // Form_Obrolan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 551);
            this.Controls.Add(this.LBaru);
            this.Controls.Add(this.LNama);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.MaximumSize = new System.Drawing.Size(412, 9999);
            this.MinimumSize = new System.Drawing.Size(276, 276);
            this.Name = "Form_Obrolan";
            this.Text = "Form_Obrolan";
            this.Load += new System.EventHandler(this.Form_Obrolan_Load);
            this.MouseEnter += new System.EventHandler(this.Form_Obrolan_MouseEnter);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGObrolan)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.DataGridViewX DGObrolan;
        private DevComponents.DotNetBar.Controls.ReflectionLabel reflectionLabel1;
        internal DevComponents.DotNetBar.ButtonX BKirimPesan;
        private DevComponents.DotNetBar.Controls.TextBoxX TBPesan;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private DevComponents.DotNetBar.LabelX LNama;
        private System.Windows.Forms.Timer timer1;
        private DevComponents.DotNetBar.LabelX LBaru;
    }
}